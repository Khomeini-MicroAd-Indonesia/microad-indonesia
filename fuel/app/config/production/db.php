<?php
/**
 * The development database settings. These get merged with the global settings.
 */


return array(
    'default' => array(
        'connection'  => array(
            'dsn'        => 'mysql:host=localhost;dbname=mai2016',
            'username'   => 'microad_dev',
            'password'   => 'a219t3d0',
        ),
    ),
);