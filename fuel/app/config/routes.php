<?php

return array(
    '_root_' => 'pages/frontend/home', // The default route
    '_404_' => 'welcome/404', // The main 404 route
    'business' => 'pages/frontend/home/business',
    'mbusiness' => 'pages/frontend/home/mbusiness',
    'culture' => 'pages/frontend/home/culture',
    'culture/news/detail' => 'pages/frontend/home/news',
    'mculture' => 'pages/frontend/home/mculture',
    'mhome' => 'pages/frontend/home/mhome',
    'business/project/(:slug)' => 'pages/frontend/home/project',
    'culture/photo/(:slug)' => 'pages/frontend/home/photo',
    'culture/news/(:slug)' => 'pages/frontend/home/news',
    
    
    
    // Backend
    'backend/installation' => 'backend/install/setup',
    
    // Backend routes
    'backend' => 'backend/dashboard', // The default route for backend
    'backend/sign-in' => 'backend/dashboard/sign_in',
    'backend/sign-out' => 'backend/dashboard/sign_out',
    'backend/change-password' => 'backend/dashboard/change_current_password',
    'backend/my-profile' => 'backend/dashboard/my_profile_form',
    'backend/no-permission' => 'error/no_permission/backend',
    
    
    
    // About Us //
    
    // All About MAIzen
    'backend/aboutmaizen' => 'aboutmaizen/backend/',
    'backend/aboutmaizen/add' => 'aboutmaizen/backend/form/0',
    'backend/aboutmaizen/edit/(:num)' => 'aboutmaizen/backend/form/$1',
    'backend/aboutmaizen/delete/(:num)' => 'aboutmaizen/backend/delete/$1',
    
    
    // Admin Management //

    // Admin User
    'backend/admin-user' => 'adminmanagement/adminuser',
    'backend/admin-user/add' => 'adminmanagement/adminuser/form/0',
    'backend/admin-user/edit/(:num)' => 'adminmanagement/adminuser/form/$1',
    'backend/admin-user/delete/(:num)' => 'adminmanagement/adminuser/delete/$1',
    'backend/admin-user/reset-password/(:num)' => 'adminmanagement/adminuser/reset_password/$1',

    // Admin Role Permission
    'backend/admin-role-permission' => 'adminmanagement/adminrolepermission',
    'backend/admin-role-permission/add' => 'adminmanagement/adminrolepermission/form/0',
    'backend/admin-role-permission/edit/(:num)' => 'adminmanagement/adminrolepermission/form/$1',
    'backend/admin-role-permission/delete/(:num)' => 'adminmanagement/adminrolepermission/delete/$1',
    'backend/admin-role-permission/assign-admin/(:num)' => 'adminmanagement/adminrolepermission/assign_admin/$1',
    'backend/admin-role-permission/do-assign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/1',
    'backend/admin-role-permission/do-unassign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/0',
    'backend/admin-role-permission/set-permission/(:num)' => 'adminmanagement/adminrolepermission/set_permission/$1',

    // Basic Setting
    'backend/setting' => 'adminmanagement/setting',
    
    
    
    // Business Unit //
    
    // Organize
    'backend/businessunit' => 'businessunit/backend',
    'backend/businessunit/add' => 'businessunit/backend/form/0',
    'backend/businessunit/edit/(:num)' => 'businessunit/backend/form/$1',
    'backend/businessunit/delete/(:num)' => 'businessunit/backend/delete/$1',
    
    // Image
    'backend/businessunit/image' => 'businessunit/backend/image',
    'backend/businessunit/image/add' => 'businessunit/backend/image/form/0',
    'backend/businessunit/image/edit/(:num)' => 'businessunit/backend/image/form/$1',
    'backend/businessunit/image/delete/(:num)' => 'businessunit/backend/image/delete/$1',
    
    
    // Capability //
    
    // Organize
    'backend/capability' => 'capability/backend',
    'backend/capability/add' => 'capability/backend/form/0',
    'backend/capability/edit/(:num)' => 'capability/backend/form/$1',
    'backend/capability/delete/(:num)' => 'capability/backend/delete/$1',
    
    // Image
    'backend/capability/image' => 'capability/backend/image',
    'backend/capability/image/add' => 'capability/backend/image/form/0',
    'backend/capability/image/edit/(:num)' => 'capability/backend/image/form/$1',
    'backend/capability/image/delete/(:num)' => 'capability/backend/image/delete/$1',
    
    
    // Client //
    
    // Organize
    'backend/client' => 'client/backend',
    'backend/client/add' => 'client/backend/form/0',
    'backend/client/edit/(:num)' => 'client/backend/form/$1',
    'backend/client/delete/(:num)' => 'client/backend/delete/$1',
    
    // Image
    'backend/client/image' => 'client/backend/image',
    'backend/client/image/add' => 'client/backend/image/form/0',
    'backend/client/image/edit/(:num)' => 'client/backend/image/form/$1',
    'backend/client/image/delete/(:num)' => 'client/backend/image/delete/$1',
    
    

    // Media Uploader //
    
    // Business Unit
    'backend/media-uploader/businessunit' => 'mediauploader/businessunit',
    'backend/media-uploader/businessunit/upload' => 'mediauploader/businessunit/upload',
    
    // Capability
    'backend/media-uploader/capability' => 'mediauploader/capability',
    'backend/media-uploader/capability/upload' => 'mediauploader/capability/upload',
    
    // Client
    'backend/media-uploader/client' => 'mediauploader/client',
    'backend/media-uploader/client/upload' => 'mediauploader/client/upload',
    
    // News
    'backend/media-uploader/news' => 'mediauploader/news',
    'backend/media-uploader/news/upload' => 'mediauploader/news/upload',
    
    // Portfolio
    'backend/media-uploader/portfolio' => 'mediauploader/portfolio',
    'backend/media-uploader/portfolio/upload' => 'mediauploader/portfolio/upload',
    
    // Photo
    'backend/media-uploader/photo' => 'mediauploader/photo',
    'backend/media-uploader/photo/upload' => 'mediauploader/photo/upload',
     
    // Video
    'backend/media-uploader/video' => 'mediauploader/video',
    'backend/media-uploader/video/upload' => 'mediauploader/video/upload',
    
    
    // Pages //
    'backend/pages' => 'pages/backend',
    'backend/pages/add' => 'pages/backend/form/0',
    'backend/pages/edit/(:num)' => 'pages/backend/form/$1',
    'backend/pages/delete/(:num)' => 'pages/backend/delete/$1',
   
    
    // News //
    
    // Organize
    'backend/news' => 'news/backend',
    'backend/news/add' => 'news/backend/form/0',
    'backend/news/edit/(:num)' => 'news/backend/form/$1',
    'backend/news/delete/(:num)' => 'news/backend/delete/$1',
    
    // Image
    'backend/news/image' => 'news/backend/image',
    'backend/news/image/add' => 'news/backend/image/form/0',
    'backend/news/image/edit/(:num)' => 'news/backend/image/form/$1',
    'backend/news/image/delete/(:num)' => 'news/backend/image/delete/$1',
    
    
    // Our People //
    
    // Organize
    'backend/ourpeople' => 'ourpeople/backend',
    'backend/ourpeople/add' => 'ourpeople/backend/form/0',
    'backend/ourpeople/edit/(:num)' => 'ourpeople/backend/form/$1',
    'backend/ourpeople/delete/(:num)' => 'ourpeople/backend/delete/$1',
    
    
    // Photo - Culture //
    
    // Organize
    'backend/photo' => 'photo/backend',
    'backend/photo/add' => 'photo/backend/form/0',
    'backend/photo/edit/(:num)' => 'photo/backend/form/$1',
    'backend/photo/delete/(:num)' => 'portfolio/backend/delete/$1',
    
    // Image
    'backend/photo/image' => 'photo/backend/image',
    'backend/photo/image/add' => 'photo/backend/image/form/0',
    'backend/photo/image/edit/(:num)' => 'photo/backend/image/form/$1',
    'backend/photo/image/delete/(:num)' => 'photo/backend/image/delete/$1',
      
    // Photo - Detail
    'backend/photo/detail' => 'photo/backend/detail',
    'backend/photo/detail/add' => 'photo/backend/detail/form/0',
    'backend/photo/detail/edit/(:num)' => 'photo/backend/detail/form/$1',
    'backend/photo/detail/delete/(:num)' => 'photo/backend/detail/delete/$1',
    
    
    // Video - Culture //
    
    // Organize
    'backend/video' => 'video/backend',
    'backend/video/add' => 'video/backend/form/0',
    'backend/video/edit/(:num)' => 'video/backend/form/$1',
    'backend/video/delete/(:num)' => 'video/backend/delete/$1',
    
    // Image
    'backend/video/image' => 'video/backend/image',
    'backend/video/image/add' => 'video/backend/image/form/0',
    'backend/video/image/edit/(:num)' => 'video/backend/image/form/$1',
    'backend/video/image/delete/(:num)' => 'video/backend/image/delete/$1',

    
    
    // Portfolio //
    
    // Organize
    'backend/portfolio' => 'portfolio/backend',
    'backend/portfolio/add' => 'portfolio/backend/form/0',
    'backend/portfolio/edit/(:num)' => 'portfolio/backend/form/$1',
    'backend/portfolio/delete/(:num)' => 'portfolio/backend/delete/$1',
    
    // Image
    'backend/portfolio/image' => 'portfolio/backend/image',
    'backend/portfolio/image/add' => 'portfolio/backend/image/form/0',
    'backend/portfolio/image/edit/(:num)' => 'portfolio/backend/image/form/$1',
    'backend/portfolio/image/delete/(:num)' => 'portfolio/backend/image/delete/$1',
    
    // Portfolio - Detail
    'backend/portfolio/detail' => 'portfolio/backend/detail',
    'backend/portfolio/detail/add' => 'portfolio/backend/detail/form/0',
    'backend/portfolio/detail/edit/(:num)' => 'portfolio/backend/detail/form/$1',
    'backend/portfolio/detail/delete/(:num)' => 'portfolio/backend/detail/delete/$1',
    
    
    // Preface //
    
    // Organize
    'backend/preface' => 'preface/backend',
    'backend/preface/add' => 'preface/backend/form/0',
    'backend/preface/edit/(:num)' => 'preface/backend/form/$1',
    'backend/preface/delete/(:num)' => 'preface/backend/delete/$1',
    
);
