<?php

return array(
    'cms_name' => 'MicroAd Indonesia',
    'max_lock_count' => 5,
    'cms_session_name' => array(
        'admin_id' => 'MicroAdIndonesia'
    ),
    'admin_default_password' => 'mai',
    'max_email_sent_daily' => 500,
    'max_email_sent_per_task' => 10,
    'menus' => array(
        'dashboard' => array(
            'label' => 'Dashboard',
            'route' => 'backend',
            'icon_class' => 'fa fa-dashboard',
            'permission' => false,
        ),
        'pages' => array(
            'label' => 'Pages',
            'route' => 'backend/pages',
            'icon_class' => 'fa fa-sitemap',
            'permission' => true,
        ),
        'about_maizen_management' => array(
            'label' => 'About MAIzen',
            'icon_class' => 'fa fa-group',
            'permission' => false,
            'submenus' => array(
                'about_maizen_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/aboutmaizen/',
                    'icon_class' => 'fa fa-male',
                    'permission' => true,
                )
            )
        ),
        'admin_management' => array(
            'label' => 'Admin Management',
            'icon_class' => 'fa fa-columns',
            'permission' => false,
            'submenus' => array(
                'admin_user' => array(
                    'label' => 'Admin User',
                    'route' => 'backend/admin-user',
                    'icon_class' => 'fa fa-users',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                ),
                'admin_setting' => array(
                    'label' => 'Basic Setting',
                    'route' => 'backend/setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                )
            )
        ),
        'businessunit' => array(
            'label' => 'Business Unit',
            'icon_class' => 'fa fa-bar-chart',
            'permission' => false,
            'submenus' => array(
                'businessunit_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/businessunit/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'businessunit_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/businessunit/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'capability' => array(
            'label' => 'Capability',
            'icon_class' => 'fa fa-rocket',
            'permission' => false,
            'submenus' => array(
                'client_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/capability/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'client_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/capability/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'client' => array(
            'label' => 'Client',
            'icon_class' => 'fa fa-money',
            'permission' => false,
            'submenus' => array(
                'client_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/client/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'client_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/client/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'media_uploader' => array(
            'label' => 'Media Uploader',
            'icon_class' => 'fa fa-upload',
            'permission' => false,
            'submenus' => array(
                'media_uploader_businessunit' => array(
                    'label' => 'Business Unit',
                    'route' => 'backend/media-uploader/businessunit',
                    'icon_class' => 'fa fa-pie-chart',
                    'permission' => true,
                ),
                'media_uploader_capability' => array(
                    'label' => 'Capability',
                    'route' => 'backend/media-uploader/capability',
                    'icon_class' => 'fa fa-paper-plane',
                    'permission' => true,
                ),
                'media_uploader_client' => array(
                    'label' => 'Client',
                    'route' => 'backend/media-uploader/client',
                    'icon_class' => 'fa fa-user',
                    'permission' => true,
                ),
                'media_uploader_news' => array(
                    'label' => 'News',
                    'route' => 'backend/media-uploader/news',
                    'icon_class' => 'fa fa-indent',
                    'permission' => true,
                ),
                'media_uploader_portfolio' => array(
                    'label' => 'Portfolio',
                    'route' => 'backend/media-uploader/portfolio',
                    'icon_class' => 'fa fa-folder-open',
                    'permission' => true,
                ),
                'media_uploader_photo' => array(
                    'label' => 'Photo',
                    'route' => 'backend/media-uploader/photo',
                    'icon_class' => 'fa fa-photo',
                    'permission' => true,
                ),
                'media_uploader_video' => array(
                    'label' => 'Video',
                    'route' => 'backend/media-uploader/video',
                    'icon_class' => 'fa fa-video-camera',
                    'permission' => true,
                )
            ),
        ),
        'news_management' => array(
            'label' => 'News Management',
            'icon_class' => 'fa fa-archive',
            'permission' => false,
            'submenus' => array(
                'news' => array(
                    'label' => 'Organize',
                    'route' => 'backend/news/',
                    'icon_class' => 'fa fa-newspaper-o',
                    'permission' => true,
                ),
                'news_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/news/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )
        ),
        'ourpeople' => array(
            'label' => 'Our People',
            'icon_class' => 'fa fa-user',
            'permission' => false,
            'submenus' => array(
                'ourpeople' => array(
                    'label' => 'Organize',
                    'route' => 'backend/ourpeople/',
                    'icon_class' => 'fa fa-quote-right',
                    'permission' => true,
                )
            )
        ),
        'pages' => array(
            'label' => 'Pages',
            'route' => 'backend/pages',
            'icon_class' => 'fa fa-sitemap',
            'permission' => true,
        ),
        'photo' => array(
            'label' => 'Photo Management',
            'icon_class' => 'fa fa-picture-o',
            'permission' => false,
            'submenus' => array(
                'photo_organizer' => array(
                    'label' => 'Photo Album',
                    'route' => 'backend/photo/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'photo_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/photo/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'photo_detail' => array(
                    'label' => 'Album Images',
                    'route' => 'backend/photo/detail',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'portfolio' => array(
            'label' => 'Portfolio',
            'icon_class' => 'fa fa-suitcase',
            'permission' => false,
            'submenus' => array(
                'portfolio_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/portfolio/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'portfolio_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/portfolio/image',
                    'icon_class' => 'fa fa-camera',
                    'permission' => true,
                ),
                'portfolio_detail' => array(
                    'label' => 'Detail',
                    'route' => 'backend/portfolio/detail',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'preface' => array(
            'label' => 'Preface',
            'icon_class' => 'fa fa-bullhorn',
            'permission' => false,
            'submenus' => array(
                'preface' => array(
                    'label' => 'Organize',
                    'route' => 'backend/preface/',
                    'icon_class' => 'fa fa-quote-right',
                    'permission' => true,
                )
            )
        ),
        'video' => array(
            'label' => 'Video Management',
            'icon_class' => 'fa fa-video-camera',
            'permission' => false,
            'submenus' => array(
                'video_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/video/',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'video_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/video/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
    ),
);
