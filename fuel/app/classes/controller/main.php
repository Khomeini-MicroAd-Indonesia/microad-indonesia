<?php

class Controller_Main extends Controller {

    public function action_index() {
        return Response::forge(View::forge('main/index.twig'));
    }
    
    public function action_404() {
        return Response::forge(View::forge('main/404.twig'));
    }

}
