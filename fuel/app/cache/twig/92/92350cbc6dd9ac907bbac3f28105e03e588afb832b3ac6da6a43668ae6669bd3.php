<?php

/* news.twig */
class __TwigTemplate_fadb19cd9bf7c1c3eddfeecd4bc444d8e8ce207af33772fe97da316c8b311820 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 22
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 25
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 29
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 35
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div class=\"detail-news\">
";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_detail"]) ? $context["news_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            echo " 
    <div class=\"news-title\">
        <div class=\"small-offset-2 small-7 columns\">
            <h1>
                ";
            // line 45
            $context["title"] = twig_split_filter($this->env, $this->getAttribute($context["news"], "highlight", array()), ",");
            // line 46
            echo "                ";
            echo $this->getAttribute((isset($context["title"]) ? $context["title"] : null), 0, array(), "array");
            echo "
            </h1>
        </div>
        <div style=\"clear: both;\"></div>
    </div>
    <div class=\"news-date\">
        <div class=\"small-offset-2 small-7 columns\">
            <img src=\"";
            // line 53
            echo Uri::base();
            echo "/assets/css/images/time-icon.png\"/> ";
            echo $this->getAttribute($context["news"], "date", array());
            echo "
        </div>
        <div style=\"clear: both;\"></div>
    </div>
    <div class=\"news-headline\">
        <div class=\"small-offset-3 small-6 columns\">
            ";
            // line 59
            echo $this->getAttribute($context["news"], "highlight", array());
            echo "
        </div>
        <div style=\"clear: both;\"></div>
    </div>
    <div class=\"news-image\">
        <div class=\"ni-wrapper\">
            <div id=\"component\" class=\"component component-fullwidth fxFortuneWheel\">
                <ul class=\"itemwrap\">
                ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["news_image"]) ? $context["news_image"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                echo "    
                    <li class=\"";
                // line 68
                echo (($this->getAttribute($context["loop"], "first", array())) ? ("current") : (""));
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/news/";
                echo $this->getAttribute($context["image"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["news"], "image_name", array());
                echo "\"/></li>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                </ul>
                <nav>
                    <a class=\"prev\" href=\"#\"><img src=\"";
            // line 72
            echo Uri::base();
            echo "assets/css/images/news-prev.png\"/></a>
                    <a class=\"next\" href=\"#\"><img src=\"";
            // line 73
            echo Uri::base();
            echo "assets/css/images/news-next.png\"/></a>
                </nav>
            </div>
        </div>
    </div>
    <div class=\"news-content\">
        <div class=\"small-offset-3 small-6 columns\">
            ";
            // line 80
            echo $this->getAttribute($context["news"], "content", array());
            echo "
        </div>
        <div style=\"clear: both;\"></div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 90
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-culture\">
            <a href=\"";
        // line 98
        echo Uri::base();
        echo "culture\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"";
        // line 101
        echo Uri::base();
        echo "business/#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 112
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 121
        echo "
<script>

    \$('#component').css('height',\$('.component-fullwidth li img').height());
</script>

</body>
</html>

";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 16
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/component_1.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/fxfullwidth.css");
        echo "
        ";
        // line 19
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 22
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 23
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 25
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 26
        echo "    ";
    }

    // line 27
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 28
        echo "    ";
    }

    // line 112
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 113
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 114
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 115
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 116
        echo Asset::js("classie.js");
        echo "
    ";
        // line 117
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 118
        echo Asset::js("main.js");
        echo "
    ";
        // line 119
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 119,  326 => 118,  322 => 117,  318 => 116,  314 => 115,  310 => 114,  305 => 113,  302 => 112,  298 => 28,  295 => 27,  291 => 26,  288 => 25,  281 => 23,  278 => 22,  272 => 20,  268 => 19,  264 => 18,  260 => 17,  256 => 16,  252 => 15,  247 => 14,  244 => 13,  231 => 121,  229 => 112,  215 => 101,  209 => 98,  196 => 90,  189 => 85,  178 => 80,  168 => 73,  164 => 72,  160 => 70,  138 => 68,  119 => 67,  108 => 59,  97 => 53,  86 => 46,  84 => 45,  75 => 41,  66 => 35,  58 => 29,  55 => 27,  52 => 25,  49 => 22,  47 => 13,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/component_1.css') | raw }}*/
/*         {{ asset_css('custom/fxfullwidth.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div class="detail-news">*/
/* {% for news in news_detail %} */
/*     <div class="news-title">*/
/*         <div class="small-offset-2 small-7 columns">*/
/*             <h1>*/
/*                 {% set title = news.highlight | split(',') %}*/
/*                 {{ title[0] }}*/
/*             </h1>*/
/*         </div>*/
/*         <div style="clear: both;"></div>*/
/*     </div>*/
/*     <div class="news-date">*/
/*         <div class="small-offset-2 small-7 columns">*/
/*             <img src="{{ base_url() }}/assets/css/images/time-icon.png"/> {{ news.date }}*/
/*         </div>*/
/*         <div style="clear: both;"></div>*/
/*     </div>*/
/*     <div class="news-headline">*/
/*         <div class="small-offset-3 small-6 columns">*/
/*             {{ news.highlight }}*/
/*         </div>*/
/*         <div style="clear: both;"></div>*/
/*     </div>*/
/*     <div class="news-image">*/
/*         <div class="ni-wrapper">*/
/*             <div id="component" class="component component-fullwidth fxFortuneWheel">*/
/*                 <ul class="itemwrap">*/
/*                 {% for image in news_image %}    */
/*                     <li class="{{ loop.first ? 'current' : '' }}"><img src="{{ base_url() }}media/news/{{ image.image }}" alt="{{ news.image_name }}"/></li>*/
/*                 {% endfor %}*/
/*                 </ul>*/
/*                 <nav>*/
/*                     <a class="prev" href="#"><img src="{{ base_url() }}assets/css/images/news-prev.png"/></a>*/
/*                     <a class="next" href="#"><img src="{{ base_url() }}assets/css/images/news-next.png"/></a>*/
/*                 </nav>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="news-content">*/
/*         <div class="small-offset-3 small-6 columns">*/
/*             {{ news.content }}*/
/*         </div>*/
/*         <div style="clear: both;"></div>*/
/*     </div>*/
/* {% endfor %}*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}culture">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="{{ base_url() }}business/#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('classie.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('main.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/* */
/*     $('#component').css('height',$('.component-fullwidth li img').height());*/
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
