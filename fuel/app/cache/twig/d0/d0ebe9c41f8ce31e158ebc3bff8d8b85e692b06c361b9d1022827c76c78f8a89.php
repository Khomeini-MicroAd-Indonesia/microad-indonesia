<?php

/* welcome.twig */
class __TwigTemplate_1b06602235ea5e40db85957ba975b03e8dbba84ffc679d665e1224a0453afd61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("backend/template.twig", "welcome.twig", 1);
        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tDashboard
\t\t<small>Welcome</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li class=\"active\">Dashboard</li>
\t</ol>
</section>
";
    }

    // line 18
    public function block_backend_content($context, array $blocks = array())
    {
        // line 19
        echo "\t<h4>WELCOME</h4>
\t<div>
\t\tThis is Admin Page For Manage this site - Ganbate!
\t</div>
";
    }

    public function getTemplateName()
    {
        return "welcome.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 19,  53 => 18,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "backend/template.twig" %}*/
/* */
/* {% block backend_content_header %}*/
/* <!-- Content Header (Page header) -->*/
/* <section class="content-header">*/
/* 	<h1>{# Set page title and subtitle manually #}*/
/* 		Dashboard*/
/* 		<small>Welcome</small>*/
/* 	</h1>*/
/* 	{# Also set breadcrumb manually #}*/
/* 	<ol class="breadcrumb">*/
/* 		<li><a href="{{ base_url() }}backend">Home</a></li>*/
/* 		<li class="active">Dashboard</li>*/
/* 	</ol>*/
/* </section>*/
/* {% endblock %}*/
/* */
/* {% block backend_content %}*/
/* 	<h4>WELCOME</h4>*/
/* 	<div>*/
/* 		This is Admin Page For Manage this site - Ganbate!*/
/* 	</div>*/
/* {% endblock %}*/
/* */
