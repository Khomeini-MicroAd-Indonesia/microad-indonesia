<?php

/* contact_us.twig */
class __TwigTemplate_c806f74eea206e067d4420712a0f370721bd445e6a60fd3a9e0359ff94215aae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    <html>
    <body>
    <div>
        <p>Dear PT. MicroAd Indonesia's administrator,</p>
        <p>Visitor with details below, left a message: </p>
        <p>Name : ";
        // line 7
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "</p>
        <p>Email : ";
        // line 8
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "</p>
        <p>Message: ";
        // line 9
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "</p>
    </div>
    </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "contact_us.twig";
    }

    public function getDebugInfo()
    {
        return array (  41 => 9,  37 => 8,  33 => 7,  26 => 2,  20 => 1,);
    }
}
/* {% block frontend_content %}*/
/*     <html>*/
/*     <body>*/
/*     <div>*/
/*         <p>Dear PT. MicroAd Indonesia's administrator,</p>*/
/*         <p>Visitor with details below, left a message: </p>*/
/*         <p>Name : {{ name }}</p>*/
/*         <p>Email : {{ email }}</p>*/
/*         <p>Message: {{ message }}</p>*/
/*     </div>*/
/*     </body>*/
/*     </html>*/
/* {% endblock %}*/
