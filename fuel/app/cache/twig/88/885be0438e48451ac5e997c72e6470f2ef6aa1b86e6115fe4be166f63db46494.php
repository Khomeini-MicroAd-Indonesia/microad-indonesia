<?php

/* business.twig */
class __TwigTemplate_199712a692d4f45ef66958319018a6c5b80be262334419c7457119bd528d1a6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 29
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 31
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 37
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div id=\"container\" class=\"container intro-effect-sliced\">
    <div class=\"business-banner header\">
        <div class=\"bb-image bg-img\">
            <img src=\"";
        // line 45
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/business-banner.jpg\"/>
        </div>
        <div class=\"bb-title\">
            <div class=\"small-12 columns\">
                <h1>Business</h1>
            </div>
            <div class=\"small-12 columns\">
                <div class=\"white-box\"></div>
            </div>
            <div class=\"small-12 medium-6 columns\">
                <div class=\"chord\">
                    Challenge, Humble, Open<br/>
                    Respect, Discipline.
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"bb-intro\" id=\"about\">
            <div class=\"small-12 medium-7 columns bbi-image no-padding\">
                <div style=\"height: 126px;\"></div>
                <img src=\"";
        // line 65
        echo Uri::base();
        echo "assets/css/images/microad-office.jpg\"/>
                <img src=\"";
        // line 66
        echo Uri::base();
        echo "assets/css/images/mozaik-top.jpg\"/>
            </div>
            <div class=\"small-12 medium-5 columns\">
                <div style=\"height: 70px;\"></div>
                <div class=\"bbi-title\">
                    <span>MicroAd</span>
                    <hr/>
                    Indonesia
                </div>
            ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["preface_content"]) ? $context["preface_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["preface"]) {
            echo "    
                <div class=\"bbi-content\">
                    <p>
                        ";
            // line 78
            echo $this->getAttribute($context["preface"], "content", array());
            echo "
                    </p>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preface'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "                <div class=\"sign\">
                    <img src=\"";
        // line 83
        echo Uri::base();
        echo "/assets/css/images/sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"bb-image bg-img\">
            <img src=\"";
        // line 89
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/business-banner.jpg\"/>
        </div>
    </div>
    <div style=\"height: 100px;\"></div>
    <div class=\"content\">
        <div class=\"project-wrapper\" id=\"project\">
            <div class=\"small-12 columns no-padding\">
                <div class=\"project\">
                    <div class=\"pro-title-wrapper\">
                        <div class=\"pro-title\">
                            <div class=\"pt-item\">
                                Portfolio
                            </div>
                        </div>
                    </div>
                    <div class=\"slider1\">
                    ";
        // line 105
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["portfolio_data"]) ? $context["portfolio_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["portfolio"]) {
            // line 106
            echo "                        <div class=\"slide\">
                            <a href=\"";
            // line 107
            echo Uri::base();
            echo "business/project/";
            echo $this->getAttribute($context["portfolio"], "slug", array());
            echo "\">
                                <div class=\"small-12 columns symbol\">
                                    <img src=\"";
            // line 109
            echo Uri::base();
            echo "assets/css/images/symbol.png\"/>
                                </div>
                                <div class=\"small-12 columns ss-content\">
                                    <div class=\"ss-port\">
                                        ";
            // line 113
            echo $this->getAttribute($context["portfolio"], "client", array());
            echo "
                                    </div>
                                    ";
            // line 116
            echo "                                </div>
                                <div style=\"clear: both;\"></div>
                            </a>
                        </div> 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['portfolio'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                    </div>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"partner\" id=\"client\">
            <div class=\"small-12 medium-2 columns p-title\">
                <div class=\"pt-left\">
                    They Trust Us
                </div>
            </div>
            <div class=\"small-12 medium-10 columns\">
                <div class=\"small-up-1 medium-up-2 large-up-4\">
                ";
        // line 134
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["client_logo"]) ? $context["client_logo"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 135
            echo "                    <div class=\"column\">
                        <a href=\"";
            // line 136
            echo $this->getAttribute($context["client"], "url", array());
            echo "\"><img src=\"";
            echo Uri::base();
            echo "media/client/";
            echo $this->getAttribute($context["client"], "image", array());
            echo "\" class=\"thumbnail\" alt=\"\"></a>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 139
        echo "                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"back-sign\">
            <div class=\"small-12 medium-offset-2 medium-10 columns\">
                <div class=\"small-12 medium-3 columns\" style=\"text-align: center;\">
                    <img src=\"";
        // line 146
        echo Uri::base();
        echo "/assets/css/images/back-sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"capabilities\" data-equalizer=\"cap\" id=\"capab\">
            <div class=\"small-12 medium-6 columns no-padding\" data-equalizer-watch=\"cap\">
                <div class=\"cap-wrap-left\">
                    <div class=\"small-12 medium-7 columns no-padding relative\">
                        <div class=\"cap-image\">
                            <img src=\"";
        // line 156
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/capabilities-image.jpg\"/>
                        </div>
                        <div class=\"cap-tag\">
                            ";
        // line 165
        echo "                        </div>
                    </div>
                    <div class=\"small-12 medium-5 columns no-padding relative\">
                        <div class=\"cap-title\">
                            <h3>CAPABILITIES</h3>
                        </div>
                        <div class=\"cap-slide\">
                            <div class=\"cs-item\">
                            ";
        // line 173
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["capability_data"]) ? $context["capability_data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["capability"]) {
            // line 174
            echo "                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"";
            // line 175
            echo ($this->getAttribute($context["loop"], "index", array()) - 1);
            echo "\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
            // line 177
            echo Uri::base();
            echo "media/capability/";
            echo $this->getAttribute($context["capability"], "image", array());
            echo "\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['capability'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 182
        echo "                                <div style=\"clear: both;\"></div>
                            </div>
                        </div>
                        <div class=\"cap-arrow\">
                            <span id=\"slider-prev\"></span>
                            <span id=\"slider-next\"></span>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                    <div class=\"just-image\">
                        <img src=\"";
        // line 192
        echo Uri::base();
        echo "/assets/css/images/cap-image-example.jpg\"/>
                    </div>
                </div>
            </div>
            <div class=\"small-12 medium-6 columns cap-desc-wrap\" data-equalizer-watch=\"cap\">
                <div class=\"cap-desc\">
                    <div class=\"cd-item\">
                    ";
        // line 199
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["capability_data"]) ? $context["capability_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["capability"]) {
            // line 200
            echo "                        <div class=\"cd-content\">
                            <div class=\"cdc-item\">
                                <h3>";
            // line 202
            echo $this->getAttribute($context["capability"], "name", array());
            echo "</h3>
                                <img src=\"";
            // line 203
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "assets/css/images/business-dot.png\"/>
                                <p>
                                    ";
            // line 205
            echo $this->getAttribute($context["capability"], "desc", array());
            echo "
                                </p>
                            </div>       
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['capability'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 210
        echo "                    </div>
                </div>
                <div class=\"sign\">
                    <img src=\"";
        // line 213
        echo Uri::base();
        echo "/assets/css/images/sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"people\" data-equalizer=\"people\" id=\"peop\">
            <div class=\"small-12 medium-6 columns no-padding people-desc\" data-equalizer-watch=\"people\" >
                <div class=\"pd-desc\">
                    ";
        // line 221
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ourpeople_content"]) ? $context["ourpeople_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ourpeople"]) {
            // line 222
            echo "                        ";
            echo $this->getAttribute($context["ourpeople"], "content1", array());
            echo "
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ourpeople'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 224
        echo "                </div>
                <div class=\"pd-image\">
                    <img src=\"";
        // line 226
        echo Uri::base();
        echo "/assets/css/images/city-bottom.jpg\" style=\"width: 100%;\"/>
                </div>
            </div>
            <div class=\"small-12 medium-6 columns people-title\" data-equalizer-watch=\"people\">
                <div class=\"pd-title\">
                    Our <br/> People
                </div>
                <div class=\"pd-line\"></div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"mai-member\">
            <div class=\"small-12-columns no-padding\">
                <img src=\"";
        // line 239
        echo Uri::base();
        echo "/assets/css/images/mai-member.jpg\" style=\"width: 100%;\"/>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"people-subhead\" data-equalizer=\"desc\">
            <div class=\"small-12 medium-6 columns no-padding\" data-equalizer-watch=\"desc\">
                <img src=\"";
        // line 245
        echo Uri::base();
        echo "/assets/css/images/after-people.jpg\" style=\"width: 100%;\"/>
            </div>
            <div class=\"small-12 medium-6 columns no-padding\">
                <div class=\"small-10 columns people-detail\" data-equalizer-watch=\"desc\">
                    ";
        // line 249
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ourpeople_content"]) ? $context["ourpeople_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ourpeople"]) {
            // line 250
            echo "                        ";
            echo $this->getAttribute($context["ourpeople"], "content2", array());
            echo "
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ourpeople'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 252
        echo "                </div>
                <div style=\"clear: both;\"></div>
                <div class=\"back-sign\">
                    <img src=\"";
        // line 255
        echo Uri::base();
        echo "/assets/css/images/back-sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>

        <div class=\"product\" id=\"platform\">
            <div class=\"product-title\">
                <div class=\"small-12 medium-11 medium-offset-1 columns p-title\">
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"p-items\">
                <div class=\"pi-blog\">
                    <div class=\"small-12 medium-1 columns\">
                        <div style=\"opacity: 0;\">They Trust Us</div>
                    </div>
                ";
        // line 272
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["businessunit_data"]) ? $context["businessunit_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["businessunit"]) {
            // line 273
            echo "                    <div class=\"small-12 medium-5 columns\">
                        <div class=\"small-12 medium-2 columns no-padding pib-title\">
                            ";
            // line 275
            echo $this->getAttribute($context["businessunit"], "name", array());
            echo "
                        </div>
                        <div style=\"clear: both;\"></div>
                        <div class=\"small-12 medium-2 columns no-padding\">
                            <img src=\"";
            // line 279
            echo Uri::base();
            echo "assets/css/images/logo-bblog.png\"/>
                        </div>
                        <div class=\"small-12 medium-10 columns pib-content\">
                            ";
            // line 282
            echo $this->getAttribute($context["businessunit"], "desc", array());
            echo "
                        </div>
                    </div>
                    <div class=\"small-12 medium-6 columns\">
                        <img src=\"";
            // line 286
            echo Uri::base();
            echo "media/businessunit/";
            echo $this->getAttribute($context["businessunit"], "image", array());
            echo "\" style=\"width: 100%;\"/>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['businessunit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 289
        echo "                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
        <div class=\"contact-wrap\" id=\"contact\">
            <div class=\"contact-wrapper\">
                <div class=\"cw-excerpt\">
                    <div class=\"small-12 medium-offset-1 medium-3 columns\">We’re very approachable and would love to speak to you. Feel free to call, send us an email, simply complete the enquiry form.</div>
                    <div style=\"clear: both;\"></div>
                </div>
                <div class=\"cw-content\">
                    <div class=\"small-12 medium-offset-1 medium-4 columns\">
                        <div class=\"cwc-step\">
                            <div class=\"small-12 columns no-padding\">
                                <div class=\"cwc-dropcap\">01/</div>
                                <div>Get<br/> in Touch</div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                        <div class=\"cwc-detail\">
                            PT. MicroAd Indonesia<br/>
                            Indosurya Plaza/Thamrin Nine, Floor 3A<br/>
                            Jl. M.H. Thamrin No. 8 - 9<br/>
                            Jakarta Pusat 10230 - Indonesia<br/>
                            <br/>
                            <br/>
                            P : +6221 293 88488<br/>
                            F : +6221 293 88489<br/>
                            <a href=\"mailto:";
        // line 317
        echo Config::get("config_basic.contact_us_email_to");
        echo "\">";
        echo Config::get("config_basic.contact_us_email_to");
        echo "</a><br/>
                        </div>
                    </div>
                    <div class=\"small-12 medium-offset-3 medium-3 end columns\">
                        <div class=\"cwc-step\">
                            <div class=\"small-12 columns no-padding\">
                                <div class=\"cwc-dropcap\">02/</div>
                                <div>Send Us<br/> A Message</div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                        <div class=\"cwc-form\">
                            <form action=\"\" method=\"post\" data-abide=\"ajax\" id=\"contact-form\">
                                <div class=\"small-12 columns no-padding ";
        // line 330
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "name", array())) > 0)) ? ("error") : (""));
        echo "\">
                                    <input type=\"text\" id=\"name\" name=\"name\" placeholder=\"name\" value=\"";
        // line 331
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "name", array());
        echo "\">
                                </div>
                                <div class=\"small-12 columns no-padding ";
        // line 333
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "email", array())) > 0)) ? ("error") : (""));
        echo "\" value=\"";
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "email", array());
        echo "\">
                                    <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"email\">
                                </div>
                                <div class=\"small-12 columns no-padding ";
        // line 336
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "message", array())) > 0)) ? ("error") : (""));
        echo "\">
                                    <textarea id=\"message\" name=\"message\" placeholder=\"message\">";
        // line 337
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "message", array());
        echo "</textarea>
                                </div>
                                <div class=\"small-12 columns no-padding\">
                                    <div class=\"g-recaptcha\" data-sitekey=\"6LdFxB0TAAAAAH9aDoQwpdwUzYEePO0QhJAV-m1G\"></div>
                                </div>
                                <div>
                                    ";
        // line 343
        echo (isset($context["contact_success_message"]) ? $context["contact_success_message"] : null);
        echo "
                                </div>
                                <div class=\"small-12 columns no-padding text-right\">
                                    <button type=\"send\">Submit</button>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </form>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 362
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-item nm-normal\">
            <a id=\"aboutButton\" href=\"#about\">About</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"portButton\" href=\"#project\">Portfolio</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"clientButton\" href=\"#client\">Client</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"capButton\" href=\"#capab\">Capabilities</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"peopButton\" href=\"#peop\">People</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"platformButton\" href=\"#platform\">Platform</a>
        </div>
        <div class=\"nm-culture\">
            <a href=\"";
        // line 388
        echo Uri::base();
        echo "culture\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 402
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 413
        echo "
<script>
    \$('.slider1').bxSlider({
        slideWidth: 216,
        minSlides: 1,
        maxSlides: 5,
        moveSlides: 1,
        auto:true,
        pager:true,
        slideMargin: 100
    });
   ";
        // line 436
        echo "
    \$('.contact-wrapper').css('height',\$( window ).height());
    \$( window ).resize(function() {
        \$('.contact-wrapper').css('height',\$( window ).height());
    });

    (function(\$){

        var jump=function(e)
        {
            if (e){
                e.preventDefault();
                var target = \$(this).attr(\"href\");
            }else{
                var target = location.hash;
            }

            \$('html,body').animate(
                    {
                        scrollTop: \$(target).offset().top
                    },1000,function()
                    {
                        location.hash = target;
                    });

        }

        \$('html, body').hide()

        \$(document).ready(function()
        {
            \$('a[href^=\\\\#]').bind(\"click\", jump);

            if (location.hash){
                setTimeout(function(){
                    \$('html, body').scrollTop(0).show()
                    jump()
                }, 0);
            }else{
                \$('html, body').show()
            }
        });

    })(jQuery)
</script>
</body>
</html>

";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 16
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/component.css");
        echo "
        ";
        // line 19
        echo Asset::css("jquery.bxslider.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/slick.css");
        echo "
        ";
        // line 21
        echo Asset::css("custom/slick-theme.css");
        echo "
        ";
        // line 22
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 24
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 25
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 27
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 28
        echo "    ";
    }

    // line 29
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 30
        echo "    ";
    }

    // line 402
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 403
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 404
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 405
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 406
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 407
        echo Asset::js("classie.js");
        echo "
    ";
        // line 408
        echo Asset::js("jquery.easing.1.3.js");
        echo "
    ";
        // line 409
        echo Asset::js("jquery.bxslider.min.js");
        echo "
    ";
        // line 410
        echo Asset::js("slick.min.js");
        echo "
    ";
        // line 411
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "business.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  829 => 411,  825 => 410,  821 => 409,  817 => 408,  813 => 407,  809 => 406,  805 => 405,  801 => 404,  796 => 403,  793 => 402,  789 => 30,  786 => 29,  782 => 28,  779 => 27,  772 => 25,  769 => 24,  763 => 22,  759 => 21,  755 => 20,  751 => 19,  747 => 18,  743 => 17,  739 => 16,  735 => 15,  730 => 14,  727 => 13,  675 => 436,  662 => 413,  660 => 402,  643 => 388,  612 => 362,  590 => 343,  581 => 337,  577 => 336,  569 => 333,  564 => 331,  560 => 330,  542 => 317,  512 => 289,  501 => 286,  494 => 282,  488 => 279,  481 => 275,  477 => 273,  473 => 272,  453 => 255,  448 => 252,  439 => 250,  435 => 249,  428 => 245,  419 => 239,  403 => 226,  399 => 224,  390 => 222,  386 => 221,  375 => 213,  370 => 210,  359 => 205,  354 => 203,  350 => 202,  346 => 200,  342 => 199,  332 => 192,  320 => 182,  299 => 177,  294 => 175,  291 => 174,  274 => 173,  264 => 165,  258 => 156,  245 => 146,  236 => 139,  223 => 136,  220 => 135,  216 => 134,  201 => 121,  191 => 116,  186 => 113,  179 => 109,  172 => 107,  169 => 106,  165 => 105,  146 => 89,  137 => 83,  134 => 82,  124 => 78,  116 => 75,  104 => 66,  100 => 65,  77 => 45,  66 => 37,  58 => 31,  55 => 29,  52 => 27,  49 => 24,  47 => 13,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/component.css') | raw }}*/
/*         {{ asset_css('jquery.bxslider.css') | raw }}*/
/*         {{ asset_css('custom/slick.css') | raw }}*/
/*         {{ asset_css('custom/slick-theme.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div id="container" class="container intro-effect-sliced">*/
/*     <div class="business-banner header">*/
/*         <div class="bb-image bg-img">*/
/*             <img src="{{ base_url }}assets/css/images/business-banner.jpg"/>*/
/*         </div>*/
/*         <div class="bb-title">*/
/*             <div class="small-12 columns">*/
/*                 <h1>Business</h1>*/
/*             </div>*/
/*             <div class="small-12 columns">*/
/*                 <div class="white-box"></div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns">*/
/*                 <div class="chord">*/
/*                     Challenge, Humble, Open<br/>*/
/*                     Respect, Discipline.*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="bb-intro" id="about">*/
/*             <div class="small-12 medium-7 columns bbi-image no-padding">*/
/*                 <div style="height: 126px;"></div>*/
/*                 <img src="{{ base_url() }}assets/css/images/microad-office.jpg"/>*/
/*                 <img src="{{ base_url() }}assets/css/images/mozaik-top.jpg"/>*/
/*             </div>*/
/*             <div class="small-12 medium-5 columns">*/
/*                 <div style="height: 70px;"></div>*/
/*                 <div class="bbi-title">*/
/*                     <span>MicroAd</span>*/
/*                     <hr/>*/
/*                     Indonesia*/
/*                 </div>*/
/*             {% for preface in preface_content %}    */
/*                 <div class="bbi-content">*/
/*                     <p>*/
/*                         {{ preface.content }}*/
/*                     </p>*/
/*                 </div>*/
/*             {% endfor %}*/
/*                 <div class="sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="bb-image bg-img">*/
/*             <img src="{{ base_url }}assets/css/images/business-banner.jpg"/>*/
/*         </div>*/
/*     </div>*/
/*     <div style="height: 100px;"></div>*/
/*     <div class="content">*/
/*         <div class="project-wrapper" id="project">*/
/*             <div class="small-12 columns no-padding">*/
/*                 <div class="project">*/
/*                     <div class="pro-title-wrapper">*/
/*                         <div class="pro-title">*/
/*                             <div class="pt-item">*/
/*                                 Portfolio*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="slider1">*/
/*                     {% for portfolio in portfolio_data %}*/
/*                         <div class="slide">*/
/*                             <a href="{{ base_url() }}business/project/{{ portfolio.slug }}">*/
/*                                 <div class="small-12 columns symbol">*/
/*                                     <img src="{{ base_url() }}assets/css/images/symbol.png"/>*/
/*                                 </div>*/
/*                                 <div class="small-12 columns ss-content">*/
/*                                     <div class="ss-port">*/
/*                                         {{ portfolio.client }}*/
/*                                     </div>*/
/*                                     {#<img src="{{ base_url() }}media/portfolio/{{ portfolio.image }}"/>#}*/
/*                                 </div>*/
/*                                 <div style="clear: both;"></div>*/
/*                             </a>*/
/*                         </div> */
/*                     {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="partner" id="client">*/
/*             <div class="small-12 medium-2 columns p-title">*/
/*                 <div class="pt-left">*/
/*                     They Trust Us*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-10 columns">*/
/*                 <div class="small-up-1 medium-up-2 large-up-4">*/
/*                 {% for client in client_logo %}*/
/*                     <div class="column">*/
/*                         <a href="{{ client.url }}"><img src="{{ base_url() }}media/client/{{ client.image }}" class="thumbnail" alt=""></a>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="back-sign">*/
/*             <div class="small-12 medium-offset-2 medium-10 columns">*/
/*                 <div class="small-12 medium-3 columns" style="text-align: center;">*/
/*                     <img src="{{ base_url() }}/assets/css/images/back-sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="capabilities" data-equalizer="cap" id="capab">*/
/*             <div class="small-12 medium-6 columns no-padding" data-equalizer-watch="cap">*/
/*                 <div class="cap-wrap-left">*/
/*                     <div class="small-12 medium-7 columns no-padding relative">*/
/*                         <div class="cap-image">*/
/*                             <img src="{{ base_url }}assets/css/images/capabilities-image.jpg"/>*/
/*                         </div>*/
/*                         <div class="cap-tag">*/
/*                             {##Integrated Digital Marketing Campaign<br/>*/
/*                             #Web & Mobile Apps Development<br/>*/
/*                             #Social Media & Viral Marketing<br/>*/
/*                             #Search Engine Optimization<br/>*/
/*                             #Digital Ads Placement<br/>*/
/*                             #Ad Platforms#}*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-5 columns no-padding relative">*/
/*                         <div class="cap-title">*/
/*                             <h3>CAPABILITIES</h3>*/
/*                         </div>*/
/*                         <div class="cap-slide">*/
/*                             <div class="cs-item">*/
/*                             {% for capability in capability_data %}*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="{{ loop.index - 1 }}" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/{{ capability.image }}" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                             {% endfor %}*/
/*                                 <div style="clear: both;"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="cap-arrow">*/
/*                             <span id="slider-prev"></span>*/
/*                             <span id="slider-next"></span>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                     <div class="just-image">*/
/*                         <img src="{{ base_url() }}/assets/css/images/cap-image-example.jpg"/>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns cap-desc-wrap" data-equalizer-watch="cap">*/
/*                 <div class="cap-desc">*/
/*                     <div class="cd-item">*/
/*                     {% for capability in capability_data %}*/
/*                         <div class="cd-content">*/
/*                             <div class="cdc-item">*/
/*                                 <h3>{{ capability.name }}</h3>*/
/*                                 <img src="{{ base_url }}assets/css/images/business-dot.png"/>*/
/*                                 <p>*/
/*                                     {{ capability.desc }}*/
/*                                 </p>*/
/*                             </div>       */
/*                         </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="people" data-equalizer="people" id="peop">*/
/*             <div class="small-12 medium-6 columns no-padding people-desc" data-equalizer-watch="people" >*/
/*                 <div class="pd-desc">*/
/*                     {% for ourpeople in ourpeople_content %}*/
/*                         {{ ourpeople.content1 }}*/
/*                     {% endfor %}*/
/*                 </div>*/
/*                 <div class="pd-image">*/
/*                     <img src="{{ base_url() }}/assets/css/images/city-bottom.jpg" style="width: 100%;"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns people-title" data-equalizer-watch="people">*/
/*                 <div class="pd-title">*/
/*                     Our <br/> People*/
/*                 </div>*/
/*                 <div class="pd-line"></div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="mai-member">*/
/*             <div class="small-12-columns no-padding">*/
/*                 <img src="{{ base_url() }}/assets/css/images/mai-member.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="people-subhead" data-equalizer="desc">*/
/*             <div class="small-12 medium-6 columns no-padding" data-equalizer-watch="desc">*/
/*                 <img src="{{ base_url() }}/assets/css/images/after-people.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns no-padding">*/
/*                 <div class="small-10 columns people-detail" data-equalizer-watch="desc">*/
/*                     {% for ourpeople in ourpeople_content %}*/
/*                         {{ ourpeople.content2 }}*/
/*                     {% endfor %}*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*                 <div class="back-sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/back-sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/* */
/*         <div class="product" id="platform">*/
/*             <div class="product-title">*/
/*                 <div class="small-12 medium-11 medium-offset-1 columns p-title">*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <div class="p-items">*/
/*                 <div class="pi-blog">*/
/*                     <div class="small-12 medium-1 columns">*/
/*                         <div style="opacity: 0;">They Trust Us</div>*/
/*                     </div>*/
/*                 {% for businessunit in businessunit_data %}*/
/*                     <div class="small-12 medium-5 columns">*/
/*                         <div class="small-12 medium-2 columns no-padding pib-title">*/
/*                             {{ businessunit.name }}*/
/*                         </div>*/
/*                         <div style="clear: both;"></div>*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <img src="{{ base_url() }}assets/css/images/logo-bblog.png"/>*/
/*                         </div>*/
/*                         <div class="small-12 medium-10 columns pib-content">*/
/*                             {{ businessunit.desc }}*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-6 columns">*/
/*                         <img src="{{ base_url() }}media/businessunit/{{ businessunit.image }}" style="width: 100%;"/>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="contact-wrap" id="contact">*/
/*             <div class="contact-wrapper">*/
/*                 <div class="cw-excerpt">*/
/*                     <div class="small-12 medium-offset-1 medium-3 columns">We’re very approachable and would love to speak to you. Feel free to call, send us an email, simply complete the enquiry form.</div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*                 <div class="cw-content">*/
/*                     <div class="small-12 medium-offset-1 medium-4 columns">*/
/*                         <div class="cwc-step">*/
/*                             <div class="small-12 columns no-padding">*/
/*                                 <div class="cwc-dropcap">01/</div>*/
/*                                 <div>Get<br/> in Touch</div>*/
/*                             </div>*/
/*                             <div style="clear: both;"></div>*/
/*                         </div>*/
/*                         <div class="cwc-detail">*/
/*                             PT. MicroAd Indonesia<br/>*/
/*                             Indosurya Plaza/Thamrin Nine, Floor 3A<br/>*/
/*                             Jl. M.H. Thamrin No. 8 - 9<br/>*/
/*                             Jakarta Pusat 10230 - Indonesia<br/>*/
/*                             <br/>*/
/*                             <br/>*/
/*                             P : +6221 293 88488<br/>*/
/*                             F : +6221 293 88489<br/>*/
/*                             <a href="mailto:{{ config('config_basic.contact_us_email_to') }}">{{ config('config_basic.contact_us_email_to') }}</a><br/>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-offset-3 medium-3 end columns">*/
/*                         <div class="cwc-step">*/
/*                             <div class="small-12 columns no-padding">*/
/*                                 <div class="cwc-dropcap">02/</div>*/
/*                                 <div>Send Us<br/> A Message</div>*/
/*                             </div>*/
/*                             <div style="clear: both;"></div>*/
/*                         </div>*/
/*                         <div class="cwc-form">*/
/*                             <form action="" method="post" data-abide="ajax" id="contact-form">*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.name | length > 0 ? 'error' : '' }}">*/
/*                                     <input type="text" id="name" name="name" placeholder="name" value="{{ post_data.name }}">*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.email | length > 0 ? 'error' : '' }}" value="{{ post_data.email }}">*/
/*                                     <input type="email" id="email" name="email" placeholder="email">*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.message | length > 0 ? 'error' : '' }}">*/
/*                                     <textarea id="message" name="message" placeholder="message">{{ post_data.message }}</textarea>*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding">*/
/*                                     <div class="g-recaptcha" data-sitekey="6LdFxB0TAAAAAH9aDoQwpdwUzYEePO0QhJAV-m1G"></div>*/
/*                                 </div>*/
/*                                 <div>*/
/*                                     {{ contact_success_message }}*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding text-right">*/
/*                                     <button type="send">Submit</button>*/
/*                                 </div>*/
/*                                 <div style="clear: both;"></div>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="aboutButton" href="#about">About</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="portButton" href="#project">Portfolio</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="clientButton" href="#client">Client</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="capButton" href="#capab">Capabilities</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="peopButton" href="#peop">People</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="platformButton" href="#platform">Platform</a>*/
/*         </div>*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}culture">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('classie.js') | raw }}*/
/*     {{ asset_js('jquery.easing.1.3.js') | raw }}*/
/*     {{ asset_js('jquery.bxslider.min.js') | raw }}*/
/*     {{ asset_js('slick.min.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/*     $('.slider1').bxSlider({*/
/*         slideWidth: 216,*/
/*         minSlides: 1,*/
/*         maxSlides: 5,*/
/*         moveSlides: 1,*/
/*         auto:true,*/
/*         pager:true,*/
/*         slideMargin: 100*/
/*     });*/
/*    {# $('.form-group .alert-danger').css("display","none");*/
/*         var flagCaptcha = false;*/
/* */
/*         var recaptchaCallback = function() {*/
/*             flagCaptcha = true;*/
/*     };*/
/*         */
/*     $('#contact-form').on('valid.fndtn.abide', function() {*/
/*         if (flagCaptcha) {*/
/*             $(this).submit();*/
/*         }*/
/*     });#}*/
/* */
/*     $('.contact-wrapper').css('height',$( window ).height());*/
/*     $( window ).resize(function() {*/
/*         $('.contact-wrapper').css('height',$( window ).height());*/
/*     });*/
/* */
/*     (function($){*/
/* */
/*         var jump=function(e)*/
/*         {*/
/*             if (e){*/
/*                 e.preventDefault();*/
/*                 var target = $(this).attr("href");*/
/*             }else{*/
/*                 var target = location.hash;*/
/*             }*/
/* */
/*             $('html,body').animate(*/
/*                     {*/
/*                         scrollTop: $(target).offset().top*/
/*                     },1000,function()*/
/*                     {*/
/*                         location.hash = target;*/
/*                     });*/
/* */
/*         }*/
/* */
/*         $('html, body').hide()*/
/* */
/*         $(document).ready(function()*/
/*         {*/
/*             $('a[href^=\\#]').bind("click", jump);*/
/* */
/*             if (location.hash){*/
/*                 setTimeout(function(){*/
/*                     $('html, body').scrollTop(0).show()*/
/*                     jump()*/
/*                 }, 0);*/
/*             }else{*/
/*                 $('html, body').show()*/
/*             }*/
/*         });*/
/* */
/*     })(jQuery)*/
/* </script>*/
/* </body>*/
/* </html>*/
/* */
/* */
