<?php

/* category.twig */
class __TwigTemplate_a3038b6d5ca9a45fa4ce035d66a5e0bf4a217e4c4030a4073f3896c333668f49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>

    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 22
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 25
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 29
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 35
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div class=\"detail-category\">
        <div class=\"row\">
            <div class=\"dc-title\">
                <div class=\"small-12 columns\">
                    <h1>BREAKTIME</h1>
                </div>
            </div>
            <div class=\"dc-desc\">
                <div class=\"small-12 columns\">
                    A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot.
                </div>
                <div style=\"clear: both;\"></div>
            </div>

            <div class=\"dc-image\">
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 56
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 59
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 62
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 65
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 68
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
        // line 71
        echo Uri::base();
        echo "/assets/css/images/cat-photo.jpg\" style=\"width: 100%;\">
                </div>
                <div style=\"clear: both;\"></div>
            </div>
        </div>
</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 81
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-culture\">
            <a href=\"";
        // line 89
        echo Uri::base();
        echo "culture\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 103
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 110
        echo "
<script>
</script>

</body>
</html>

";
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 19
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 22
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 23
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 25
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 26
        echo "    ";
    }

    // line 27
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 28
        echo "    ";
    }

    // line 103
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 104
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 105
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 106
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 107
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 108
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 108,  244 => 107,  240 => 106,  236 => 105,  231 => 104,  228 => 103,  224 => 28,  221 => 27,  217 => 26,  214 => 25,  207 => 23,  204 => 22,  198 => 20,  194 => 19,  190 => 18,  186 => 17,  181 => 16,  178 => 15,  167 => 110,  165 => 103,  148 => 89,  135 => 81,  122 => 71,  116 => 68,  110 => 65,  104 => 62,  98 => 59,  92 => 56,  68 => 35,  60 => 29,  57 => 27,  54 => 25,  51 => 22,  49 => 15,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>*/
/* */
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div class="detail-category">*/
/*         <div class="row">*/
/*             <div class="dc-title">*/
/*                 <div class="small-12 columns">*/
/*                     <h1>BREAKTIME</h1>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="dc-desc">*/
/*                 <div class="small-12 columns">*/
/*                     A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot.*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/* */
/*             <div class="dc-image">*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}/assets/css/images/cat-photo.jpg" style="width: 100%;">*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*         </div>*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}culture">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
