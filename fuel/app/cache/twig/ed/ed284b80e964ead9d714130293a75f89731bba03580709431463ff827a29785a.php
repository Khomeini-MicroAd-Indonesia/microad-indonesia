<?php

/* setting.twig */
class __TwigTemplate_3473b4645d4dd52c004792461504627ccfbed71263e8c6fb1ce9f5ed851806ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("backend/template.twig", "setting.twig", 1);
        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tAdmin Management
\t\t<small>Basic Setting</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Admin Management</li>
\t\t<li class=\"active\">Basic Setting</li>
\t</ol>
</section>
";
    }

    // line 19
    public function block_backend_content($context, array $blocks = array())
    {
        // line 20
        echo "
";
        // line 21
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 22
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 24
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 27
        echo "
";
        // line 28
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 29
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 31
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 34
        echo "
\t<form class=\"form-horizontal\" accept-charset=\"utf-8\" method=\"post\" action=\"\" role=\"form\" name=\"frm_basic_setting\">
\t\t";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["setting_data"]) ? $context["setting_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["setting_item"]) {
            // line 37
            echo "\t\t<div class=\"form-group\">
\t\t\t<label for=\"form_";
            // line 38
            echo $this->getAttribute($context["setting_item"], "name", array());
            echo "\" class=\"col-sm-2 control-label\">";
            echo $this->getAttribute($context["setting_item"], "label", array());
            echo "</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t";
            // line 40
            $context["form_attr"] = array("class" => "form-control", "placeholder" => $this->getAttribute($context["setting_item"], "label", array()));
            // line 41
            echo "\t\t\t\t";
            echo Form::input($this->getAttribute($context["setting_item"], "name", array()), $this->getAttribute($context["setting_item"], "value", array()), (isset($context["form_attr"]) ? $context["form_attr"] : null));
            echo "
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['setting_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-sm-offset-2 col-sm-10\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>
\t\t\t</div>
\t\t</div>
\t</form>
";
    }

    public function getTemplateName()
    {
        return "setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 45,  107 => 41,  105 => 40,  98 => 38,  95 => 37,  91 => 36,  87 => 34,  81 => 31,  77 => 29,  75 => 28,  72 => 27,  66 => 24,  62 => 22,  60 => 21,  57 => 20,  54 => 19,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "backend/template.twig" %}*/
/* */
/* {% block backend_content_header %}*/
/* <!-- Content Header (Page header) -->*/
/* <section class="content-header">*/
/* 	<h1>{# Set page title and subtitle manually #}*/
/* 		Admin Management*/
/* 		<small>Basic Setting</small>*/
/* 	</h1>*/
/* 	{# Also set breadcrumb manually #}*/
/* 	<ol class="breadcrumb">*/
/* 		<li><a href="{{ base_url() }}backend">Home</a></li>*/
/* 		<li>Admin Management</li>*/
/* 		<li class="active">Basic Setting</li>*/
/* 	</ol>*/
/* </section>*/
/* {% endblock %}*/
/* */
/* {% block backend_content %}*/
/* */
/* {% if success_message | length > 0 %}*/
/* <div class="alert alert-success alert-dismissable">*/
/* 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 	{{ success_message | raw }}*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if error_message | length > 0 %}*/
/* <div class="alert alert-danger alert-dismissable">*/
/* 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 	{{ error_message | raw }}*/
/* </div>*/
/* {% endif %}*/
/* */
/* 	<form class="form-horizontal" accept-charset="utf-8" method="post" action="" role="form" name="frm_basic_setting">*/
/* 		{% for setting_item in setting_data %}*/
/* 		<div class="form-group">*/
/* 			<label for="form_{{ setting_item.name }}" class="col-sm-2 control-label">{{ setting_item.label }}</label>*/
/* 			<div class="col-sm-10">*/
/* 				{% set form_attr = {'class':'form-control', 'placeholder':setting_item.label} %}*/
/* 				{{ form_input(setting_item.name, setting_item.value, form_attr) | raw }}*/
/* 			</div>*/
/* 		</div>*/
/* 		{% endfor %}*/
/* 		<div class="form-group">*/
/* 			<div class="col-sm-offset-2 col-sm-10">*/
/* 				<button type="submit" class="btn btn-primary">Save</button>*/
/* 			</div>*/
/* 		</div>*/
/* 	</form>*/
/* {% endblock %}*/
/* */
