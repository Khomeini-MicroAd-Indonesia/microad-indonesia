<?php

/* project.twig */
class __TwigTemplate_77d44707e800096b668d6fc2396c256705ca09d70a0d80d18e1a044379d4b9f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 20
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 23
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 25
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 27
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 33
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div class=\"detail-project\">
    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["portfolio_detail"]) ? $context["portfolio_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 40
            echo "    <div class=\"row\">
        <div class=\"dp-title\">
            <div class=\"small-12 columns\">
                <h1>";
            // line 43
            echo $this->getAttribute($context["detail"], "title", array());
            echo " </h1>
            </div>
        </div>
        <div class=\"dp-type\">
            <div class=\"small-12 columns\">
                <div class=\"dp-name\">Project:</div>
                <div class=\"dp-item\">
                    ";
            // line 50
            echo $this->getAttribute($context["detail"], "desc", array());
            echo "
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"dp-client\">
            <div class=\"small-12 columns\">
                <div class=\"dp-name\">Client:</div>
                <div class=\"dp-item\">
                    ";
            // line 59
            echo $this->getAttribute($context["detail"], "client", array());
            echo "
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"dp-date\">
            <div class=\"small-12 columns\">
                <div class=\"dp-name\">Periode:</div>
                <div class=\"dp-item\">
                    ";
            // line 68
            echo $this->getAttribute($context["detail"], "start_date", array());
            echo " - ";
            echo $this->getAttribute($context["detail"], "end_date", array());
            echo "
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"back-detail dp-client\">
            <a href=\"";
            // line 74
            echo Uri::base();
            echo "business\">Back to Business</a>
        </div>
        <div class=\"dp-image\">
            <a href=\"";
            // line 77
            echo $this->getAttribute($context["detail"], "url", array());
            echo "\">
                <img src=\"";
            // line 78
            echo Uri::base();
            echo "media/portfolio/";
            echo $this->getAttribute($context["detail"], "image", array());
            echo ".jpg\" style=\"width: 100%;\">
            </a>
        </div>
        <div class=\"back-detail dp-client back-top\">
            <a href=\"#top\">Back to Top</a>
        </div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 91
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-culture\">
            <a href=\"";
        // line 99
        echo Uri::base();
        echo "business\">Business</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 113
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 120
        echo "
<script>
    \$('.back-top a').click(function () {
        \$('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
</script>

</body>
</html>

";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 16
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 20
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 21
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 23
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 24
        echo "    ";
    }

    // line 25
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 26
        echo "    ";
    }

    // line 113
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 114
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 115
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 116
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 117
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 118
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "project.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 118,  277 => 117,  273 => 116,  269 => 115,  264 => 114,  261 => 113,  257 => 26,  254 => 25,  250 => 24,  247 => 23,  240 => 21,  237 => 20,  231 => 18,  227 => 17,  223 => 16,  219 => 15,  214 => 14,  211 => 13,  194 => 120,  192 => 113,  175 => 99,  162 => 91,  155 => 86,  139 => 78,  135 => 77,  129 => 74,  118 => 68,  106 => 59,  94 => 50,  84 => 43,  79 => 40,  75 => 39,  66 => 33,  58 => 27,  55 => 25,  52 => 23,  49 => 20,  47 => 13,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div class="detail-project">*/
/*     {% for detail in portfolio_detail %}*/
/*     <div class="row">*/
/*         <div class="dp-title">*/
/*             <div class="small-12 columns">*/
/*                 <h1>{{ detail.title }} </h1>*/
/*             </div>*/
/*         </div>*/
/*         <div class="dp-type">*/
/*             <div class="small-12 columns">*/
/*                 <div class="dp-name">Project:</div>*/
/*                 <div class="dp-item">*/
/*                     {{ detail.desc }}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="dp-client">*/
/*             <div class="small-12 columns">*/
/*                 <div class="dp-name">Client:</div>*/
/*                 <div class="dp-item">*/
/*                     {{ detail.client }}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="dp-date">*/
/*             <div class="small-12 columns">*/
/*                 <div class="dp-name">Periode:</div>*/
/*                 <div class="dp-item">*/
/*                     {{ detail.start_date }} - {{ detail.end_date }}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="back-detail dp-client">*/
/*             <a href="{{ base_url() }}business">Back to Business</a>*/
/*         </div>*/
/*         <div class="dp-image">*/
/*             <a href="{{ detail.url }}">*/
/*                 <img src="{{ base_url() }}media/portfolio/{{ detail.image }}.jpg" style="width: 100%;">*/
/*             </a>*/
/*         </div>*/
/*         <div class="back-detail dp-client back-top">*/
/*             <a href="#top">Back to Top</a>*/
/*         </div>*/
/*     </div>*/
/*     {% endfor %}*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}business">Business</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/*     $('.back-top a').click(function () {*/
/*         $('body,html').animate({*/
/*             scrollTop: 0*/
/*         }, 800);*/
/*         return false;*/
/*     });*/
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
