<?php

/* business.twig */
class __TwigTemplate_c5a983a641e2820502b05895bfad841ac791722506f893b7794fe2f3b8a77b69 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 29
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 31
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 37
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div id=\"container\" class=\"container intro-effect-sliced\">
    <div class=\"business-banner header\">
        <div class=\"bb-image bg-img\">
            <img src=\"";
        // line 45
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/business-banner.jpg\"/>
        </div>
        <div class=\"bb-title\">
            <div class=\"small-12 columns\">
                <h1>Business</h1>
            </div>
            <div class=\"small-12 columns\">
                <div class=\"white-box\"></div>
            </div>
            <div class=\"small-12 medium-6 columns\">
                <div class=\"chord\">
                    Challenge, Humble, Open<br/>
                    Respect, Discipline.
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"bb-intro\" id=\"about\">
            <div class=\"small-12 medium-7 columns bbi-image no-padding\">
                <div style=\"height: 126px;\"></div>
                <img src=\"";
        // line 65
        echo Uri::base();
        echo "assets/css/images/microad-office.jpg\"/>
                <img src=\"";
        // line 66
        echo Uri::base();
        echo "assets/css/images/mozaik-top.jpg\"/>
            </div>
            <div class=\"small-12 medium-5 columns\">
                <div style=\"height: 126px;\"></div>
                <div class=\"bbi-title\">
                    <span>MicroAd</span>
                    <hr/>
                    Indonesia
                </div>
            ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["preface_content"]) ? $context["preface_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["preface"]) {
            echo "    
                <div class=\"bbi-content\">
                    <p>
                        ";
            // line 78
            echo $this->getAttribute($context["preface"], "content", array());
            echo "
                    </p>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preface'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "                <div class=\"sign\">
                    <img src=\"";
        // line 83
        echo Uri::base();
        echo "/assets/css/images/sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"bb-image bg-img\">
            <img src=\"";
        // line 89
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/business-banner.jpg\"/>
        </div>
    </div>
    <div style=\"height: 100px;\"></div>
    <div class=\"content\">
        <div class=\"project-wrapper\" id=\"project\">
            <div class=\"small-12 columns no-padding\">
                <div class=\"project\">
                    <div class=\"pro-title-wrapper\">
                        <div class=\"pro-title\">
                            <div class=\"pt-item\">
                                Projects
                            </div>
                        </div>
                    </div>
                    <div class=\"slider1\">
                    ";
        // line 105
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["portfolio_data"]) ? $context["portfolio_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["portfolio"]) {
            // line 106
            echo "                        <div class=\"slide\">
                            <a href=\"#\">
                                <div class=\"small-12 columns symbol\">
                                    <img src=\"";
            // line 109
            echo Uri::base();
            echo "assets/css/images/symbol.png\"/>
                                </div>
                                <div class=\"small-12 columns ss-content\">
                                    ";
            // line 112
            echo $this->getAttribute($context["portfolio"], "client", array());
            echo "
                                    <img src=\"";
            // line 113
            echo Uri::base();
            echo "media/portfolio/";
            echo $this->getAttribute($context["portfolio"], "image", array());
            echo "\"/>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </a>
                        </div> 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['portfolio'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "                    </div>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"partner\">
            <div class=\"small-12 medium-2 columns p-title\">
                <div class=\"pt-left\">
                    They Trust Us
                </div>
            </div>
            <div class=\"small-12 medium-10 columns\">
                <div class=\"small-up-1 medium-up-2 large-up-4\">
                ";
        // line 132
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["client_logo"]) ? $context["client_logo"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 133
            echo "                    <div class=\"column\">
                        <img src=\"";
            // line 134
            echo Uri::base();
            echo "media/client/";
            echo $this->getAttribute($context["client"], "image", array());
            echo "\" class=\"thumbnail\" alt=\"\">
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 137
        echo "                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"back-sign\">
            <div class=\"small-12 medium-offset-2 medium-10 columns\">
                <div class=\"small-12 medium-3 columns\" style=\"text-align: center;\">
                    <img src=\"";
        // line 144
        echo Uri::base();
        echo "/assets/css/images/back-sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"capabilities\" data-equalizer=\"cap\">
            <div class=\"small-12 medium-6 columns no-padding\" data-equalizer-watch=\"cap\">
                <div class=\"cap-wrap-left\">
                    <div class=\"small-12 medium-7 columns no-padding relative\">
                        <div class=\"cap-image\">
                            <img src=\"";
        // line 154
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/capabilities-image.jpg\"/>
                        </div>
                        <div class=\"cap-tag\">
                            #Ad platform<br/>
                            #website development<br/>
                            #social media marketing<br/>
                            #Search engine optimization<br/>
                            #Search engine marketing<br/>
                            #Branding and Strategy
                        </div>
                    </div>
                    <div class=\"small-12 medium-5 columns no-padding relative\">
                        <div class=\"cap-title\">
                            <h3>Capabilities</h3>
                        </div>
                        <div class=\"cap-slide\">
                            <div class=\"cs-item\">
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"0\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 174
        echo Uri::base();
        echo "media/capability/network.png\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"1\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 181
        echo Uri::base();
        echo "media/capability/design.png\" alt=\"\" /> 
                                        </div>
                                    </a>
                                </div>
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"2\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 188
        echo Uri::base();
        echo "media/capability/social.png\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"3\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 195
        echo Uri::base();
        echo "media/capability/optimization.png\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"4\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 202
        echo Uri::base();
        echo "media/capability/marketing.png\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                                <div class=\"small-12 medium-4 columns\">
                                    <a data-slide-index=\"5\" href=\"\">
                                        <div class=\"csi-image\">
                                            <img src=\"";
        // line 209
        echo Uri::base();
        echo "media/capability/branding.png\" alt=\"\" />
                                        </div>
                                    </a>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                        </div>
                        <div class=\"cap-arrow\">
                            <span id=\"slider-prev\"></span>
                            <span id=\"slider-next\"></span>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                    <div class=\"just-image\">
                        <img src=\"";
        // line 223
        echo Uri::base();
        echo "/assets/css/images/cap-image-example.jpg\"/>
                    </div>
                </div>
            </div>
            <div class=\"small-12 medium-6 columns cap-desc-wrap\" data-equalizer-watch=\"cap\">
                <div class=\"cap-desc\">
                    <div class=\"cd-item\">
                    ";
        // line 230
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["capability_data"]) ? $context["capability_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["capability"]) {
            // line 231
            echo "                        <div class=\"cd-content\">
                            <div class=\"cdc-item\">
                                <h3>";
            // line 233
            echo $this->getAttribute($context["capability"], "name", array());
            echo "</h3>
                                <img src=\"";
            // line 234
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "assets/css/images/business-dot.png\"/>
                                <p>
                                    ";
            // line 236
            echo $this->getAttribute($context["capability"], "desc", array());
            echo "
                                </p>
                            </div>       
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['capability'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 241
        echo "                    </div>
                </div>
                <div class=\"sign\">
                    <img src=\"";
        // line 244
        echo Uri::base();
        echo "/assets/css/images/sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"people\" data-equalizer=\"people\">
            <div class=\"small-12 medium-6 columns no-padding people-desc\" data-equalizer-watch=\"people\" >
                <div class=\"pd-desc\">
                    ";
        // line 252
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ourpeople_content"]) ? $context["ourpeople_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ourpeople"]) {
            // line 253
            echo "                        ";
            echo $this->getAttribute($context["ourpeople"], "content1", array());
            echo "
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ourpeople'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 255
        echo "                </div>
                <div class=\"pd-image\">
                    <img src=\"";
        // line 257
        echo Uri::base();
        echo "/assets/css/images/city-bottom.jpg\" style=\"width: 100%;\"/>
                </div>
            </div>
            <div class=\"small-12 medium-6 columns people-title\" data-equalizer-watch=\"people\">
                <div class=\"pd-title\">
                    Our <br/> People
                </div>
                <div class=\"pd-line\"></div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"mai-member\">
            <div class=\"small-12-columns no-padding\">
                <img src=\"";
        // line 270
        echo Uri::base();
        echo "/assets/css/images/mai-member.jpg\" style=\"width: 100%;\"/>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div class=\"people-subhead\" data-equalizer=\"desc\">
            <div class=\"small-12 medium-6 columns no-padding\" data-equalizer-watch=\"desc\">
                <img src=\"";
        // line 276
        echo Uri::base();
        echo "/assets/css/images/after-people.jpg\" style=\"width: 100%;\"/>
            </div>
            <div class=\"small-12 medium-6 columns no-padding\">
                <div class=\"small-10 columns people-detail\" data-equalizer-watch=\"desc\">
                    ";
        // line 280
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ourpeople_content"]) ? $context["ourpeople_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ourpeople"]) {
            // line 281
            echo "                        ";
            echo $this->getAttribute($context["ourpeople"], "content2", array());
            echo "
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ourpeople'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 283
        echo "                </div>
                <div style=\"clear: both;\"></div>
                <div class=\"back-sign\">
                    <img src=\"";
        // line 286
        echo Uri::base();
        echo "/assets/css/images/back-sign.png\"/>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>

        <div class=\"product\">
            <div class=\"product-title\">
                <div class=\"small-12 medium-11 medium-offset-1 columns p-title\">
                    <span>Microad</span><br/>
                    PLATFORM<br/>
                    PRODUCTS
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"p-items\">
                <div class=\"pi-blog\">
                    <div class=\"small-12 medium-1 columns\">
                        <div style=\"opacity: 0;\">They Trust Us</div>
                    </div>
                ";
        // line 306
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["businessunit_data"]) ? $context["businessunit_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["businessunit"]) {
            // line 307
            echo "                    <div class=\"small-12 medium-5 columns\">
                        <div class=\"small-12 medium-2 columns no-padding pib-title\">
                            ";
            // line 309
            echo $this->getAttribute($context["businessunit"], "name", array());
            echo "
                        </div>
                        <div style=\"clear: both;\"></div>
                        <div class=\"small-12 medium-3 columns no-padding\">
                            <img src=\"";
            // line 313
            echo Uri::base();
            echo "assets/css/images/logo-bblog.png\"/>
                        </div>
                        <div class=\"small-12 medium-8 columns pib-content\">
                            ";
            // line 316
            echo $this->getAttribute($context["businessunit"], "desc", array());
            echo "
                        </div>
                    </div>
                    <div class=\"small-12 medium-6 columns\">
                        <img src=\"";
            // line 320
            echo Uri::base();
            echo "media/businessunit/";
            echo $this->getAttribute($context["businessunit"], "image", array());
            echo "\" style=\"width: 100%;\"/>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['businessunit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 323
        echo "                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
        <div class=\"contact-wrap\" id=\"contact\">
            <div class=\"contact-wrapper\">
                <div class=\"cw-excerpt\">
                    <div class=\"small-12 medium-offset-1 medium-3 columns\">We’re very approachable and would love to speak to you. Feel free to call, send us an email, simply complete the enquiry form.</div>
                    <div style=\"clear: both;\"></div>
                </div>
                <div class=\"cw-content\">
                    <div class=\"small-12 medium-offset-1 medium-4 columns\">
                        <div class=\"cwc-step\">
                            <div class=\"small-12 columns no-padding\">
                                <div class=\"cwc-dropcap\">01/</div>
                                <div>Get<br/> in Touch</div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                        <div class=\"cwc-detail\">
                            PT. MicroAd Indonesia<br/>
                            Indosurya Plaza/Thamrin Nine, Floor 3A<br/>
                            Jl. M.H. Thamrin No. 8 - 9<br/>
                            Jakarta Pusat 10230 - Indonesia<br/>
                            <br/>
                            <br/>
                            P : +6221 293 88488<br/>
                            F : +6221 293 88489<br/>
                            <a href=\"mailto:";
        // line 351
        echo Config::get("config_basic.contact_us_email_to");
        echo "\">";
        echo Config::get("config_basic.contact_us_email_to");
        echo "</a><br/>
                        </div>
                    </div>
                    <div class=\"small-12 medium-offset-3 medium-3 end columns\">
                        <div class=\"cwc-step\">
                            <div class=\"small-12 columns no-padding\">
                                <div class=\"cwc-dropcap\">02/</div>
                                <div>Send Us<br/> A Message</div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                        <div class=\"cwc-form\">
                            <form action=\"\" method=\"post\" data-abide=\"ajax\" id=\"contact-form\">
                                <div class=\"small-12 columns no-padding ";
        // line 364
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "name", array())) > 0)) ? ("error") : (""));
        echo "\">
                                    <input type=\"text\" id=\"name\" name=\"name\" placeholder=\"name\" value=\"";
        // line 365
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "name", array());
        echo "\">
                                </div>
                                <div class=\"small-12 columns no-padding ";
        // line 367
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "email", array())) > 0)) ? ("error") : (""));
        echo "\" value=\"";
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "email", array());
        echo "\">
                                    <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"email\">
                                </div>
                                <div class=\"small-12 columns no-padding ";
        // line 370
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "message", array())) > 0)) ? ("error") : (""));
        echo "\">
                                    <textarea id=\"message\" name=\"message\" placeholder=\"message\">";
        // line 371
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "message", array());
        echo "</textarea>
                                </div>
                                <div class=\"small-12 columns no-padding\">
                                    <div class=\"g-recaptcha\" data-sitekey=\"6LdFxB0TAAAAAH9aDoQwpdwUzYEePO0QhJAV-m1G\"></div>
                                </div>
                                <div class=\"small-12 columns no-padding text-right\">
                                    <button type=\"send\">Submit</button>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </form>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 393
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-item nm-normal\">
            <a id=\"aboutButton\" href=\"#about\">About</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"portButton\" href=\"#project\">Portfolio</a>
        </div>
        <div class=\"nm-culture\">
            <a href=\"";
        // line 407
        echo Uri::base();
        echo "culture\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 421
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 432
        echo "
<script>
    
   ";
        // line 447
        echo "        
        
    \$(document).ready(function(){
        \$('.slider1').bxSlider({
            slideWidth: 216,
            minSlides: 1,
            maxSlides: 5,
            moveSlides: 1,
            auto:false,
            pager:true,
            slideMargin: 100
        });
    });
    \$('.contact-wrapper').css('height',\$( window ).height());
    \$( window ).resize(function() {
        \$('.contact-wrapper').css('height',\$( window ).height());
    });

    (function(\$){

        var jump=function(e)
        {
            if (e){
                e.preventDefault();
                var target = \$(this).attr(\"href\");
            }else{
                var target = location.hash;
            }

            \$('html,body').animate(
                    {
                        scrollTop: \$(target).offset().top
                    },1000,function()
                    {
                        location.hash = target;
                    });

        }

        \$('html, body').hide()

        \$(document).ready(function()
        {
            \$('a[href^=\\\\#]').bind(\"click\", jump);

            if (location.hash){
                setTimeout(function(){
                    \$('html, body').scrollTop(0).show()
                    jump()
                }, 0);
            }else{
                \$('html, body').show()
            }
        });

    })(jQuery)



</script>

</body>
</html>

";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 16
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/component.css");
        echo "
        ";
        // line 19
        echo Asset::css("jquery.bxslider.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/slick.css");
        echo "
        ";
        // line 21
        echo Asset::css("custom/slick-theme.css");
        echo "
        ";
        // line 22
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 24
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 25
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 27
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 28
        echo "    ";
    }

    // line 29
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 30
        echo "    ";
    }

    // line 421
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 422
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 423
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 424
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 425
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 426
        echo Asset::js("classie.js");
        echo "
    ";
        // line 427
        echo Asset::js("jquery.easing.1.3.js");
        echo "
    ";
        // line 428
        echo Asset::js("jquery.bxslider.min.js");
        echo "
    ";
        // line 429
        echo Asset::js("slick.min.js");
        echo "
    ";
        // line 430
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "business.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  833 => 430,  829 => 429,  825 => 428,  821 => 427,  817 => 426,  813 => 425,  809 => 424,  805 => 423,  800 => 422,  797 => 421,  793 => 30,  790 => 29,  786 => 28,  783 => 27,  776 => 25,  773 => 24,  767 => 22,  763 => 21,  759 => 20,  755 => 19,  751 => 18,  747 => 17,  743 => 16,  739 => 15,  734 => 14,  731 => 13,  663 => 447,  658 => 432,  656 => 421,  639 => 407,  620 => 393,  595 => 371,  591 => 370,  583 => 367,  578 => 365,  574 => 364,  556 => 351,  526 => 323,  515 => 320,  508 => 316,  502 => 313,  495 => 309,  491 => 307,  487 => 306,  464 => 286,  459 => 283,  450 => 281,  446 => 280,  439 => 276,  430 => 270,  414 => 257,  410 => 255,  401 => 253,  397 => 252,  386 => 244,  381 => 241,  370 => 236,  365 => 234,  361 => 233,  357 => 231,  353 => 230,  343 => 223,  326 => 209,  316 => 202,  306 => 195,  296 => 188,  286 => 181,  276 => 174,  253 => 154,  240 => 144,  231 => 137,  220 => 134,  217 => 133,  213 => 132,  198 => 119,  184 => 113,  180 => 112,  174 => 109,  169 => 106,  165 => 105,  146 => 89,  137 => 83,  134 => 82,  124 => 78,  116 => 75,  104 => 66,  100 => 65,  77 => 45,  66 => 37,  58 => 31,  55 => 29,  52 => 27,  49 => 24,  47 => 13,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/component.css') | raw }}*/
/*         {{ asset_css('jquery.bxslider.css') | raw }}*/
/*         {{ asset_css('custom/slick.css') | raw }}*/
/*         {{ asset_css('custom/slick-theme.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div id="container" class="container intro-effect-sliced">*/
/*     <div class="business-banner header">*/
/*         <div class="bb-image bg-img">*/
/*             <img src="{{ base_url }}assets/css/images/business-banner.jpg"/>*/
/*         </div>*/
/*         <div class="bb-title">*/
/*             <div class="small-12 columns">*/
/*                 <h1>Business</h1>*/
/*             </div>*/
/*             <div class="small-12 columns">*/
/*                 <div class="white-box"></div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns">*/
/*                 <div class="chord">*/
/*                     Challenge, Humble, Open<br/>*/
/*                     Respect, Discipline.*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="bb-intro" id="about">*/
/*             <div class="small-12 medium-7 columns bbi-image no-padding">*/
/*                 <div style="height: 126px;"></div>*/
/*                 <img src="{{ base_url() }}assets/css/images/microad-office.jpg"/>*/
/*                 <img src="{{ base_url() }}assets/css/images/mozaik-top.jpg"/>*/
/*             </div>*/
/*             <div class="small-12 medium-5 columns">*/
/*                 <div style="height: 126px;"></div>*/
/*                 <div class="bbi-title">*/
/*                     <span>MicroAd</span>*/
/*                     <hr/>*/
/*                     Indonesia*/
/*                 </div>*/
/*             {% for preface in preface_content %}    */
/*                 <div class="bbi-content">*/
/*                     <p>*/
/*                         {{ preface.content }}*/
/*                     </p>*/
/*                 </div>*/
/*             {% endfor %}*/
/*                 <div class="sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="bb-image bg-img">*/
/*             <img src="{{ base_url }}assets/css/images/business-banner.jpg"/>*/
/*         </div>*/
/*     </div>*/
/*     <div style="height: 100px;"></div>*/
/*     <div class="content">*/
/*         <div class="project-wrapper" id="project">*/
/*             <div class="small-12 columns no-padding">*/
/*                 <div class="project">*/
/*                     <div class="pro-title-wrapper">*/
/*                         <div class="pro-title">*/
/*                             <div class="pt-item">*/
/*                                 Projects*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="slider1">*/
/*                     {% for portfolio in portfolio_data %}*/
/*                         <div class="slide">*/
/*                             <a href="#">*/
/*                                 <div class="small-12 columns symbol">*/
/*                                     <img src="{{ base_url() }}assets/css/images/symbol.png"/>*/
/*                                 </div>*/
/*                                 <div class="small-12 columns ss-content">*/
/*                                     {{ portfolio.client }}*/
/*                                     <img src="{{ base_url() }}media/portfolio/{{ portfolio.image }}"/>*/
/*                                 </div>*/
/*                                 <div style="clear: both;"></div>*/
/*                             </a>*/
/*                         </div> */
/*                     {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="partner">*/
/*             <div class="small-12 medium-2 columns p-title">*/
/*                 <div class="pt-left">*/
/*                     They Trust Us*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-10 columns">*/
/*                 <div class="small-up-1 medium-up-2 large-up-4">*/
/*                 {% for client in client_logo %}*/
/*                     <div class="column">*/
/*                         <img src="{{ base_url() }}media/client/{{ client.image }}" class="thumbnail" alt="">*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="back-sign">*/
/*             <div class="small-12 medium-offset-2 medium-10 columns">*/
/*                 <div class="small-12 medium-3 columns" style="text-align: center;">*/
/*                     <img src="{{ base_url() }}/assets/css/images/back-sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="capabilities" data-equalizer="cap">*/
/*             <div class="small-12 medium-6 columns no-padding" data-equalizer-watch="cap">*/
/*                 <div class="cap-wrap-left">*/
/*                     <div class="small-12 medium-7 columns no-padding relative">*/
/*                         <div class="cap-image">*/
/*                             <img src="{{ base_url }}assets/css/images/capabilities-image.jpg"/>*/
/*                         </div>*/
/*                         <div class="cap-tag">*/
/*                             #Ad platform<br/>*/
/*                             #website development<br/>*/
/*                             #social media marketing<br/>*/
/*                             #Search engine optimization<br/>*/
/*                             #Search engine marketing<br/>*/
/*                             #Branding and Strategy*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-5 columns no-padding relative">*/
/*                         <div class="cap-title">*/
/*                             <h3>Capabilities</h3>*/
/*                         </div>*/
/*                         <div class="cap-slide">*/
/*                             <div class="cs-item">*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="0" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/network.png" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="1" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/design.png" alt="" /> */
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="2" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/social.png" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="3" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/optimization.png" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="4" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/marketing.png" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div class="small-12 medium-4 columns">*/
/*                                     <a data-slide-index="5" href="">*/
/*                                         <div class="csi-image">*/
/*                                             <img src="{{ base_url() }}media/capability/branding.png" alt="" />*/
/*                                         </div>*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div style="clear: both;"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="cap-arrow">*/
/*                             <span id="slider-prev"></span>*/
/*                             <span id="slider-next"></span>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                     <div class="just-image">*/
/*                         <img src="{{ base_url() }}/assets/css/images/cap-image-example.jpg"/>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns cap-desc-wrap" data-equalizer-watch="cap">*/
/*                 <div class="cap-desc">*/
/*                     <div class="cd-item">*/
/*                     {% for capability in capability_data %}*/
/*                         <div class="cd-content">*/
/*                             <div class="cdc-item">*/
/*                                 <h3>{{ capability.name }}</h3>*/
/*                                 <img src="{{ base_url }}assets/css/images/business-dot.png"/>*/
/*                                 <p>*/
/*                                     {{ capability.desc }}*/
/*                                 </p>*/
/*                             </div>       */
/*                         </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="people" data-equalizer="people">*/
/*             <div class="small-12 medium-6 columns no-padding people-desc" data-equalizer-watch="people" >*/
/*                 <div class="pd-desc">*/
/*                     {% for ourpeople in ourpeople_content %}*/
/*                         {{ ourpeople.content1 }}*/
/*                     {% endfor %}*/
/*                 </div>*/
/*                 <div class="pd-image">*/
/*                     <img src="{{ base_url() }}/assets/css/images/city-bottom.jpg" style="width: 100%;"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns people-title" data-equalizer-watch="people">*/
/*                 <div class="pd-title">*/
/*                     Our <br/> People*/
/*                 </div>*/
/*                 <div class="pd-line"></div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="mai-member">*/
/*             <div class="small-12-columns no-padding">*/
/*                 <img src="{{ base_url() }}/assets/css/images/mai-member.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*         <div class="people-subhead" data-equalizer="desc">*/
/*             <div class="small-12 medium-6 columns no-padding" data-equalizer-watch="desc">*/
/*                 <img src="{{ base_url() }}/assets/css/images/after-people.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns no-padding">*/
/*                 <div class="small-10 columns people-detail" data-equalizer-watch="desc">*/
/*                     {% for ourpeople in ourpeople_content %}*/
/*                         {{ ourpeople.content2 }}*/
/*                     {% endfor %}*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*                 <div class="back-sign">*/
/*                     <img src="{{ base_url() }}/assets/css/images/back-sign.png"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/* */
/*         <div class="product">*/
/*             <div class="product-title">*/
/*                 <div class="small-12 medium-11 medium-offset-1 columns p-title">*/
/*                     <span>Microad</span><br/>*/
/*                     PLATFORM<br/>*/
/*                     PRODUCTS*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <div class="p-items">*/
/*                 <div class="pi-blog">*/
/*                     <div class="small-12 medium-1 columns">*/
/*                         <div style="opacity: 0;">They Trust Us</div>*/
/*                     </div>*/
/*                 {% for businessunit in businessunit_data %}*/
/*                     <div class="small-12 medium-5 columns">*/
/*                         <div class="small-12 medium-2 columns no-padding pib-title">*/
/*                             {{ businessunit.name }}*/
/*                         </div>*/
/*                         <div style="clear: both;"></div>*/
/*                         <div class="small-12 medium-3 columns no-padding">*/
/*                             <img src="{{ base_url() }}assets/css/images/logo-bblog.png"/>*/
/*                         </div>*/
/*                         <div class="small-12 medium-8 columns pib-content">*/
/*                             {{ businessunit.desc }}*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-6 columns">*/
/*                         <img src="{{ base_url() }}media/businessunit/{{ businessunit.image }}" style="width: 100%;"/>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="contact-wrap" id="contact">*/
/*             <div class="contact-wrapper">*/
/*                 <div class="cw-excerpt">*/
/*                     <div class="small-12 medium-offset-1 medium-3 columns">We’re very approachable and would love to speak to you. Feel free to call, send us an email, simply complete the enquiry form.</div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*                 <div class="cw-content">*/
/*                     <div class="small-12 medium-offset-1 medium-4 columns">*/
/*                         <div class="cwc-step">*/
/*                             <div class="small-12 columns no-padding">*/
/*                                 <div class="cwc-dropcap">01/</div>*/
/*                                 <div>Get<br/> in Touch</div>*/
/*                             </div>*/
/*                             <div style="clear: both;"></div>*/
/*                         </div>*/
/*                         <div class="cwc-detail">*/
/*                             PT. MicroAd Indonesia<br/>*/
/*                             Indosurya Plaza/Thamrin Nine, Floor 3A<br/>*/
/*                             Jl. M.H. Thamrin No. 8 - 9<br/>*/
/*                             Jakarta Pusat 10230 - Indonesia<br/>*/
/*                             <br/>*/
/*                             <br/>*/
/*                             P : +6221 293 88488<br/>*/
/*                             F : +6221 293 88489<br/>*/
/*                             <a href="mailto:{{ config('config_basic.contact_us_email_to') }}">{{ config('config_basic.contact_us_email_to') }}</a><br/>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-offset-3 medium-3 end columns">*/
/*                         <div class="cwc-step">*/
/*                             <div class="small-12 columns no-padding">*/
/*                                 <div class="cwc-dropcap">02/</div>*/
/*                                 <div>Send Us<br/> A Message</div>*/
/*                             </div>*/
/*                             <div style="clear: both;"></div>*/
/*                         </div>*/
/*                         <div class="cwc-form">*/
/*                             <form action="" method="post" data-abide="ajax" id="contact-form">*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.name | length > 0 ? 'error' : '' }}">*/
/*                                     <input type="text" id="name" name="name" placeholder="name" value="{{ post_data.name }}">*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.email | length > 0 ? 'error' : '' }}" value="{{ post_data.email }}">*/
/*                                     <input type="email" id="email" name="email" placeholder="email">*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding {{ err_msg.message | length > 0 ? 'error' : '' }}">*/
/*                                     <textarea id="message" name="message" placeholder="message">{{ post_data.message }}</textarea>*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding">*/
/*                                     <div class="g-recaptcha" data-sitekey="6LdFxB0TAAAAAH9aDoQwpdwUzYEePO0QhJAV-m1G"></div>*/
/*                                 </div>*/
/*                                 <div class="small-12 columns no-padding text-right">*/
/*                                     <button type="send">Submit</button>*/
/*                                 </div>*/
/*                                 <div style="clear: both;"></div>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="aboutButton" href="#about">About</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="portButton" href="#project">Portfolio</a>*/
/*         </div>*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}culture">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('classie.js') | raw }}*/
/*     {{ asset_js('jquery.easing.1.3.js') | raw }}*/
/*     {{ asset_js('jquery.bxslider.min.js') | raw }}*/
/*     {{ asset_js('slick.min.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/*     */
/*    {# $('.form-group .alert-danger').css("display","none");*/
/*         var flagCaptcha = false;*/
/* */
/*         var recaptchaCallback = function() {*/
/*             flagCaptcha = true;*/
/*     };*/
/*         */
/*     $('#contact-form').on('valid.fndtn.abide', function() {*/
/*         if (flagCaptcha) {*/
/*             $(this).submit();*/
/*         }*/
/*     });#}*/
/*         */
/*         */
/*     $(document).ready(function(){*/
/*         $('.slider1').bxSlider({*/
/*             slideWidth: 216,*/
/*             minSlides: 1,*/
/*             maxSlides: 5,*/
/*             moveSlides: 1,*/
/*             auto:false,*/
/*             pager:true,*/
/*             slideMargin: 100*/
/*         });*/
/*     });*/
/*     $('.contact-wrapper').css('height',$( window ).height());*/
/*     $( window ).resize(function() {*/
/*         $('.contact-wrapper').css('height',$( window ).height());*/
/*     });*/
/* */
/*     (function($){*/
/* */
/*         var jump=function(e)*/
/*         {*/
/*             if (e){*/
/*                 e.preventDefault();*/
/*                 var target = $(this).attr("href");*/
/*             }else{*/
/*                 var target = location.hash;*/
/*             }*/
/* */
/*             $('html,body').animate(*/
/*                     {*/
/*                         scrollTop: $(target).offset().top*/
/*                     },1000,function()*/
/*                     {*/
/*                         location.hash = target;*/
/*                     });*/
/* */
/*         }*/
/* */
/*         $('html, body').hide()*/
/* */
/*         $(document).ready(function()*/
/*         {*/
/*             $('a[href^=\\#]').bind("click", jump);*/
/* */
/*             if (location.hash){*/
/*                 setTimeout(function(){*/
/*                     $('html, body').scrollTop(0).show()*/
/*                     jump()*/
/*                 }, 0);*/
/*             }else{*/
/*                 $('html, body').show()*/
/*             }*/
/*         });*/
/* */
/*     })(jQuery)*/
/* */
/* */
/* */
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
