<?php

/* email_responder.twig */
class __TwigTemplate_be1dd7acf30ab2aa84bafa6d1e1215cdc6d994cf8922a8cd1951635adf04d5e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    Dear ";
        echo (isset($context["name"]) ? $context["name"] : null);
        echo ", <br/><br/>
    Thank you for contacting us,<br/>
    our sales team would be contacting you as soon as possible<br/><br/>
    Best Regards,<br/><br/><br/>
    PT. MicroAd Indonesia
    <div><img src=\"https://postimg.org/image/ynhb45hrx/\" alt=\"city\"/></div>
";
    }

    public function getTemplateName()
    {
        return "email_responder.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  20 => 1,);
    }
}
/* {% block frontend_content %}*/
/*     Dear {{ name }}, <br/><br/>*/
/*     Thank you for contacting us,<br/>*/
/*     our sales team would be contacting you as soon as possible<br/><br/>*/
/*     Best Regards,<br/><br/><br/>*/
/*     PT. MicroAd Indonesia*/
/*     <div><img src="https://postimg.org/image/ynhb45hrx/" alt="city"/></div>*/
/* {% endblock %}*/
