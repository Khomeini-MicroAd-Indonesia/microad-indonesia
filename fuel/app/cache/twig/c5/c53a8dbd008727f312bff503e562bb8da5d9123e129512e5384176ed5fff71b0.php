<?php

/* home.twig */
class __TwigTemplate_260ed41e3b21a596a875d2dc289e44121baea66946f112945471aefc47b007a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!DOCTYPE html>
    <html class=\"ie8\">
    <head>
        <meta charset=\"UTF-8\">
        ";
        // line 6
        echo "        <meta property=\"og:url\"                content=\"http://microad.co.id\" />
        <meta property=\"og:type\"               content=\"website\" />
        <meta property=\"og:title\"              content=\"Welcome to MicroAd Indonesia\" />
        <meta property=\"og:description\"        content=\"PT. MicroAd Indonesia is a subsidiary of MicroAd, Inc. from Japand.

We are a full service digital agency, uniquely known for our advanced online advertising technologies from Japan. Here in Indonesia, we offer complete digital services,from integrated digital campaign to all categories of digital marketing such as creative web/mobile development, performing ads, social media, and viral video marketing.

Smart. Creative. Dynamic. The one-stop solution for your digital marketing needs.

Overcome obstacles. Consult us today.\" />
        <meta property=\"og:image\"              content=\"http://microad.co.id/assets/css/images/mai-web-bg.jpg\" />
        ";
        // line 18
        echo "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>
        <link rel=\"shortcut icon\" href=\"";
        // line 21
        echo Uri::base();
        echo "/assets/css/images/favicon.ico\" type=\"image/vnd.microsoft.icon\">
        <link rel=\"icon\" href=\"";
        // line 22
        echo Uri::base();
        echo "/assets/css/images/favicon.ico\" type=\"image/vnd.microsoft.icon\">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        ";
        // line 24
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 29
        echo "        <style>
            .edgeLoad-EDGE-7814028 { visibility:hidden; }
            .edgeLoad-EDGE-14088978 { visibility:hidden; }
            .edgeLoad-EDGE-15798491 { visibility:hidden; }
        </style>
        ";
        // line 34
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 37
        echo "        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '";
        // line 43
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>

    <div id=\"base\"></div>
    <div id=\"bg-city\"></div>
    <div id=\"tokyo-eye\">
        <div id=\"Stage-eye\" class=\"EDGE-7814028\"></div>
    </div>
    <div id=\"tokyo-tower\"></div>
    <div id=\"tokyo-crane\">
        <div id=\"Stage-crane\" class=\"EDGE-14088978\"></div>
    </div>
    <div id=\"tokyo\"></div>
    <div id=\"tokyo-street\"></div>
    <div id=\"tokyo-shinkansen\">
        <div id=\"Stage-train\" class=\"EDGE-15798491\"></div>
    </div>
    <div id=\"gen-city\"></div>
    <div id=\"monas\"></div>
    <div id=\"jakarta\"></div>
    <div id=\"jakarta-street\"></div>
    <a href=\"";
        // line 67
        echo Uri::base();
        echo "business\">
        <div id=\"logo-mai2\"></div>
    </a>
    <div id=\"cyber-agent\">
        <img src=\"";
        // line 71
        echo Uri::base();
        echo "/assets/css/images/cyber-agent.png\"/>
    </div>


    <div id=\"middleNav\">
        <div id=\"pull-left\" class=\"business-container\">
            <a href=\"";
        // line 77
        echo Uri::base();
        echo "business\">
                <div class=\"business\">
                    <div class=\"side\">
                        <div class=\"side-content\">
                            Our Services
                        </div>
                    </div>
                    <div class=\"side back\"><img src=\"";
        // line 84
        echo Uri::base();
        echo "/assets/css/images/business-img.jpg\" alt=\"business-nav\"></div>
                </div>
            </a>
        </div>
        <div id=\"pull-right\" class=\"culture-container\">
            <a href=\"";
        // line 89
        echo Uri::base();
        echo "culture\">
                <div class=\"business\">
                    <div class=\"side\">
                        <div class=\"side-content\" style=\"margin-left: 25px; text-align: right\">
                            Our Culture
                        </div>
                    </div>
                    ";
        // line 97
        echo "                </div>
            </a>
        </div>
    </div>

    <nav id=\"main-nav\">
        <div id=\"logo-mai\">
            <div class=\"lm-image\">
                <a href=\"";
        // line 105
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo-mai.png\"/></a>
            </div>
        </div>
        <div class=\"nav-menu\">
            <div class=\"nm-item\">
                <a href=\"";
        // line 110
        echo Uri::base();
        echo "business#contact\">Contact</a>
            </div>
            ";
        // line 115
        echo "            <div class=\"nm-item\">
                <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
            </div>
        </div>
    </nav>

    ";
        // line 121
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 156
        echo "    </body>
    </html>";
    }

    // line 24
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 25
        echo "            ";
        echo Asset::css("custom/normalize.css");
        echo "
            ";
        // line 26
        echo Asset::css("custom/magic.min.css");
        echo "
            ";
        // line 27
        echo Asset::css("custom/main.css");
        echo "
        ";
    }

    // line 34
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 35
        echo "            ";
        echo Asset::js("modernizr.js");
        echo "
        ";
    }

    // line 121
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 122
        echo "        ";
        echo Asset::js("vendor/jquery.min.js");
        echo "
        ";
        // line 123
        echo Asset::js("parallax.js");
        echo "
        ";
        // line 124
        echo Asset::js("publish/edge_includes/edge.6.0.0.min.js");
        echo "
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-eye', 'EDGE-7814028', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"175px\",
                height: \"175px\"
            }, {\"dom\":{}}, {\"style\":{\"\${symbolSelector}\":{\"isStage\":\"true\",\"rect\":[\"undefined\",\"undefined\",\"175px\",\"175px\"],\"fill\":[\"rgba(255,255,255,1)\"]}},\"dom\":{}});
        </script>
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-crane', 'EDGE-14088978', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"291px\",
                height: \"148px\"
            }, {\"dom\":{}}, {\"dom\":{}});
        </script>
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-shinkansen', 'EDGE-15798491', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"4000px\",
                height: \"56px\"
            }, {\"dom\":{}}, {\"dom\":{}});
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 124,  221 => 123,  216 => 122,  213 => 121,  206 => 35,  203 => 34,  197 => 27,  193 => 26,  188 => 25,  185 => 24,  180 => 156,  178 => 121,  170 => 115,  165 => 110,  155 => 105,  145 => 97,  135 => 89,  127 => 84,  117 => 77,  108 => 71,  101 => 67,  74 => 43,  66 => 37,  64 => 34,  57 => 29,  55 => 24,  50 => 22,  46 => 21,  41 => 18,  28 => 6,  22 => 1,);
    }
}
/*     <!DOCTYPE html>*/
/*     <html class="ie8">*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         {#<title>{{ meta_title }}</title>#}*/
/*         <meta property="og:url"                content="http://microad.co.id" />*/
/*         <meta property="og:type"               content="website" />*/
/*         <meta property="og:title"              content="Welcome to MicroAd Indonesia" />*/
/*         <meta property="og:description"        content="PT. MicroAd Indonesia is a subsidiary of MicroAd, Inc. from Japand.*/
/* */
/* We are a full service digital agency, uniquely known for our advanced online advertising technologies from Japan. Here in Indonesia, we offer complete digital services,from integrated digital campaign to all categories of digital marketing such as creative web/mobile development, performing ads, social media, and viral video marketing.*/
/* */
/* Smart. Creative. Dynamic. The one-stop solution for your digital marketing needs.*/
/* */
/* Overcome obstacles. Consult us today." />*/
/*         <meta property="og:image"              content="http://microad.co.id/assets/css/images/mai-web-bg.jpg" />*/
/*         {#<meta property="fb:app_id"              content="222689941109369" />#}*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>*/
/*         <link rel="shortcut icon" href="{{ base_url() }}/assets/css/images/favicon.ico" type="image/vnd.microsoft.icon">*/
/*         <link rel="icon" href="{{ base_url() }}/assets/css/images/favicon.ico" type="image/vnd.microsoft.icon">*/
/*         <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*         {% block frontend_css %}*/
/*             {{ asset_css('custom/normalize.css') | raw }}*/
/*             {{ asset_css('custom/magic.min.css') | raw }}*/
/*             {{ asset_css('custom/main.css') | raw }}*/
/*         {% endblock %}*/
/*         <style>*/
/*             .edgeLoad-EDGE-7814028 { visibility:hidden; }*/
/*             .edgeLoad-EDGE-14088978 { visibility:hidden; }*/
/*             .edgeLoad-EDGE-15798491 { visibility:hidden; }*/
/*         </style>*/
/*         {% block frontend_head_js %}*/
/*             {{ asset_js('modernizr.js') }}*/
/*         {% endblock %}*/
/*         <script>*/
/*             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*             })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*             ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*             ga('send', 'pageview');*/
/*         </script>*/
/*     </head>*/
/*     <body>*/
/* */
/*     <div id="base"></div>*/
/*     <div id="bg-city"></div>*/
/*     <div id="tokyo-eye">*/
/*         <div id="Stage-eye" class="EDGE-7814028"></div>*/
/*     </div>*/
/*     <div id="tokyo-tower"></div>*/
/*     <div id="tokyo-crane">*/
/*         <div id="Stage-crane" class="EDGE-14088978"></div>*/
/*     </div>*/
/*     <div id="tokyo"></div>*/
/*     <div id="tokyo-street"></div>*/
/*     <div id="tokyo-shinkansen">*/
/*         <div id="Stage-train" class="EDGE-15798491"></div>*/
/*     </div>*/
/*     <div id="gen-city"></div>*/
/*     <div id="monas"></div>*/
/*     <div id="jakarta"></div>*/
/*     <div id="jakarta-street"></div>*/
/*     <a href="{{ base_url() }}business">*/
/*         <div id="logo-mai2"></div>*/
/*     </a>*/
/*     <div id="cyber-agent">*/
/*         <img src="{{ base_url() }}/assets/css/images/cyber-agent.png"/>*/
/*     </div>*/
/* */
/* */
/*     <div id="middleNav">*/
/*         <div id="pull-left" class="business-container">*/
/*             <a href="{{ base_url() }}business">*/
/*                 <div class="business">*/
/*                     <div class="side">*/
/*                         <div class="side-content">*/
/*                             Our Services*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="side back"><img src="{{ base_url() }}/assets/css/images/business-img.jpg" alt="business-nav"></div>*/
/*                 </div>*/
/*             </a>*/
/*         </div>*/
/*         <div id="pull-right" class="culture-container">*/
/*             <a href="{{ base_url() }}culture">*/
/*                 <div class="business">*/
/*                     <div class="side">*/
/*                         <div class="side-content" style="margin-left: 25px; text-align: right">*/
/*                             Our Culture*/
/*                         </div>*/
/*                     </div>*/
/*                     {#<div class="side back"><a href="{{ base_url() }}culture"><img src="{{ base_url() }}/assets/css/images/culture-img.jpg" alt="culture-nav"></a></div>#}*/
/*                 </div>*/
/*             </a>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <nav id="main-nav">*/
/*         <div id="logo-mai">*/
/*             <div class="lm-image">*/
/*                 <a href="{{ base_url() }}"><img src="{{ base_url() }}/assets/css/images/logo-mai.png"/></a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="nav-menu">*/
/*             <div class="nm-item">*/
/*                 <a href="{{ base_url() }}business#contact">Contact</a>*/
/*             </div>*/
/*             {#<div class="nm-item separator">*/
/*                 <a href="#">Career</a>*/
/*             </div>#}*/
/*             <div class="nm-item">*/
/*                 <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*             </div>*/
/*         </div>*/
/*     </nav>*/
/* */
/*     {% block frontend_js %}*/
/*         {{ asset_js('vendor/jquery.min.js') | raw }}*/
/*         {{ asset_js('parallax.js') | raw }}*/
/*         {{ asset_js('publish/edge_includes/edge.6.0.0.min.js') | raw }}*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-eye', 'EDGE-7814028', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "175px",*/
/*                 height: "175px"*/
/*             }, {"dom":{}}, {"style":{"${symbolSelector}":{"isStage":"true","rect":["undefined","undefined","175px","175px"],"fill":["rgba(255,255,255,1)"]}},"dom":{}});*/
/*         </script>*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-crane', 'EDGE-14088978', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "291px",*/
/*                 height: "148px"*/
/*             }, {"dom":{}}, {"dom":{}});*/
/*         </script>*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-shinkansen', 'EDGE-15798491', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "4000px",*/
/*                 height: "56px"*/
/*             }, {"dom":{}}, {"dom":{}});*/
/*         </script>*/
/*     {% endblock %}*/
/*     </body>*/
/*     </html>*/
