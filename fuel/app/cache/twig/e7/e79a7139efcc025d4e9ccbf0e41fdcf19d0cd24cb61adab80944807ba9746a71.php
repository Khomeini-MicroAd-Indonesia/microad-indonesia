<?php

/* list_image.twig */
class __TwigTemplate_933aa88d14e49113ab0dc6af75840ccacac17920382f46e9052550f55ff2edf4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("backend/template.twig", "list_image.twig", 1);
        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t<!-- dataTables css -->
\t";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\tPhoto
\t\t<small>Image List</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li><a href=\"";
        // line 19
        echo Uri::base();
        echo "backend/photo\">Photo</a></li>
\t\t<li class=\"active\">Image List</li>
\t</ol>
</section>
";
    }

    // line 25
    public function block_backend_content($context, array $blocks = array())
    {
        // line 26
        echo "
";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 28
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 30
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 33
        echo "
";
        // line 34
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 35
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 37
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 40
        echo "
\t<div class=\"box box-solid\">
\t\t<div class=\"box-body text-right\">
\t\t\t<a href=\"";
        // line 43
        echo Uri::base();
        echo "backend/photo/image/add\">
\t\t\t\t<button class=\"btn btn-default\">Create</button>
\t\t\t</a>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"box\">
\t\t\t\t<div class=\"box-body table-responsive\">
\t\t\t\t\t<table id=\"table-photo-images\" class=\"table table-bordered table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>ID</th>
\t\t\t\t\t\t\t<th>Name</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t<th>Seq</th>
\t\t\t\t\t\t\t<th>Image</th>
\t\t\t\t\t\t\t<th>&nbsp;</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["image_list"]) ? $context["image_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 65
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 66
            echo $this->getAttribute($context["image"], "id", array());
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 67
            echo $this->getAttribute($context["image"], "name", array());
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 68
            echo $this->getAttribute($context["image"], "get_status_name", array(), "method");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 69
            echo $this->getAttribute($context["image"], "seq", array());
            echo "</td>
\t\t\t\t\t\t\t<td><img src=\"";
            // line 70
            echo Uri::base();
            echo "media/photo/thumbnail/";
            echo $this->getAttribute($context["image"], "filename", array());
            echo "\"</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t<div class=\"btn-group\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t\t\t\tAction <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 77
            echo Uri::base();
            echo "backend/photo/image/edit/";
            echo $this->getAttribute($context["image"], "id", array());
            echo "\">Edit</a></li>
\t\t\t\t\t\t\t\t\t\t
                                        <li><a class=\"btn-delete\" data-url-delete=\"";
            // line 79
            echo Uri::base();
            echo "backend/photo/image/delete/";
            echo $this->getAttribute($context["image"], "id", array());
            echo "\" href=\"#\">Delete</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div><!-- /.box-body -->
\t\t\t</div><!-- /.box -->
\t\t</div>
\t</div>
";
    }

    // line 93
    public function block_backend_js($context, array $blocks = array())
    {
        // line 94
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 96
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 97
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t<!-- custom script -->
\t<script type=\"text/javascript\">
\t\t\$(function() {
\t\t\t\$(\"#table-photo-images\").dataTable( {
\t\t\t\t\"aoColumns\": [ 
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false },
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false }
\t\t\t\t]
\t\t\t} );
\t\t} );
\t</script>
\t<!-- Dialog Confirmation Script -->
\t";
        // line 114
        echo Asset::js("backend-dialog.js");
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery('#table-photo-images').on('click', '.btn-delete', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-delete');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);
\t\t});
        jQuery('#table-photo-images').on('click', '.btn-rearrange', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-rearrange');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to rearrange from this?', yes_callback);
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "list_image.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 114,  215 => 97,  211 => 96,  205 => 94,  202 => 93,  192 => 85,  178 => 79,  171 => 77,  159 => 70,  155 => 69,  151 => 68,  147 => 67,  143 => 66,  140 => 65,  136 => 64,  112 => 43,  107 => 40,  101 => 37,  97 => 35,  95 => 34,  92 => 33,  86 => 30,  82 => 28,  80 => 27,  77 => 26,  74 => 25,  65 => 19,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,  11 => 1,);
    }
}
/* {% extends "backend/template.twig" %}*/
/* */
/* {% block backend_css %}*/
/* 	{{ parent() }}*/
/* 	<!-- dataTables css -->*/
/* 	{{ asset_css('datatables/dataTables.bootstrap.css') | raw }}*/
/* {% endblock %}*/
/* */
/* {% block backend_content_header %}*/
/* <!-- Content Header (Page header) -->*/
/* <section class="content-header">*/
/* 	<h1>{# Set page title and subtitle manually #}*/
/* 		Photo*/
/* 		<small>Image List</small>*/
/* 	</h1>*/
/* 	{# Also set breadcrumb manually #}*/
/* 	<ol class="breadcrumb">*/
/* 		<li><a href="{{ base_url() }}backend">Home</a></li>*/
/* 		<li><a href="{{ base_url() }}backend/photo">Photo</a></li>*/
/* 		<li class="active">Image List</li>*/
/* 	</ol>*/
/* </section>*/
/* {% endblock %}*/
/* */
/* {% block backend_content %}*/
/* */
/* {% if success_message | length > 0 %}*/
/* <div class="alert alert-success alert-dismissable">*/
/* 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 	{{ success_message | raw }}*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if error_message | length > 0 %}*/
/* <div class="alert alert-danger alert-dismissable">*/
/* 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 	{{ error_message | raw }}*/
/* </div>*/
/* {% endif %}*/
/* */
/* 	<div class="box box-solid">*/
/* 		<div class="box-body text-right">*/
/* 			<a href="{{ base_url() }}backend/photo/image/add">*/
/* 				<button class="btn btn-default">Create</button>*/
/* 			</a>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="row">*/
/* 		<div class="col-xs-12">*/
/* 			<div class="box">*/
/* 				<div class="box-body table-responsive">*/
/* 					<table id="table-photo-images" class="table table-bordered table-striped">*/
/* 					<thead>*/
/* 						<tr>*/
/* 							<th>ID</th>*/
/* 							<th>Name</th>*/
/* 							<th>Status</th>*/
/* 							<th>Seq</th>*/
/* 							<th>Image</th>*/
/* 							<th>&nbsp;</th>*/
/* 						</tr>*/
/* 					</thead>*/
/* 					<tbody>*/
/* 						{% for image in image_list %}*/
/* 						<tr>*/
/* 							<td>{{ image.id }}</td>*/
/* 							<td>{{ image.name }}</td>*/
/* 							<td>{{ image.get_status_name() }}</td>*/
/* 							<td>{{ image.seq }}</td>*/
/* 							<td><img src="{{ base_url() }}media/photo/thumbnail/{{ image.filename }}"</td>*/
/* 							<td>*/
/* 								<div class="btn-group">*/
/* 									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">*/
/* 										Action <span class="caret"></span>*/
/* 									</button>*/
/* 									<ul class="dropdown-menu" role="menu">*/
/* 										<li><a href="{{ base_url() }}backend/photo/image/edit/{{ image.id }}">Edit</a></li>*/
/* 										*/
/*                                         <li><a class="btn-delete" data-url-delete="{{ base_url() }}backend/photo/image/delete/{{ image.id }}" href="#">Delete</a></li>*/
/* 									</ul>*/
/* 								</div>*/
/* 							</td>*/
/* 						</tr>*/
/* 						{% endfor %}*/
/* 					</tbody>*/
/* 					</table>*/
/* 				</div><!-- /.box-body -->*/
/* 			</div><!-- /.box -->*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* 		*/
/* {% block backend_js %}*/
/* 	{{ parent() }}*/
/* 	<!-- DATA TABES SCRIPT -->*/
/* 	{{ asset_js('plugins/datatables/jquery.dataTables.js') | raw }}*/
/* 	{{ asset_js('plugins/datatables/dataTables.bootstrap.js') | raw }}*/
/* 	<!-- custom script -->*/
/* 	<script type="text/javascript">*/
/* 		$(function() {*/
/* 			$("#table-photo-images").dataTable( {*/
/* 				"aoColumns": [ */
/* 					null,*/
/* 					null,*/
/* 					null,*/
/* 					null,*/
/* 					{ "bSearchable": false, "bSortable": false },*/
/* 					{ "bSearchable": false, "bSortable": false }*/
/* 				]*/
/* 			} );*/
/* 		} );*/
/* 	</script>*/
/* 	<!-- Dialog Confirmation Script -->*/
/* 	{{ asset_js('backend-dialog.js') | raw }}*/
/* 	<script type="text/javascript">*/
/* 		jQuery('#table-photo-images').on('click', '.btn-delete', function(e){*/
/* 			e.preventDefault();*/
/* 			var my = jQuery(this),*/
/* 				url_del = my.data('url-delete');*/
/* 			var yes_callback = function(e){*/
/* 				e.preventDefault();*/
/* 				jQuery(location).attr('href', url_del);*/
/* 			}*/
/* 			backend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);*/
/* 		});*/
/*         jQuery('#table-photo-images').on('click', '.btn-rearrange', function(e){*/
/* 			e.preventDefault();*/
/* 			var my = jQuery(this),*/
/* 				url_del = my.data('url-rearrange');*/
/* 			var yes_callback = function(e){*/
/* 				e.preventDefault();*/
/* 				jQuery(location).attr('href', url_del);*/
/* 			}*/
/* 			backend_dialog.show_dialog_confirm('Are you sure want to rearrange from this?', yes_callback);*/
/* 		});*/
/* 	</script>*/
/* {% endblock %}*/
