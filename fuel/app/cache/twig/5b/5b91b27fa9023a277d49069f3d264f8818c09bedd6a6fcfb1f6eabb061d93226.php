<?php

/* culture.twig */
class __TwigTemplate_7293e80c5d28e399c5f7f6ddf63d495d31d5ead313fde8944bcd2a03915eeab0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js\"></script>
    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 30
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 32
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 34
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 40
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body id=\"culture\">
<div id=\"weather\" class=\"small-12 columns\"></div>
<div class=\"culture\">
    <div class=\"culture-1\">
        <div class=\"c1-wrap\">
            <div class=\"culture-title\">
                <div class=\"small-12 columns\">Culture</div>
                <div style=\"clear: both;\"></div>
            </div>
            <img src=\"";
        // line 53
        echo Uri::base();
        echo "assets/css/images/culturefirstview1.png\" style=\"width: 100%;\"/>
            <div class=\"c1-left\">
                <div class=\"small-12 columns\">
                    ";
        // line 56
        echo $this->getAttribute((isset($context["indonesia"]) ? $context["indonesia"] : null), 0, array(), "array");
        echo "
                </div>
                <div class=\"small-12 columns\">
                    <div id=\"jkt-time\" class=\"c1l-time\">";
        // line 59
        echo $this->getAttribute((isset($context["indonesia"]) ? $context["indonesia"] : null), 1, array(), "array");
        echo "</div>
                    <div class=\"c1l-region\">Jakarta</div>
                </div>
                ";
        // line 74
        echo "                ";
        // line 78
        echo "                <div style=\"clear: both;\"></div>
            </div>
                    
            <div class=\"c1-right\">
                <div class=\"small-12 columns\">
                    <div class=\"c1r-icon\">
                        <div class=\"icon\">
                        ";
        // line 86
        echo "                            <div class=\"cloud\"></div>
                            <div class=\"rain\"></div>
                        ";
        // line 89
        echo "                        ";
        // line 90
        echo "                            <div class=\"rain\"></div>
                            <div class=\"cloud\"></div>
                        ";
        // line 93
        echo "                        ";
        // line 94
        echo "                            <div class=\"sun\">
                                <div class=\"rays\"></div>
                            </div>
                        ";
        // line 98
        echo "                        </div>
                        30 °C
                    </div>   
                    <div class=\"c1r-region\">Jakarta</div>
                </div>
                ";
        // line 190
        echo "            </div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"about\">
            ";
        // line 195
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["about_maizen"]) ? $context["about_maizen"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["about"]) {
            // line 196
            echo "            <div class=\"abl-image\">
                <img src=\"";
            // line 197
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "assets/css/images/about-image.jpg\" style=\"width: 100%;\"/>
            </div>
            <div class=\"small-12 medium-6 columns ab-left\" style=\"padding-left: 0;\">
                <div class=\"ab-title\">
                    ";
            // line 201
            echo $this->getAttribute($context["about"], "header", array());
            echo "
                </div>
                <div class=\"abl-image\" style=\"bottom: 50px; top: auto; z-index: -2;\">
                    <img src=\"";
            // line 204
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "assets/css/images/ab-image.jpg\" style=\"width: 100%;\"/>
                </div>
            </div>
            <div class=\"small-12 medium-6 columns ab-right\">
                <div class=\"abr-quote\">
                    We believe in C.H.O.R.D
                </div>
                <div class=\"abr-detail\">
                    ";
            // line 212
            echo $this->getAttribute($context["about"], "content", array());
            echo "
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['about'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 217
        echo "        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"news\" id=\"news-content\">
            <div class=\"ne-back\">
            </div>
            <div class=\"ne-content\">
                <div class=\"ne-title\">
                    <h1>News</h1>
                </div>
                <div class=\"ne-detail\">
                    <div class=\"ned-image\">
                        <img src=\"";
        // line 229
        echo Uri::base();
        echo "assets/css/images/news-image.jpg\" style=\"width: 100%;\"/>
                    </div>
                    <div class=\"ned-content\">
                        <div class=\"small-12 medium-2 columns no-padding\">
                            <div class=\"nedc-title\">
                            </div>
                            <div class=\"nedc-detail\">
                            </div>
                        </div>
                    ";
        // line 238
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 239
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 1)) {
                // line 240
                echo "                        <div class=\"small-12 medium-2 columns no-padding last-history\">
                            <div class=\"nedc-title\">
                                ";
                // line 242
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 243
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 246
                echo $this->getAttribute($context["news"], "highlight", array());
                echo " <a href=\"";
                echo Uri::base();
                echo "culture/news/";
                echo $this->getAttribute($context["news"], "slug", array());
                echo "\">...read more</a>
                            </div>
                        </div>
                        ";
            }
            // line 250
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 2)) {
                // line 251
                echo "                        <div class=\"small-12 medium-2 columns no-padding third\">
                            <div class=\"nedc-title\">
                                ";
                // line 253
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 254
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo " 
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 257
                echo $this->getAttribute($context["news"], "highlight", array());
                echo " <a href=\"";
                echo Uri::base();
                echo "culture/news/";
                echo $this->getAttribute($context["news"], "slug", array());
                echo "\">...read more</a>
                            </div>
                        </div>
                        ";
            }
            // line 261
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 3)) {
                // line 262
                echo "                        <div class=\"small-12 medium-2 columns no-padding second\">
                            <div class=\"nedc-title\">
                                ";
                // line 264
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 265
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 268
                echo $this->getAttribute($context["news"], "highlight", array());
                echo " <a href=\"";
                echo Uri::base();
                echo "culture/news/";
                echo $this->getAttribute($context["news"], "slug", array());
                echo "\">...read more</a>
                            </div>
                        </div>
                        ";
            }
            // line 272
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 4)) {
                // line 273
                echo "                        <div class=\"small-12 medium-2 columns no-padding\">
                            <div class=\"nedc-title\">
                                ";
                // line 275
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 276
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 279
                echo $this->getAttribute($context["news"], "highlight", array());
                echo " <a href=\"";
                echo Uri::base();
                echo "culture/news/";
                echo $this->getAttribute($context["news"], "slug", array());
                echo "\">...read more</a>
                            </div>
                        </div>
                        ";
            }
            // line 283
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 5)) {
                // line 284
                echo "                        <div class=\"small-12 medium-2 columns no-padding\">
                            <div class=\"nedc-title\">
                                ";
                // line 286
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 287
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 290
                echo $this->getAttribute($context["news"], "highlight", array());
                echo " <a href=\"";
                echo Uri::base();
                echo "culture/news/";
                echo $this->getAttribute($context["news"], "slug", array());
                echo "\">...read more</a>
                            </div>
                        </div>
                        ";
            }
            // line 294
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 295
        echo "                        <div style=\"clear: both;\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"gallery\">
            <div class=\"gal-content\" style=\"position: relative;\">
                <div class=\"galc-wrapper\" data-equalizer=\"people\">
                    <div class=\"small-12 medium-offset-2 medium-3 columns no-padding galc-middle\" data-equalizer-watch=\"people\">
                    </div>
                    <div class=\"small-12 medium-4 columns no-padding galc-middle\" data-equalizer-watch=\"people\">
                        <div class=\"small-12 columns no-padding\">
                            <img src=\"";
        // line 309
        echo Uri::base();
        echo "assets/css/images/top-center.jpg\" style=\"width: 100%;\">
                        </div>
                    </div>
                    <div class=\"small-12 medium-3 columns no-padding\" data-equalizer-watch=\"people\">
                        <img src=\"";
        // line 313
        echo Uri::base();
        echo "assets/css/images/right-top.jpg\" style=\"width: 100%;\"/>
                    </div>
                    <div style=\"clear: both;\"></div>
                    <div class=\"mai-category\">
                        
                        ";
        // line 318
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data1"]) ? $context["photo_data1"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 319
            echo "                            <div class=\"small-12 medium-offset-3 medium-2 columns no-padding\">
                                ";
            // line 320
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-share")) {
                // line 321
                echo "                                    <a href=\"";
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                                ";
            }
            // line 323
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 325
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data1"]) ? $context["photo_data1"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 326
            echo "                            <div class=\"small-12 medium-2 columns no-padding\">
                                ";
            // line 327
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-sport")) {
                // line 328
                echo "                                    <a href=\"";
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                                ";
            }
            // line 330
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 332
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data1"]) ? $context["photo_data1"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 333
            echo "                            <div class=\"small-12 medium-2 columns no-padding\">
                                ";
            // line 334
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-study")) {
                // line 335
                echo "                                    <a href=\"";
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                                ";
            }
            // line 337
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 339
        echo "                        <div style=\"clear: both;\"></div>
                    </div>
                </div>
            </div>
            <div  class=\"gal-content-2\">
                <img src=\"";
        // line 344
        echo Uri::base();
        echo "assets/css/images/background-photo.jpg\" style=\"width: 100%;\"/>
                <div class=\"mai-category-2\">
                    <div class=\"small-12 medium-offset-2 medium-3 columns no-padding\">
                        <a><img src=\"";
        // line 347
        echo Uri::base();
        echo "/assets/css/images/quote-middle.jpg\" style=\"width: 100%;\"/> </a>
                    </div>
                    <div class=\"small-12 medium-2 columns no-padding\">
                        <a><img src=\"";
        // line 350
        echo Uri::base();
        echo "/assets/css/images/water.jpg\" style=\"width: 100%;\"/> </a>
                    </div>
                ";
        // line 352
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data2"]) ? $context["photo_data2"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 353
            echo "                    <div class=\"small-12 medium-2 columns no-padding\">
                    ";
            // line 354
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-talk")) {
                echo "    
                        <a href=\"";
                // line 355
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                    ";
            }
            // line 357
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 359
        echo "                    <div style=\"clear: both;\"></div>
                </div>
            </div>
            <div  class=\"gal-content-2\">
                <img src=\"";
        // line 363
        echo Uri::base();
        echo "assets/css/images/flower.jpg\" style=\"width: 100%;\"/>
                <div class=\"mai-category-2\">
                ";
        // line 365
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data3"]) ? $context["photo_data3"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            echo "    
                    <div class=\"small-12 medium-offset-3 medium-2 columns no-padding\">
                    ";
            // line 367
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-trip")) {
                echo "   
                        <a href=\"";
                // line 368
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                    ";
            }
            // line 370
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 372
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data3"]) ? $context["photo_data3"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 373
            echo "                    <div class=\"small-12 medium-2 columns no-padding\">
                    ";
            // line 374
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-birthday")) {
                echo "    
                        <a href=\"";
                // line 375
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                    ";
            }
            // line 377
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 379
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data3"]) ? $context["photo_data3"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 380
            echo "                    <div class=\"small-12 medium-2 columns no-padding\">
                    ";
            // line 381
            if (($this->getAttribute($context["photo"], "slug", array()) == "mai-otm")) {
                echo "   
                        <a href=\"";
                // line 382
                echo Uri::base();
                echo "culture/photo/";
                echo $this->getAttribute($context["photo"], "slug", array());
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/photo/";
                echo $this->getAttribute($context["photo"], "image", array());
                echo "\" style=\"width: 100%;\"/> </a>
                    ";
            }
            // line 384
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 386
        echo "                    <div style=\"clear: both;\"></div>
                </div>
            </div>
            <div class=\"gal-content\">
                <div class=\"galc-wrapper\" data-equalizer=\"foot\">
                    <div class=\"small-12 medium-offset-2 medium-3 columns no-padding galc-middle\" data-equalizer-watch=\"foot\">
                    </div>
                    <div class=\"small-12 medium-4 columns no-padding galc-middle\" data-equalizer-watch=\"foot\">
                        <div class=\"small-12 medium-12 columns no-padding\">
                            <img src=\"";
        // line 395
        echo Uri::base();
        echo "assets/css/images/bottom-center.jpg\" style=\"width: 100%;\">
                        </div>
                    </div>
                    <div class=\"small-12 medium-3 columns no-padding\" data-equalizer-watch=\"foot\">
                        <img src=\"";
        // line 399
        echo Uri::base();
        echo "assets/css/images/bottom-right.jpg\" style=\"width: 100%;\"/>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"video\">
            <div class=\"cv-content\">
                <div class=\"row\">
                    <div class=\"cvc-wrap\">
                        <div class=\"small-12 columns\">
                        
                            <div class=\"cvc-slider\">
                                <div class=\"cvc-slide\">
                                ";
        // line 415
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["video_data"]) ? $context["video_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 416
            echo "                                    <div class=\"cvc-item\" data-equalizer=\"video\">
                                        <div class=\"small-12 medium-4 columns no-padding cvc-detail\" data-equalizer-watch=\"video\" >
                                            <div class=\"small-2 columns no-padding\">
                                                <img src=\"";
            // line 419
            echo Uri::base();
            echo "assets/css/images/video-icon.jpg\" style=\"width: 100%;\"/>
                                            </div>
                                            <div class=\"small-10 columns\">
                                                <div class=\"cvd-title\">
                                                    ";
            // line 423
            echo $this->getAttribute($context["video"], "title", array());
            echo "<br/>
                                                </div>
                                                <div class=\"cvd-date\">
                                                    ";
            // line 426
            echo $this->getAttribute($context["video"], "created_at", array());
            echo "
                                                </div>
                                                <div class=\"cvd-content\">
                                                    ";
            // line 429
            $context["desc"] = twig_split_filter($this->env, $this->getAttribute($context["video"], "desc", array()), ".");
            // line 430
            echo "                                                    ";
            echo $this->getAttribute((isset($context["desc"]) ? $context["desc"] : null), 0, array(), "array");
            echo "
                                                </div>
                                            </div>
                                            <div style=\"clear: both;\"></div>
                                        </div>
                                        <div class=\"small-12 medium-offset-1 medium-7 columns no-padding \" data-equalizer-watch=\"video\">
                                            <iframe style=\"width: 100%; height: 100%;\" src=\"";
            // line 436
            echo $this->getAttribute($context["video"], "url", array());
            echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                        </div>
                                        <div style=\"clear: both;\"></div>
                                    </div> 
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 441
        echo "                                </div>
                            </div>
                            <div class=\"cvc-arrow\">
                                ";
        // line 444
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["video_data"]) ? $context["video_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 445
            echo "                                <div class=\"va-wrap\">
                                    <div id=\"video-prev\">
                                        <div class=\"va-preview-left\">
                                            <img src=\"";
            // line 448
            echo Uri::base();
            echo "media/video/";
            echo $this->getAttribute($context["video"], "image_prev", array());
            echo "\" style=\"width: 107px; height: 109px;\"/>
                                        </div>
                                    </div>
                                    <div id=\"video-next\">
                                        <div class=\"va-preview-left va-preview-right\">
                                            <img src=\"";
            // line 453
            echo Uri::base();
            echo "media/video/";
            echo $this->getAttribute($context["video"], "image_next", array());
            echo "\" style=\"width: 107px; height: 109px;\"/>
                                        </div>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div> 
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 459
        echo "                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"cv-background\">
                <img src=\"";
        // line 465
        echo Uri::base();
        echo "/assets/css/images/top-video.jpg\" style=\"width: 100%;\"/>
                <img src=\"";
        // line 466
        echo Uri::base();
        echo "assets/css/images/background-video.jpg\" style=\"width: 100%;\"/>
                <img src=\"";
        // line 467
        echo Uri::base();
        echo "/assets/css/images/bottom-video.jpg\" style=\"width: 100%;\"/>
            </div>
        </div>
    </div>
</div>
<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 475
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-item nm-normal\">
            <a id=\"about\">About</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"news\">News</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"gallery\">Photos</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a id=\"video\">Videos</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a href=\"#\">Blog</a>
        </div>
        <div class=\"nm-culture\">
            <a href=\"";
        // line 498
        echo Uri::base();
        echo "business\">Business</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a href=\"";
        // line 501
        echo Uri::base();
        echo "business#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 512
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 614
        echo "

</body>
</html>

";
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 19
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/component.css");
        echo "
        ";
        // line 21
        echo Asset::css("jquery.bxslider.css");
        echo "
        ";
        // line 22
        echo Asset::css("custom/slick.css");
        echo "
        ";
        // line 23
        echo Asset::css("custom/slick-theme.css");
        echo "
        ";
        // line 24
        echo Asset::css("custom/weather.css");
        echo "
        ";
        // line 25
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 27
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 28
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 30
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 31
        echo "    ";
    }

    // line 32
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 33
        echo "    ";
    }

    // line 512
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 513
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 514
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 515
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 516
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 517
        echo Asset::js("classie.js");
        echo "
    ";
        // line 518
        echo Asset::js("jquery.mousewheel.min.js");
        echo "
    ";
        // line 519
        echo Asset::js("jquery.easing.1.3.js");
        echo "
    ";
        // line 520
        echo Asset::js("jquery.bxslider.min.js");
        echo "
    ";
        // line 521
        echo Asset::js("slick.min.js");
        echo "
    <script>
        
        
        \$('.culture-1').css('width',\$( window ).width());

        \$(function() {

            \$('body').mousewheel(function(event, delta) {

                //console.log('test');
                var itemLeft;
                itemLeft = this.scrollLeft - (delta * \$( window ).width());
//                var culture = \$('.culture-1').next();
                \$('body').stop().animate({
                    scrollLeft: itemLeft
                }, 250);
                //console.log(delta);
                event.preventDefault();

            });

            \$(\"body\").keydown(function(e,delta) {

                if(e.keyCode == 37) { // left
                    \$('html, body').stop().animate({
                        scrollLeft: \"-=\" + \$( window ).width()
                    }, 250);
                    e.preventDefault();
                }
                else if(e.keyCode == 39) { // right
                    \$('html, body').stop().animate({
                        scrollLeft: \"+=\" + \$( window ).width()
                    }, 250);
                    e.preventDefault();
                }
            });

        });

        \$( window ).resize(function() {
            //console.log(\$( window ).width());
            \$('.culture-1').css('width',\$( window ).width());
        });

        \$('.cvc-slide').bxSlider({
            slideWidth: 1005,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            auto:false,
            slideMargin: 100,
            nextSelector: '#video-next',
            prevSelector: '#video-prev',
            prevText: '<div class=\"va-prev\"><div class=\"vap-top\">Previous</div><div class=\"vap-bottom\">001</div></div><div style=\"clear: both;\"></div>',
            nextText: '<div class=\"va-next\"><div class=\"vap-top\">Next</div><div class=\"vap-bottom\">003</div></div><div style=\"clear: both;\"></div>'
        });

        \$(\"#about\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollLeft: \$(\".about\").offset().left
            }, 1700);
            //});
        });

        \$(\"#news\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollLeft: \$(\".news\").offset().left
            }, 1700);
            //});
        });

        \$(\"#gallery\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollLeft: \$(\".gallery\").offset().left
            }, 1700);
            //});
        });

        \$(\"#video\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollLeft: \$(\".cv-content\").offset().left
            }, 1700);
            //});
        });

    </script>
    ";
        // line 612
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "culture.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1054 => 612,  960 => 521,  956 => 520,  952 => 519,  948 => 518,  944 => 517,  940 => 516,  936 => 515,  932 => 514,  927 => 513,  924 => 512,  920 => 33,  917 => 32,  913 => 31,  910 => 30,  903 => 28,  900 => 27,  894 => 25,  890 => 24,  886 => 23,  882 => 22,  878 => 21,  874 => 20,  870 => 19,  866 => 18,  862 => 17,  857 => 16,  854 => 15,  845 => 614,  843 => 512,  829 => 501,  823 => 498,  795 => 475,  784 => 467,  780 => 466,  776 => 465,  768 => 459,  754 => 453,  744 => 448,  739 => 445,  735 => 444,  730 => 441,  719 => 436,  709 => 430,  707 => 429,  701 => 426,  695 => 423,  688 => 419,  683 => 416,  679 => 415,  660 => 399,  653 => 395,  642 => 386,  635 => 384,  624 => 382,  620 => 381,  617 => 380,  612 => 379,  605 => 377,  594 => 375,  590 => 374,  587 => 373,  582 => 372,  575 => 370,  564 => 368,  560 => 367,  553 => 365,  548 => 363,  542 => 359,  535 => 357,  524 => 355,  520 => 354,  517 => 353,  513 => 352,  508 => 350,  502 => 347,  496 => 344,  489 => 339,  482 => 337,  470 => 335,  468 => 334,  465 => 333,  460 => 332,  453 => 330,  441 => 328,  439 => 327,  436 => 326,  431 => 325,  424 => 323,  412 => 321,  410 => 320,  407 => 319,  403 => 318,  395 => 313,  388 => 309,  372 => 295,  366 => 294,  355 => 290,  347 => 287,  343 => 286,  339 => 284,  336 => 283,  325 => 279,  317 => 276,  313 => 275,  309 => 273,  306 => 272,  295 => 268,  287 => 265,  283 => 264,  279 => 262,  276 => 261,  265 => 257,  257 => 254,  253 => 253,  249 => 251,  246 => 250,  235 => 246,  227 => 243,  223 => 242,  219 => 240,  216 => 239,  212 => 238,  200 => 229,  186 => 217,  175 => 212,  164 => 204,  158 => 201,  151 => 197,  148 => 196,  144 => 195,  137 => 190,  130 => 98,  125 => 94,  123 => 93,  119 => 90,  117 => 89,  113 => 86,  104 => 78,  102 => 74,  96 => 59,  90 => 56,  84 => 53,  68 => 40,  60 => 34,  57 => 32,  54 => 30,  51 => 27,  49 => 15,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/component.css') | raw }}*/
/*         {{ asset_css('jquery.bxslider.css') | raw }}*/
/*         {{ asset_css('custom/slick.css') | raw }}*/
/*         {{ asset_css('custom/slick-theme.css') | raw }}*/
/*         {{ asset_css('custom/weather.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body id="culture">*/
/* <div id="weather" class="small-12 columns"></div>*/
/* <div class="culture">*/
/*     <div class="culture-1">*/
/*         <div class="c1-wrap">*/
/*             <div class="culture-title">*/
/*                 <div class="small-12 columns">Culture</div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <img src="{{ base_url() }}assets/css/images/culturefirstview1.png" style="width: 100%;"/>*/
/*             <div class="c1-left">*/
/*                 <div class="small-12 columns">*/
/*                     {{ indonesia[0] }}*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div id="jkt-time" class="c1l-time">{{ indonesia[1] }}</div>*/
/*                     <div class="c1l-region">Jakarta</div>*/
/*                 </div>*/
/*                 {#<div class="small-12 columns">*/
/*                     <div class="c1l-time">{{ singapore }}</div>*/
/*                     <div class="c1l-region">Singapore</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">{{ thailand }}</div>*/
/*                     <div class="c1l-region">Bangkok</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">{{ vietnam }}</div>*/
/*                     <div class="c1l-region">Ho Chi Minh</div>*/
/*                 </div>#}*/
/*                 {#<div class="small-12 columns">*/
/*                     <div class="c1l-time">{{ philippines }}</div>*/
/*                     <div class="c1l-region">Taguig</div>*/
/*                 </div>#}*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*                     */
/*             <div class="c1-right">*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon">*/
/*                         {#{% if cloud_jkt > '35' %}#}*/
/*                             <div class="cloud"></div>*/
/*                             <div class="rain"></div>*/
/*                         {#{% endif %}#}*/
/*                         {#{% if cloud_jkt == '35' %}#}*/
/*                             <div class="rain"></div>*/
/*                             <div class="cloud"></div>*/
/*                         {#{% endif %}#}*/
/*                         {#{% if cloud_jkt < '35' %}#}*/
/*                             <div class="sun">*/
/*                                 <div class="rays"></div>*/
/*                             </div>*/
/*                         {#{% endif %} #}*/
/*                         </div>*/
/*                         30 °C*/
/*                     </div>   */
/*                     <div class="c1r-region">Jakarta</div>*/
/*                 </div>*/
/*                 {#<div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon">*/
/*                         {% if cloud_sin > '35' %}*/
/*                             {% if cond_sin == 'sun' %}*/
/*                                 <div class="cloud"></div>*/
/*                                 <div class="sun">*/
/*                                     <div class="rays"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             {% if cond_sin == 'lightning' %}*/
/*                                 <div class="cloud"></div>*/
/*                                 <div class="lightning">*/
/*                                     <div class="bolt"></div>*/
/*                                     <div class="bolt"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             {% if cond_sin == 'rain' %}*/
/*                                 <div class="cloud"></div>*/
/*                                 <div class="rain"></div>*/
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                         {% if cloud_sin == '35' %}*/
/*                             <div class="{{ cond_sin }}"></div>*/
/*                             <div class="cloud"></div>*/
/*                         {% endif %}*/
/*                         {% if cloud_sin < '35' %}*/
/*                             <div class="sun">*/
/*                                 <div class="rays"></div>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                         </div>*/
/*                         {{ temp_sin }} °C*/
/*                     </div>*/
/*                     <div class="c1r-region">Singapore</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon">*/
/*                         {% if cloud_tyo > '35' %}*/
/*                             {% if cond_tyo == 'sun' %}*/
/*                                 <div class="sun">*/
/*                                     <div class="rays"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             {% if cond_tyo == 'snow' %}*/
/*                                 <div class="cloud"></div>*/
/*                                 <div class="snow">*/
/*                                     <div class="flake"></div>*/
/*                                     <div class="flake"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             {% if cond_tyo == 'rain' %}*/
/*                                 <div class="cloud"></div>*/
/*                                 <div class="rain"></div>*/
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                         {% if cloud_tyo == '35' %}*/
/*                             {% if cond_tyo == 'snow' %}*/
/*                                 <div class="snow">*/
/*                                     <div class="flake"></div>*/
/*                                     <div class="flake"></div>*/
/*                                 </div>*/
/*                                 <div class="cloud"></div>*/
/*                             {% endif %}*/
/*                             <div class="{{ cond_tyo }}"></div>*/
/*                             <div class="cloud"></div>*/
/*                         {% endif %}*/
/*                         {% if cloud_tyo < '35' %}*/
/*                             {% if cond_tyo == 'sun' %}*/
/*                                 <div class="sun">*/
/*                                     <div class="rays"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             {% if cond_tyo == 'snow' %}*/
/*                                 <div class="snow">*/
/*                                     <div class="flake"></div>*/
/*                                     <div class="flake"></div>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                         </div>*/
/*                         {{ temp_tyo }} °C*/
/*                     </div>*/
/*                     <div class="c1r-region">Tokyo</div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>#}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="about">*/
/*             {% for about in about_maizen %}*/
/*             <div class="abl-image">*/
/*                 <img src="{{ base_url }}assets/css/images/about-image.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns ab-left" style="padding-left: 0;">*/
/*                 <div class="ab-title">*/
/*                     {{ about.header }}*/
/*                 </div>*/
/*                 <div class="abl-image" style="bottom: 50px; top: auto; z-index: -2;">*/
/*                     <img src="{{ base_url }}assets/css/images/ab-image.jpg" style="width: 100%;"/>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns ab-right">*/
/*                 <div class="abr-quote">*/
/*                     We believe in C.H.O.R.D*/
/*                 </div>*/
/*                 <div class="abr-detail">*/
/*                     {{ about.content }}*/
/*                 </div>*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         {% endfor %}*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="news" id="news-content">*/
/*             <div class="ne-back">*/
/*             </div>*/
/*             <div class="ne-content">*/
/*                 <div class="ne-title">*/
/*                     <h1>News</h1>*/
/*                 </div>*/
/*                 <div class="ne-detail">*/
/*                     <div class="ned-image">*/
/*                         <img src="{{ base_url() }}assets/css/images/news-image.jpg" style="width: 100%;"/>*/
/*                     </div>*/
/*                     <div class="ned-content">*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <div class="nedc-title">*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                             </div>*/
/*                         </div>*/
/*                     {% for news in news_data %}*/
/*                         {% if news.seq == 1 %}*/
/*                         <div class="small-12 medium-2 columns no-padding last-history">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }} <a href="{{ base_url() }}culture/news/{{ news.slug }}">...read more</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 2 %}*/
/*                         <div class="small-12 medium-2 columns no-padding third">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }} */
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }} <a href="{{ base_url() }}culture/news/{{ news.slug }}">...read more</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 3 %}*/
/*                         <div class="small-12 medium-2 columns no-padding second">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }} <a href="{{ base_url() }}culture/news/{{ news.slug }}">...read more</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 4 %}*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }} <a href="{{ base_url() }}culture/news/{{ news.slug }}">...read more</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 5 %}*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }} <a href="{{ base_url() }}culture/news/{{ news.slug }}">...read more</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                     {% endfor %}*/
/*                         <div style="clear: both;"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="gallery">*/
/*             <div class="gal-content" style="position: relative;">*/
/*                 <div class="galc-wrapper" data-equalizer="people">*/
/*                     <div class="small-12 medium-offset-2 medium-3 columns no-padding galc-middle" data-equalizer-watch="people">*/
/*                     </div>*/
/*                     <div class="small-12 medium-4 columns no-padding galc-middle" data-equalizer-watch="people">*/
/*                         <div class="small-12 columns no-padding">*/
/*                             <img src="{{ base_url() }}assets/css/images/top-center.jpg" style="width: 100%;">*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-3 columns no-padding" data-equalizer-watch="people">*/
/*                         <img src="{{ base_url() }}assets/css/images/right-top.jpg" style="width: 100%;"/>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                     <div class="mai-category">*/
/*                         */
/*                         {% for photo in photo_data1 %}*/
/*                             <div class="small-12 medium-offset-3 medium-2 columns no-padding">*/
/*                                 {% if photo.slug == 'mai-share' %}*/
/*                                     <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                         {% for photo in photo_data1 %}*/
/*                             <div class="small-12 medium-2 columns no-padding">*/
/*                                 {% if photo.slug == 'mai-sport' %}*/
/*                                     <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                         {% for photo in photo_data1 %}*/
/*                             <div class="small-12 medium-2 columns no-padding">*/
/*                                 {% if photo.slug == 'mai-study' %}*/
/*                                     <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                         <div style="clear: both;"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div  class="gal-content-2">*/
/*                 <img src="{{ base_url() }}assets/css/images/background-photo.jpg" style="width: 100%;"/>*/
/*                 <div class="mai-category-2">*/
/*                     <div class="small-12 medium-offset-2 medium-3 columns no-padding">*/
/*                         <a><img src="{{ base_url() }}/assets/css/images/quote-middle.jpg" style="width: 100%;"/> </a>*/
/*                     </div>*/
/*                     <div class="small-12 medium-2 columns no-padding">*/
/*                         <a><img src="{{ base_url() }}/assets/css/images/water.jpg" style="width: 100%;"/> </a>*/
/*                     </div>*/
/*                 {% for photo in photo_data2 %}*/
/*                     <div class="small-12 medium-2 columns no-padding">*/
/*                     {% if photo.slug == 'mai-talk' %}    */
/*                         <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*             <div  class="gal-content-2">*/
/*                 <img src="{{ base_url() }}assets/css/images/flower.jpg" style="width: 100%;"/>*/
/*                 <div class="mai-category-2">*/
/*                 {% for photo in photo_data3 %}    */
/*                     <div class="small-12 medium-offset-3 medium-2 columns no-padding">*/
/*                     {% if photo.slug == 'mai-trip' %}   */
/*                         <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                 {% for photo in photo_data3 %}*/
/*                     <div class="small-12 medium-2 columns no-padding">*/
/*                     {% if photo.slug == 'mai-birthday' %}    */
/*                         <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                 {% for photo in photo_data3 %}*/
/*                     <div class="small-12 medium-2 columns no-padding">*/
/*                     {% if photo.slug == 'mai-otm' %}   */
/*                         <a href="{{ base_url() }}culture/photo/{{ photo.slug }}"><img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;"/> </a>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="gal-content">*/
/*                 <div class="galc-wrapper" data-equalizer="foot">*/
/*                     <div class="small-12 medium-offset-2 medium-3 columns no-padding galc-middle" data-equalizer-watch="foot">*/
/*                     </div>*/
/*                     <div class="small-12 medium-4 columns no-padding galc-middle" data-equalizer-watch="foot">*/
/*                         <div class="small-12 medium-12 columns no-padding">*/
/*                             <img src="{{ base_url() }}assets/css/images/bottom-center.jpg" style="width: 100%;">*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="small-12 medium-3 columns no-padding" data-equalizer-watch="foot">*/
/*                         <img src="{{ base_url() }}assets/css/images/bottom-right.jpg" style="width: 100%;"/>*/
/*                     </div>*/
/*                     <div style="clear: both;"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="video">*/
/*             <div class="cv-content">*/
/*                 <div class="row">*/
/*                     <div class="cvc-wrap">*/
/*                         <div class="small-12 columns">*/
/*                         */
/*                             <div class="cvc-slider">*/
/*                                 <div class="cvc-slide">*/
/*                                 {% for video in video_data %}*/
/*                                     <div class="cvc-item" data-equalizer="video">*/
/*                                         <div class="small-12 medium-4 columns no-padding cvc-detail" data-equalizer-watch="video" >*/
/*                                             <div class="small-2 columns no-padding">*/
/*                                                 <img src="{{ base_url() }}assets/css/images/video-icon.jpg" style="width: 100%;"/>*/
/*                                             </div>*/
/*                                             <div class="small-10 columns">*/
/*                                                 <div class="cvd-title">*/
/*                                                     {{ video.title }}<br/>*/
/*                                                 </div>*/
/*                                                 <div class="cvd-date">*/
/*                                                     {{ video.created_at }}*/
/*                                                 </div>*/
/*                                                 <div class="cvd-content">*/
/*                                                     {% set desc = video.desc | split('.') %}*/
/*                                                     {{ desc[0] }}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div style="clear: both;"></div>*/
/*                                         </div>*/
/*                                         <div class="small-12 medium-offset-1 medium-7 columns no-padding " data-equalizer-watch="video">*/
/*                                             <iframe style="width: 100%; height: 100%;" src="{{ video.url }}" frameborder="0" allowfullscreen></iframe>*/
/*                                         </div>*/
/*                                         <div style="clear: both;"></div>*/
/*                                     </div> */
/*                                 {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="cvc-arrow">*/
/*                                 {% for video in video_data %}*/
/*                                 <div class="va-wrap">*/
/*                                     <div id="video-prev">*/
/*                                         <div class="va-preview-left">*/
/*                                             <img src="{{ base_url() }}media/video/{{ video.image_prev }}" style="width: 107px; height: 109px;"/>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div id="video-next">*/
/*                                         <div class="va-preview-left va-preview-right">*/
/*                                             <img src="{{ base_url() }}media/video/{{ video.image_next }}" style="width: 107px; height: 109px;"/>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div style="clear: both;"></div>*/
/*                                 </div> */
/*                                 {% endfor %}*/
/*                             </div> */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="cv-background">*/
/*                 <img src="{{ base_url() }}/assets/css/images/top-video.jpg" style="width: 100%;"/>*/
/*                 <img src="{{ base_url() }}assets/css/images/background-video.jpg" style="width: 100%;"/>*/
/*                 <img src="{{ base_url() }}/assets/css/images/bottom-video.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="about">About</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="news">News</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="gallery">Photos</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a id="video">Videos</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a href="#">Blog</a>*/
/*         </div>*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}business">Business</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a href="{{ base_url() }}business#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('classie.js') | raw }}*/
/*     {{ asset_js('jquery.mousewheel.min.js') | raw }}*/
/*     {{ asset_js('jquery.easing.1.3.js') | raw }}*/
/*     {{ asset_js('jquery.bxslider.min.js') | raw }}*/
/*     {{ asset_js('slick.min.js') | raw }}*/
/*     <script>*/
/*         */
/*         */
/*         $('.culture-1').css('width',$( window ).width());*/
/* */
/*         $(function() {*/
/* */
/*             $('body').mousewheel(function(event, delta) {*/
/* */
/*                 //console.log('test');*/
/*                 var itemLeft;*/
/*                 itemLeft = this.scrollLeft - (delta * $( window ).width());*/
/* //                var culture = $('.culture-1').next();*/
/*                 $('body').stop().animate({*/
/*                     scrollLeft: itemLeft*/
/*                 }, 250);*/
/*                 //console.log(delta);*/
/*                 event.preventDefault();*/
/* */
/*             });*/
/* */
/*             $("body").keydown(function(e,delta) {*/
/* */
/*                 if(e.keyCode == 37) { // left*/
/*                     $('html, body').stop().animate({*/
/*                         scrollLeft: "-=" + $( window ).width()*/
/*                     }, 250);*/
/*                     e.preventDefault();*/
/*                 }*/
/*                 else if(e.keyCode == 39) { // right*/
/*                     $('html, body').stop().animate({*/
/*                         scrollLeft: "+=" + $( window ).width()*/
/*                     }, 250);*/
/*                     e.preventDefault();*/
/*                 }*/
/*             });*/
/* */
/*         });*/
/* */
/*         $( window ).resize(function() {*/
/*             //console.log($( window ).width());*/
/*             $('.culture-1').css('width',$( window ).width());*/
/*         });*/
/* */
/*         $('.cvc-slide').bxSlider({*/
/*             slideWidth: 1005,*/
/*             minSlides: 1,*/
/*             maxSlides: 1,*/
/*             moveSlides: 1,*/
/*             auto:false,*/
/*             slideMargin: 100,*/
/*             nextSelector: '#video-next',*/
/*             prevSelector: '#video-prev',*/
/*             prevText: '<div class="va-prev"><div class="vap-top">Previous</div><div class="vap-bottom">001</div></div><div style="clear: both;"></div>',*/
/*             nextText: '<div class="va-next"><div class="vap-top">Next</div><div class="vap-bottom">003</div></div><div style="clear: both;"></div>'*/
/*         });*/
/* */
/*         $("#about").click(function (){*/
/*             //$(this).animate(function(){*/
/*             $('html, body').animate({*/
/*                 scrollLeft: $(".about").offset().left*/
/*             }, 1700);*/
/*             //});*/
/*         });*/
/* */
/*         $("#news").click(function (){*/
/*             //$(this).animate(function(){*/
/*             $('html, body').animate({*/
/*                 scrollLeft: $(".news").offset().left*/
/*             }, 1700);*/
/*             //});*/
/*         });*/
/* */
/*         $("#gallery").click(function (){*/
/*             //$(this).animate(function(){*/
/*             $('html, body').animate({*/
/*                 scrollLeft: $(".gallery").offset().left*/
/*             }, 1700);*/
/*             //});*/
/*         });*/
/* */
/*         $("#video").click(function (){*/
/*             //$(this).animate(function(){*/
/*             $('html, body').animate({*/
/*                 scrollLeft: $(".cv-content").offset().left*/
/*             }, 1700);*/
/*             //});*/
/*         });*/
/* */
/*     </script>*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* */
/* </body>*/
/* </html>*/
/* */
/* */
