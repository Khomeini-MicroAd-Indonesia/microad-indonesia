<?php

/* culture.twig */
class __TwigTemplate_60d54c7d270548cd7ff561bbc62cbd8234b5dc028c37c182df7f5de8af57571f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 25
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 28
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 30
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 32
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 38
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body id=\"culture\">

<div class=\"culture\">
    <div class=\"culture-1\">
        <div class=\"c1-wrap\">
            <div class=\"culture-title\">
                <div class=\"small-12 columns\">Culture</div>
                <div style=\"clear: both;\"></div>
            </div>
            <img src=\"";
        // line 51
        echo Uri::base();
        echo "/assets/css/images/culturefirstview.png\" style=\"width: 100%;\"/>
            <div class=\"c1-left\">
                <div class=\"small-12 columns\">
                    Feb' 23
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1l-time\">09:36:07</div>
                    <div class=\"c1l-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1l-time\">09:36:07</div>
                    <div class=\"c1l-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1l-time\">09:36:07</div>
                    <div class=\"c1l-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1l-time\">09:36:07</div>
                    <div class=\"c1l-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1l-time\">09:36:07</div>
                    <div class=\"c1l-region\">jakarta</div>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"c1-right\">
                <div class=\"small-12 columns\">
                    <div class=\"c1r-icon\">
                        <div class=\"icon rainy\">
                            <div class=\"cloud\"></div>
                            <div class=\"rain\"></div>
                        </div>
                    </div>
                    <div class=\"c1r-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1r-icon\">
                        <div class=\"icon rainy\">
                            <div class=\"cloud\"></div>
                            <div class=\"rain\"></div>
                        </div>
                    </div>
                    <div class=\"c1r-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1r-icon\">
                        <div class=\"icon rainy\">
                            <div class=\"cloud\"></div>
                            <div class=\"rain\"></div>
                        </div>
                    </div>
                    <div class=\"c1r-region\">jakarta</div>
                </div>
                <div class=\"small-12 columns\">
                    <div class=\"c1r-icon\">
                        <div class=\"icon rainy\">
                            <div class=\"cloud\"></div>
                            <div class=\"rain\"></div>
                        </div>
                    </div>
                    <div class=\"c1r-region\">jakarta</div>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"about\">
            <div class=\"abl-image\">
                <img src=\"";
        // line 122
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "assets/css/images/about-image.jpg\" style=\"width: 100%;\"/>
            </div>
            <div class=\"small-12 medium-6 columns ab-left\">
                <div class=\"ab-title\">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                </div>
            </div>
            <div class=\"small-12 medium-6 columns ab-right\">
                <div class=\"abr-quote\">
                    WORK TO LIVE + DONT’ LIVE TO WORK
                </div>
            ";
        // line 133
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["about_maizen"]) ? $context["about_maizen"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["about"]) {
            // line 134
            echo "                <div class=\"abr-detail\">
                    ";
            // line 135
            echo $this->getAttribute($context["about"], "content", array());
            echo "
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['about'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "            </div>
            <div style=\"clear: both;\"></div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"news\">
            <div class=\"ne-back\">
            </div>
            <div class=\"ne-content\">
                <div class=\"ne-title\">
                    <h1>News</h1>
                </div>
                <div class=\"ne-detail\">
                    <div class=\"ned-image\">
                        <img src=\"";
        // line 152
        echo Uri::base();
        echo "assets/css/images/news-image.jpg\" style=\"width: 100%;\"/>
                    </div>
                    <div class=\"ned-content\">
                        <div class=\"small-12 medium-2 columns no-padding\">
                            <div class=\"nedc-title\">
                            </div>
                            <div class=\"nedc-detail\">
                            </div>
                        </div>
                    ";
        // line 161
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 162
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 1)) {
                // line 163
                echo "                        <div class=\"small-12 medium-2 columns no-padding last-history\">
                            <div class=\"nedc-title\">
                                ";
                // line 165
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 166
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo " ";
                echo $this->getAttribute($context["news"], "seq", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 169
                echo $this->getAttribute($context["news"], "highlight", array());
                echo "
                            </div>
                        </div>
                        ";
            }
            // line 173
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 2)) {
                // line 174
                echo "                        <div class=\"small-12 medium-2 columns no-padding third\">
                            <div class=\"nedc-title\">
                                ";
                // line 176
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 177
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo " ";
                echo $this->getAttribute($context["news"], "seq", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 180
                echo $this->getAttribute($context["news"], "highlight", array());
                echo "
                            </div>
                        </div>
                        ";
            }
            // line 184
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 3)) {
                // line 185
                echo "                        <div class=\"small-12 medium-2 columns no-padding second\">
                            <div class=\"nedc-title\">
                                ";
                // line 187
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 188
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo " ";
                echo $this->getAttribute($context["news"], "seq", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 191
                echo $this->getAttribute($context["news"], "highlight", array());
                echo "
                            </div>
                        </div>
                        ";
            }
            // line 195
            echo "                        ";
            if (($this->getAttribute($context["news"], "seq", array()) == 4)) {
                // line 196
                echo "                        <div class=\"small-12 medium-2 columns no-padding\">
                            <div class=\"nedc-title\">
                                ";
                // line 198
                echo $this->getAttribute($context["news"], "year", array());
                echo "<br/>
                                ";
                // line 199
                echo $this->getAttribute($context["news"], "month", array());
                echo " ‘";
                echo $this->getAttribute($context["news"], "day", array());
                echo " ";
                echo $this->getAttribute($context["news"], "seq", array());
                echo "
                            </div>
                            <div class=\"nedc-detail\">
                                ";
                // line 202
                echo $this->getAttribute($context["news"], "highlight", array());
                echo "
                            </div>
                        </div>
                        ";
            }
            // line 206
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 207
        echo "                        <div style=\"clear: both;\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"culture-1\">
        <div class=\"gallery\">
            <div class=\"gal-background\">
                <img src=\"";
        // line 216
        echo Uri::base();
        echo "assets/css/images/background-photo.jpg\" style=\"width: 100%;\"/>
            </div>
        </div>
    </div>
</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <img src=\"";
        // line 225
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-item nm-normal\">
            <a href=\"#\">About</a>
        </div>
        <div class=\"nm-item nm-normal\">
            <a href=\"#\">Portfolio</a>
        </div>
        <div class=\"nm-culture\">
            <a href=\"#\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"#contact\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 253
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 292
        echo "

</body>
</html>

";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 16
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/component.css");
        echo "
        ";
        // line 19
        echo Asset::css("jquery.bxslider.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/slick.css");
        echo "
        ";
        // line 21
        echo Asset::css("custom/slick-theme.css");
        echo "
        ";
        // line 22
        echo Asset::css("custom/weather.css");
        echo "
        ";
        // line 23
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 25
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 26
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 28
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 29
        echo "    ";
    }

    // line 30
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 31
        echo "    ";
    }

    // line 253
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 254
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 255
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 256
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 257
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 258
        echo Asset::js("classie.js");
        echo "
    ";
        // line 259
        echo Asset::js("jquery.mousewheel.min.js");
        echo "
    ";
        // line 260
        echo Asset::js("jquery.easing.1.3.js");
        echo "
    ";
        // line 261
        echo Asset::js("jquery.bxslider.min.js");
        echo "
    ";
        // line 262
        echo Asset::js("slick.min.js");
        echo "
    ";
        // line 263
        echo Asset::js("app.js");
        echo "
    <script>

        \$('.culture-1').css('width',\$( window ).width());

        \$(function() {

            \$(\"body\").mousewheel(function(event, delta) {

                console.log('test');
                var itemLeft;
                itemLeft = this.scrollLeft - (delta * \$( window ).width());
//                var culture = \$('.culture-1').next();
                \$('body').stop().animate({
                    scrollLeft: itemLeft
                }, 250);
                console.log(delta);
                event.preventDefault();

            });

        });

        \$( window ).resize(function() {
            console.log(\$( window ).width());
            \$('.culture-1').css('width',\$( window ).width());
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "culture.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  511 => 263,  507 => 262,  503 => 261,  499 => 260,  495 => 259,  491 => 258,  487 => 257,  483 => 256,  479 => 255,  474 => 254,  471 => 253,  467 => 31,  464 => 30,  460 => 29,  457 => 28,  450 => 26,  447 => 25,  441 => 23,  437 => 22,  433 => 21,  429 => 20,  425 => 19,  421 => 18,  417 => 17,  413 => 16,  409 => 15,  404 => 14,  401 => 13,  392 => 292,  390 => 253,  359 => 225,  347 => 216,  336 => 207,  330 => 206,  323 => 202,  313 => 199,  309 => 198,  305 => 196,  302 => 195,  295 => 191,  285 => 188,  281 => 187,  277 => 185,  274 => 184,  267 => 180,  257 => 177,  253 => 176,  249 => 174,  246 => 173,  239 => 169,  229 => 166,  225 => 165,  221 => 163,  218 => 162,  214 => 161,  202 => 152,  186 => 138,  177 => 135,  174 => 134,  170 => 133,  156 => 122,  82 => 51,  66 => 38,  58 => 32,  55 => 30,  52 => 28,  49 => 25,  47 => 13,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/component.css') | raw }}*/
/*         {{ asset_css('jquery.bxslider.css') | raw }}*/
/*         {{ asset_css('custom/slick.css') | raw }}*/
/*         {{ asset_css('custom/slick-theme.css') | raw }}*/
/*         {{ asset_css('custom/weather.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body id="culture">*/
/* */
/* <div class="culture">*/
/*     <div class="culture-1">*/
/*         <div class="c1-wrap">*/
/*             <div class="culture-title">*/
/*                 <div class="small-12 columns">Culture</div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <img src="{{ base_url() }}/assets/css/images/culturefirstview.png" style="width: 100%;"/>*/
/*             <div class="c1-left">*/
/*                 <div class="small-12 columns">*/
/*                     Feb' 23*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">09:36:07</div>*/
/*                     <div class="c1l-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">09:36:07</div>*/
/*                     <div class="c1l-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">09:36:07</div>*/
/*                     <div class="c1l-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">09:36:07</div>*/
/*                     <div class="c1l-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1l-time">09:36:07</div>*/
/*                     <div class="c1l-region">jakarta</div>*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <div class="c1-right">*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon rainy">*/
/*                             <div class="cloud"></div>*/
/*                             <div class="rain"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="c1r-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon rainy">*/
/*                             <div class="cloud"></div>*/
/*                             <div class="rain"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="c1r-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon rainy">*/
/*                             <div class="cloud"></div>*/
/*                             <div class="rain"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="c1r-region">jakarta</div>*/
/*                 </div>*/
/*                 <div class="small-12 columns">*/
/*                     <div class="c1r-icon">*/
/*                         <div class="icon rainy">*/
/*                             <div class="cloud"></div>*/
/*                             <div class="rain"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="c1r-region">jakarta</div>*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="about">*/
/*             <div class="abl-image">*/
/*                 <img src="{{ base_url }}assets/css/images/about-image.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns ab-left">*/
/*                 <div class="ab-title">*/
/*                     Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.*/
/*                 </div>*/
/*             </div>*/
/*             <div class="small-12 medium-6 columns ab-right">*/
/*                 <div class="abr-quote">*/
/*                     WORK TO LIVE + DONT’ LIVE TO WORK*/
/*                 </div>*/
/*             {% for about in about_maizen %}*/
/*                 <div class="abr-detail">*/
/*                     {{ about.content }}*/
/*                 </div>*/
/*             {% endfor %}*/
/*             </div>*/
/*             <div style="clear: both;"></div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="news">*/
/*             <div class="ne-back">*/
/*             </div>*/
/*             <div class="ne-content">*/
/*                 <div class="ne-title">*/
/*                     <h1>News</h1>*/
/*                 </div>*/
/*                 <div class="ne-detail">*/
/*                     <div class="ned-image">*/
/*                         <img src="{{ base_url() }}assets/css/images/news-image.jpg" style="width: 100%;"/>*/
/*                     </div>*/
/*                     <div class="ned-content">*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <div class="nedc-title">*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                             </div>*/
/*                         </div>*/
/*                     {% for news in news_data %}*/
/*                         {% if news.seq == 1 %}*/
/*                         <div class="small-12 medium-2 columns no-padding last-history">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }} {{ news.seq }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }}*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 2 %}*/
/*                         <div class="small-12 medium-2 columns no-padding third">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }} {{ news.seq }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }}*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 3 %}*/
/*                         <div class="small-12 medium-2 columns no-padding second">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }} {{ news.seq }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }}*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if news.seq == 4 %}*/
/*                         <div class="small-12 medium-2 columns no-padding">*/
/*                             <div class="nedc-title">*/
/*                                 {{ news.year }}<br/>*/
/*                                 {{ news.month }} ‘{{ news.day }} {{ news.seq }}*/
/*                             </div>*/
/*                             <div class="nedc-detail">*/
/*                                 {{ news.highlight }}*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                     {% endfor %}*/
/*                         <div style="clear: both;"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="culture-1">*/
/*         <div class="gallery">*/
/*             <div class="gal-background">*/
/*                 <img src="{{ base_url() }}assets/css/images/background-photo.jpg" style="width: 100%;"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <img src="{{ base_url() }}assets/css/images/logo-mai.png"/>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-item nm-normal">*/
/*             <a href="#">About</a>*/
/*         </div>*/
/*         <div class="nm-item nm-normal">*/
/*             <a href="#">Portfolio</a>*/
/*         </div>*/
/*         <div class="nm-culture">*/
/*             <a href="#">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="#contact">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('classie.js') | raw }}*/
/*     {{ asset_js('jquery.mousewheel.min.js') | raw }}*/
/*     {{ asset_js('jquery.easing.1.3.js') | raw }}*/
/*     {{ asset_js('jquery.bxslider.min.js') | raw }}*/
/*     {{ asset_js('slick.min.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/*     <script>*/
/* */
/*         $('.culture-1').css('width',$( window ).width());*/
/* */
/*         $(function() {*/
/* */
/*             $("body").mousewheel(function(event, delta) {*/
/* */
/*                 console.log('test');*/
/*                 var itemLeft;*/
/*                 itemLeft = this.scrollLeft - (delta * $( window ).width());*/
/* //                var culture = $('.culture-1').next();*/
/*                 $('body').stop().animate({*/
/*                     scrollLeft: itemLeft*/
/*                 }, 250);*/
/*                 console.log(delta);*/
/*                 event.preventDefault();*/
/* */
/*             });*/
/* */
/*         });*/
/* */
/*         $( window ).resize(function() {*/
/*             console.log($( window ).width());*/
/*             $('.culture-1').css('width',$( window ).width());*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* </body>*/
/* </html>*/
/* */
/* */
