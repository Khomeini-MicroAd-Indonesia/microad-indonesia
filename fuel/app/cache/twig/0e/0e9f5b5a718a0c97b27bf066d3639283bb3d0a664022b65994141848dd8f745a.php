<?php

/* superadmin.twig */
class __TwigTemplate_b39b74c5eb4154ecc14625d9ff45f01cf15e413d2b3a85859265c58d7420e618 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("backend/base.twig", "superadmin.twig", 1);
        $this->blocks = array(
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"form-box\" id=\"login-box\">
\t";
        // line 5
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 6
            echo "\t<div class=\"alert alert-danger alert-dismissable\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t\t";
            // line 8
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
\t</div>
\t";
        }
        // line 11
        echo "\t<div class=\"header\">Superadmin Registration</div>
\t<form action=\"\" method=\"post\">
\t\t<div class=\"body bg-gray\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"fullname\" class=\"form-control\" placeholder=\"Fullname*\" value=\"";
        // line 15
        echo (isset($context["txtfullname"]) ? $context["txtfullname"] : null);
        echo "\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"phone\" class=\"form-control\" placeholder=\"Phone\" value=\"";
        // line 18
        echo (isset($context["txtphone"]) ? $context["txtphone"] : null);
        echo "\" />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"email\" class=\"form-control\" placeholder=\"Email*\" value=\"";
        // line 21
        echo (isset($context["txtemail"]) ? $context["txtemail"] : null);
        echo "\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password*\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" name=\"password_confirm\" class=\"form-control\" placeholder=\"Password Confirm*\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group text-red\">
\t\t\t\t*) Required
\t\t\t</div>
\t\t</div>
\t\t<div class=\"footer\">                                                               
\t\t\t<button type=\"submit\" class=\"btn bg-olive btn-block\">Submit</button>
\t\t</div>
\t</form>
</div>
";
    }

    public function getTemplateName()
    {
        return "superadmin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 21,  58 => 18,  52 => 15,  46 => 11,  40 => 8,  36 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "backend/base.twig" %}*/
/* */
/* {% block backend_content %}*/
/* <div class="form-box" id="login-box">*/
/* 	{% if (error_message | length > 0) %}*/
/* 	<div class="alert alert-danger alert-dismissable">*/
/* 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 		{{ error_message | raw }}*/
/* 	</div>*/
/* 	{% endif %}*/
/* 	<div class="header">Superadmin Registration</div>*/
/* 	<form action="" method="post">*/
/* 		<div class="body bg-gray">*/
/* 			<div class="form-group">*/
/* 				<input type="text" name="fullname" class="form-control" placeholder="Fullname*" value="{{ txtfullname }}" required />*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<input type="text" name="phone" class="form-control" placeholder="Phone" value="{{ txtphone }}" />*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<input type="text" name="email" class="form-control" placeholder="Email*" value="{{ txtemail }}" required />*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<input type="password" name="password" class="form-control" placeholder="Password*" required />*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<input type="password" name="password_confirm" class="form-control" placeholder="Password Confirm*" required />*/
/* 			</div>*/
/* 			<div class="form-group text-red">*/
/* 				*) Required*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="footer">                                                               */
/* 			<button type="submit" class="btn bg-olive btn-block">Submit</button>*/
/* 		</div>*/
/* 	</form>*/
/* </div>*/
/* {% endblock %}*/
