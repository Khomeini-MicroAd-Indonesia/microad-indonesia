<?php

/* home.twig */
class __TwigTemplate_35f518ec3cae6b43e0b1e2ebaf7ce76bc5967f7a81307ec11ff00469a6f2fcf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!DOCTYPE html>
    <html class=\"ie8\">
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        ";
        // line 11
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 16
        echo "        <style>
            .edgeLoad-EDGE-7814028 { visibility:hidden; }
            .edgeLoad-EDGE-14088978 { visibility:hidden; }
            .edgeLoad-EDGE-15798491 { visibility:hidden; }
        </style>
        ";
        // line 21
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 24
        echo "        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '";
        // line 30
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>

    <div id=\"base\"></div>
    <div id=\"bg-city\"></div>
    <div id=\"tokyo-eye\">
        <div id=\"Stage-eye\" class=\"EDGE-7814028\"></div>
    </div>
    <div id=\"tokyo-tower\"></div>
    <div id=\"tokyo-crane\">
        <div id=\"Stage-crane\" class=\"EDGE-14088978\"></div>
    </div>
    <div id=\"tokyo\"></div>
    <div id=\"tokyo-street\"></div>
    <div id=\"tokyo-shinkansen\">
        <div id=\"Stage-train\" class=\"EDGE-15798491\"></div>
    </div>
    <div id=\"gen-city\"></div>
    <div id=\"monas\"></div>
    <div id=\"jakarta\"></div>
    <div id=\"jakarta-street\"></div>
    <div id=\"logo-mai2\"></div>
    <div id=\"cyber-agent\">
        <img src=\"";
        // line 56
        echo Uri::base();
        echo "/assets/css/images/cyber-agent.png\"/>
    </div>


    <div id=\"middleNav\">
        <div id=\"pull-left\" class=\"business-container\">
            <a href=\"";
        // line 62
        echo Uri::base();
        echo "business\">
                <div class=\"business\">
                    <div class=\"side\">
                        <div class=\"side-content\">
                            Here to see our business
                        </div>
                    </div>
                    <div class=\"side back\"><img src=\"";
        // line 69
        echo Uri::base();
        echo "/assets/css/images/business-img.jpg\" alt=\"business-nav\"></div>
                </div>
            </a>
        </div>
        <div id=\"pull-right\" class=\"culture-container\">
            <div class=\"business\">
                <div class=\"side\">
                    <div class=\"side-content\" style=\"margin-left: 25px; text-align: right\">
                        Here to see our culture
                    </div>
                </div>
                <div class=\"side back\"><a href=\"#\"><img src=\"";
        // line 80
        echo Uri::base();
        echo "/assets/css/images/culture-img.jpg\" alt=\"culture-nav\"></a></div>
            </div>
        </div>
    </div>

    <nav id=\"main-nav\">
        <div id=\"logo-mai\">
            <div class=\"lm-image\">
                <a href=\"";
        // line 88
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo-mai.png\"/></a>
            </div>
            <div class=\"lm-title\">
                MicroAd Indonesia 2016
            </div>
        </div>
        <div class=\"nav-menu\">
            <div class=\"nm-item\">
                <a href=\"#contact\">Contact</a>
            </div>
            <div class=\"nm-item separator\">
                <a href=\"#career\">Career</a>
            </div>
            <div class=\"nm-item\">
                <a href=\"#contact\">MicroAd Asia</a>
            </div>
        </div>
    </nav>

    ";
        // line 107
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 142
        echo "    </body>
    </html>";
    }

    // line 11
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 12
        echo "            ";
        echo Asset::css("custom/normalize.css");
        echo "
            ";
        // line 13
        echo Asset::css("custom/magic.min.css");
        echo "
            ";
        // line 14
        echo Asset::css("custom/main.css");
        echo "
        ";
    }

    // line 21
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 22
        echo "            ";
        echo Asset::js("modernizr.js");
        echo "
        ";
    }

    // line 107
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 108
        echo "        ";
        echo Asset::js("vendor/jquery.min.js");
        echo "
        ";
        // line 109
        echo Asset::js("parallax.js");
        echo "
        ";
        // line 110
        echo Asset::js("publish/edge_includes/edge.6.0.0.min.js");
        echo "
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-eye', 'EDGE-7814028', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"175px\",
                height: \"175px\"
            }, {\"dom\":{}}, {\"style\":{\"\${symbolSelector}\":{\"isStage\":\"true\",\"rect\":[\"undefined\",\"undefined\",\"175px\",\"175px\"],\"fill\":[\"rgba(255,255,255,1)\"]}},\"dom\":{}});
        </script>
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-crane', 'EDGE-14088978', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"291px\",
                height: \"148px\"
            }, {\"dom\":{}}, {\"dom\":{}});
        </script>
        <script>
            AdobeEdge.loadComposition('assets/js/publish/tokyo-shinkansen', 'EDGE-15798491', {
                scaleToFit: \"none\",
                centerStage: \"none\",
                minW: \"0px\",
                maxW: \"undefined\",
                width: \"4000px\",
                height: \"56px\"
            }, {\"dom\":{}}, {\"dom\":{}});
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 110,  199 => 109,  194 => 108,  191 => 107,  184 => 22,  181 => 21,  175 => 14,  171 => 13,  166 => 12,  163 => 11,  158 => 142,  156 => 107,  132 => 88,  121 => 80,  107 => 69,  97 => 62,  88 => 56,  59 => 30,  51 => 24,  49 => 21,  42 => 16,  40 => 11,  32 => 6,  28 => 5,  22 => 1,);
    }
}
/*     <!DOCTYPE html>*/
/*     <html class="ie8">*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{{ meta_title }}</title>*/
/*         <meta name="description" content="{{ meta_desc }}">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>*/
/*         <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*         {% block frontend_css %}*/
/*             {{ asset_css('custom/normalize.css') | raw }}*/
/*             {{ asset_css('custom/magic.min.css') | raw }}*/
/*             {{ asset_css('custom/main.css') | raw }}*/
/*         {% endblock %}*/
/*         <style>*/
/*             .edgeLoad-EDGE-7814028 { visibility:hidden; }*/
/*             .edgeLoad-EDGE-14088978 { visibility:hidden; }*/
/*             .edgeLoad-EDGE-15798491 { visibility:hidden; }*/
/*         </style>*/
/*         {% block frontend_head_js %}*/
/*             {{ asset_js('modernizr.js') }}*/
/*         {% endblock %}*/
/*         <script>*/
/*             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*             })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*             ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*             ga('send', 'pageview');*/
/*         </script>*/
/*     </head>*/
/*     <body>*/
/* */
/*     <div id="base"></div>*/
/*     <div id="bg-city"></div>*/
/*     <div id="tokyo-eye">*/
/*         <div id="Stage-eye" class="EDGE-7814028"></div>*/
/*     </div>*/
/*     <div id="tokyo-tower"></div>*/
/*     <div id="tokyo-crane">*/
/*         <div id="Stage-crane" class="EDGE-14088978"></div>*/
/*     </div>*/
/*     <div id="tokyo"></div>*/
/*     <div id="tokyo-street"></div>*/
/*     <div id="tokyo-shinkansen">*/
/*         <div id="Stage-train" class="EDGE-15798491"></div>*/
/*     </div>*/
/*     <div id="gen-city"></div>*/
/*     <div id="monas"></div>*/
/*     <div id="jakarta"></div>*/
/*     <div id="jakarta-street"></div>*/
/*     <div id="logo-mai2"></div>*/
/*     <div id="cyber-agent">*/
/*         <img src="{{ base_url() }}/assets/css/images/cyber-agent.png"/>*/
/*     </div>*/
/* */
/* */
/*     <div id="middleNav">*/
/*         <div id="pull-left" class="business-container">*/
/*             <a href="{{ base_url() }}business">*/
/*                 <div class="business">*/
/*                     <div class="side">*/
/*                         <div class="side-content">*/
/*                             Here to see our business*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="side back"><img src="{{ base_url() }}/assets/css/images/business-img.jpg" alt="business-nav"></div>*/
/*                 </div>*/
/*             </a>*/
/*         </div>*/
/*         <div id="pull-right" class="culture-container">*/
/*             <div class="business">*/
/*                 <div class="side">*/
/*                     <div class="side-content" style="margin-left: 25px; text-align: right">*/
/*                         Here to see our culture*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="side back"><a href="#"><img src="{{ base_url() }}/assets/css/images/culture-img.jpg" alt="culture-nav"></a></div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <nav id="main-nav">*/
/*         <div id="logo-mai">*/
/*             <div class="lm-image">*/
/*                 <a href="{{ base_url() }}"><img src="{{ base_url() }}/assets/css/images/logo-mai.png"/></a>*/
/*             </div>*/
/*             <div class="lm-title">*/
/*                 MicroAd Indonesia 2016*/
/*             </div>*/
/*         </div>*/
/*         <div class="nav-menu">*/
/*             <div class="nm-item">*/
/*                 <a href="#contact">Contact</a>*/
/*             </div>*/
/*             <div class="nm-item separator">*/
/*                 <a href="#career">Career</a>*/
/*             </div>*/
/*             <div class="nm-item">*/
/*                 <a href="#contact">MicroAd Asia</a>*/
/*             </div>*/
/*         </div>*/
/*     </nav>*/
/* */
/*     {% block frontend_js %}*/
/*         {{ asset_js('vendor/jquery.min.js') | raw }}*/
/*         {{ asset_js('parallax.js') | raw }}*/
/*         {{ asset_js('publish/edge_includes/edge.6.0.0.min.js') | raw }}*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-eye', 'EDGE-7814028', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "175px",*/
/*                 height: "175px"*/
/*             }, {"dom":{}}, {"style":{"${symbolSelector}":{"isStage":"true","rect":["undefined","undefined","175px","175px"],"fill":["rgba(255,255,255,1)"]}},"dom":{}});*/
/*         </script>*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-crane', 'EDGE-14088978', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "291px",*/
/*                 height: "148px"*/
/*             }, {"dom":{}}, {"dom":{}});*/
/*         </script>*/
/*         <script>*/
/*             AdobeEdge.loadComposition('assets/js/publish/tokyo-shinkansen', 'EDGE-15798491', {*/
/*                 scaleToFit: "none",*/
/*                 centerStage: "none",*/
/*                 minW: "0px",*/
/*                 maxW: "undefined",*/
/*                 width: "4000px",*/
/*                 height: "56px"*/
/*             }, {"dom":{}}, {"dom":{}});*/
/*         </script>*/
/*     {% endblock %}*/
/*     </body>*/
/*     </html>*/
