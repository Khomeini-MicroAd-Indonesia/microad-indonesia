<?php

/* backend/template.twig */
class __TwigTemplate_6f61b05e63db2b46bf41de79f7949875980d83482daeeff154b357692e76d18c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"bg-black\">
    <head>
        <meta charset=\"UTF-8\">
\t\t<title>";
        // line 5
        echo Config::get("config_cms.cms_name");
        echo " | ";
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        ";
        // line 8
        $this->displayBlock('backend_css', $context, $blocks);
        // line 18
        echo "\t\t
\t\t<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"";
        // line 26
        echo (isset($context["body_tag_class"]) ? $context["body_tag_class"] : null);
        echo "\">
\t\t<div id=\"dialog-confirm\" class=\"modal fade\">
\t\t\t<div class=\"modal-dialog\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<p class=\"confirm-msg\"></p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Yes</button>
\t\t\t\t\t</div>
\t\t\t\t</div><!-- /.modal-content -->
\t\t\t</div><!-- /.modal-dialog -->
\t\t</div><!-- /.modal -->
\t\t
\t\t<!-- header logo: style can be found in header.less -->
        <header class=\"header\">
            <a href=\"";
        // line 43
        echo Uri::base();
        echo "backend\" class=\"logo\">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
\t\t\t\t";
        // line 45
        echo Config::get("config_cms.cms_name");
        echo "
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                <!-- Sidebar toggle button-->
                <a href=\"#\" class=\"navbar-btn sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </a>
                <div class=\"navbar-right\">
                    <ul class=\"nav navbar-nav\">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class=\"dropdown user user-menu\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                <i class=\"glyphicon glyphicon-user\"></i>
                                <i class=\"caret\"></i>
                            </a>
                            <ul class=\"dropdown-menu\">
                                <!-- User image -->
                                <li class=\"user-header bg-light-blue\">
\t\t\t\t\t\t\t\t\t";
        // line 67
        if ((twig_length_filter($this->env, (isset($context["current_admin_photo"]) ? $context["current_admin_photo"] : null)) > 0)) {
            // line 68
            echo "\t\t\t\t\t\t\t\t\t<img src=\"";
            echo Uri::base();
            echo "media/admin/photos/";
            echo (isset($context["current_admin_photo"]) ? $context["current_admin_photo"] : null);
            echo "\" alt=\"Admin Photo\" />
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 70
            echo "\t\t\t\t\t\t\t\t\t<i class=\"fa fa-user fa-5x\"></i>
\t\t\t\t\t\t\t\t\t";
        }
        // line 72
        echo "                                    <p>
\t\t\t\t\t\t\t\t\t\t";
        // line 73
        echo (isset($context["current_admin_fullname"]) ? $context["current_admin_fullname"] : null);
        echo "
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class=\"user-body\">
                                    <div class=\"col-xs-8 text-left\">
                                        <a href=\"";
        // line 79
        echo Uri::base();
        echo "backend/change-password\">Change Password</a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class=\"user-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 85
        echo Uri::base();
        echo "backend/my-profile\" class=\"btn btn-default btn-flat\">Profile</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <a href=\"";
        // line 88
        echo Uri::base();
        echo "backend/sign-out\" class=\"btn btn-default btn-flat\">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class=\"wrapper row-offcanvas row-offcanvas-left\">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class=\"left-side sidebar-offcanvas\">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class=\"sidebar\">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
\t\t\t\t\t<ul class=\"sidebar-menu\">
\t\t\t\t\t";
        // line 104
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cms_menus"]) ? $context["cms_menus"] : null));
        foreach ($context['_seq'] as $context["menu_key"] => $context["menu"]) {
            // line 105
            echo "\t\t\t\t\t\t";
            $context["has_submenu"] = (((twig_length_filter($this->env, $this->getAttribute($context["menu"], "submenus", array())) > 0)) ? (true) : (false));
            // line 106
            echo "\t\t\t\t\t\t";
            if ( !(isset($context["has_submenu"]) ? $context["has_submenu"] : null)) {
                // line 107
                echo "\t\t\t\t\t\t\t";
                $context["menu_active_class"] = ((($context["menu_key"] == (isset($context["menu_current_key"]) ? $context["menu_current_key"] : null))) ? ("active") : (""));
                // line 108
                echo "\t\t\t\t\t\t\t<li class=\"";
                echo (isset($context["menu_active_class"]) ? $context["menu_active_class"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 109
                echo (Uri::base() . $this->getAttribute($context["menu"], "route", array()));
                echo "\">
\t\t\t\t\t\t\t\t\t<i class=\"";
                // line 110
                echo $this->getAttribute($context["menu"], "icon_class", array());
                echo "\"></i>
\t\t\t\t\t\t\t\t\t<span>";
                // line 111
                echo $this->getAttribute($context["menu"], "label", array());
                echo "</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
            } else {
                // line 115
                echo "\t\t\t\t\t\t\t";
                $context["menu_active_class"] = ((($context["menu_key"] == (isset($context["menu_parent_key"]) ? $context["menu_parent_key"] : null))) ? ("active") : (""));
                // line 116
                echo "\t\t\t\t\t\t\t<li class=\"treeview ";
                echo (isset($context["menu_active_class"]) ? $context["menu_active_class"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\">
\t\t\t\t\t\t\t\t\t<i class=\"";
                // line 118
                echo $this->getAttribute($context["menu"], "icon_class", array());
                echo "\"></i>
\t\t\t\t\t\t\t\t\t<span>";
                // line 119
                echo $this->getAttribute($context["menu"], "label", array());
                echo "</span>
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-left pull-right\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<ul class=\"treeview-menu\">
\t\t\t\t\t\t\t\t\t";
                // line 123
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu"], "submenus", array()));
                foreach ($context['_seq'] as $context["submenu_key"] => $context["submenu"]) {
                    // line 124
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context["submenu_active_class"] = ((($context["submenu_key"] == (isset($context["menu_current_key"]) ? $context["menu_current_key"] : null))) ? ("active") : (""));
                    // line 125
                    echo "\t\t\t\t\t\t\t\t\t\t<li class=\"";
                    echo (isset($context["submenu_active_class"]) ? $context["submenu_active_class"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 126
                    echo (Uri::base() . $this->getAttribute($context["submenu"], "route", array()));
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"";
                    // line 127
                    echo $this->getAttribute($context["submenu"], "icon_class", array());
                    echo "\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 128
                    echo $this->getAttribute($context["submenu"], "label", array());
                    echo "
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['submenu_key'], $context['submenu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 132
                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
            }
            // line 135
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['menu_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "\t\t\t\t\t</ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class=\"right-side\">
                ";
        // line 143
        $this->displayBlock('backend_content_header', $context, $blocks);
        // line 144
        echo "\t\t\t\t
                <!-- Main content -->
                <section class=\"content\">
\t\t\t\t\t";
        // line 147
        $this->displayBlock('backend_content', $context, $blocks);
        // line 148
        echo "\t\t\t\t</section>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        ";
        // line 152
        $this->displayBlock('backend_js', $context, $blocks);
        // line 165
        echo "    </body>
</html>";
    }

    // line 8
    public function block_backend_css($context, array $blocks = array())
    {
        // line 9
        echo "\t\t<!-- bootstrap 3.0.2 -->
\t\t";
        // line 10
        echo Asset::css("bootstrap.min.css");
        echo "
        
        <!-- font Awesome -->
\t\t";
        // line 13
        echo Asset::css("font-awesome.min.css");
        echo "
        
        <!-- Theme style -->
\t\t";
        // line 16
        echo Asset::css("AdminLTE.css");
        echo "
\t\t";
    }

    // line 143
    public function block_backend_content_header($context, array $blocks = array())
    {
    }

    // line 147
    public function block_backend_content($context, array $blocks = array())
    {
    }

    // line 152
    public function block_backend_js($context, array $blocks = array())
    {
        // line 153
        echo "\t\t<!-- jQuery 2.0.2 -->
\t\t<!-- <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js\"></script> -->
\t\t
\t\t<!-- jQuery 1.11.1 -->
        ";
        // line 157
        echo Asset::js("jquery-1.11.1.min.js");
        echo "
\t\t
\t\t<!-- Bootstrap -->
\t\t";
        // line 160
        echo Asset::js("bootstrap.min.js");
        echo "
\t\t
        <!-- AdminLTE App -->
        ";
        // line 163
        echo Asset::js("AdminLTE/app.js");
        echo "
\t\t";
    }

    public function getTemplateName()
    {
        return "backend/template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 163,  333 => 160,  327 => 157,  321 => 153,  318 => 152,  313 => 147,  308 => 143,  302 => 16,  296 => 13,  290 => 10,  287 => 9,  284 => 8,  279 => 165,  277 => 152,  271 => 148,  269 => 147,  264 => 144,  262 => 143,  253 => 136,  247 => 135,  242 => 132,  232 => 128,  228 => 127,  224 => 126,  219 => 125,  216 => 124,  212 => 123,  205 => 119,  201 => 118,  195 => 116,  192 => 115,  185 => 111,  181 => 110,  177 => 109,  172 => 108,  169 => 107,  166 => 106,  163 => 105,  159 => 104,  140 => 88,  134 => 85,  125 => 79,  116 => 73,  113 => 72,  109 => 70,  101 => 68,  99 => 67,  74 => 45,  69 => 43,  49 => 26,  39 => 18,  37 => 8,  29 => 5,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="bg-black">*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/* 		<title>{{ config('config_cms.cms_name') }} | {{ meta_title }}</title>*/
/*         <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>*/
/* */
/*         {% block backend_css %}*/
/* 		<!-- bootstrap 3.0.2 -->*/
/* 		{{ asset_css('bootstrap.min.css') | raw }}*/
/*         */
/*         <!-- font Awesome -->*/
/* 		{{ asset_css('font-awesome.min.css') | raw }}*/
/*         */
/*         <!-- Theme style -->*/
/* 		{{ asset_css('AdminLTE.css') | raw }}*/
/* 		{% endblock %}*/
/* 		*/
/* 		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*         <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*         <!--[if lt IE 9]>*/
/*           <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>*/
/*           <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>*/
/*         <![endif]-->*/
/*     </head>*/
/*     <body class="{{ body_tag_class }}">*/
/* 		<div id="dialog-confirm" class="modal fade">*/
/* 			<div class="modal-dialog">*/
/* 				<div class="modal-content">*/
/* 					<div class="modal-body">*/
/* 						<p class="confirm-msg"></p>*/
/* 					</div>*/
/* 					<div class="modal-footer">*/
/* 						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>*/
/* 						<button type="button" class="btn btn-primary">Yes</button>*/
/* 					</div>*/
/* 				</div><!-- /.modal-content -->*/
/* 			</div><!-- /.modal-dialog -->*/
/* 		</div><!-- /.modal -->*/
/* 		*/
/* 		<!-- header logo: style can be found in header.less -->*/
/*         <header class="header">*/
/*             <a href="{{ base_url() }}backend" class="logo">*/
/*                 <!-- Add the class icon to your logo image or logo icon to add the margining -->*/
/* 				{{ config('config_cms.cms_name') }}*/
/*             </a>*/
/*             <!-- Header Navbar: style can be found in header.less -->*/
/*             <nav class="navbar navbar-static-top" role="navigation">*/
/*                 <!-- Sidebar toggle button-->*/
/*                 <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </a>*/
/*                 <div class="navbar-right">*/
/*                     <ul class="nav navbar-nav">*/
/*                         <!-- User Account: style can be found in dropdown.less -->*/
/*                         <li class="dropdown user user-menu">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                                 <i class="glyphicon glyphicon-user"></i>*/
/*                                 <i class="caret"></i>*/
/*                             </a>*/
/*                             <ul class="dropdown-menu">*/
/*                                 <!-- User image -->*/
/*                                 <li class="user-header bg-light-blue">*/
/* 									{% if current_admin_photo | length > 0 %}*/
/* 									<img src="{{ base_url() }}media/admin/photos/{{ current_admin_photo }}" alt="Admin Photo" />*/
/* 									{% else %}*/
/* 									<i class="fa fa-user fa-5x"></i>*/
/* 									{% endif %}*/
/*                                     <p>*/
/* 										{{ current_admin_fullname }}*/
/*                                     </p>*/
/*                                 </li>*/
/*                                 <!-- Menu Body -->*/
/*                                 <li class="user-body">*/
/*                                     <div class="col-xs-8 text-left">*/
/*                                         <a href="{{ base_url() }}backend/change-password">Change Password</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <!-- Menu Footer-->*/
/*                                 <li class="user-footer">*/
/*                                     <div class="pull-left">*/
/*                                         <a href="{{ base_url() }}backend/my-profile" class="btn btn-default btn-flat">Profile</a>*/
/*                                     </div>*/
/*                                     <div class="pull-right">*/
/*                                         <a href="{{ base_url() }}backend/sign-out" class="btn btn-default btn-flat">Sign out</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </div>*/
/*             </nav>*/
/*         </header>*/
/*         <div class="wrapper row-offcanvas row-offcanvas-left">*/
/*             <!-- Left side column. contains the logo and sidebar -->*/
/*             <aside class="left-side sidebar-offcanvas">*/
/*                 <!-- sidebar: style can be found in sidebar.less -->*/
/*                 <section class="sidebar">*/
/*                     <!-- sidebar menu: : style can be found in sidebar.less -->*/
/* 					<ul class="sidebar-menu">*/
/* 					{% for menu_key, menu in cms_menus %}*/
/* 						{% set has_submenu = (menu.submenus | length > 0) ? true : false %}*/
/* 						{% if not has_submenu %}*/
/* 							{% set menu_active_class = (menu_key == menu_current_key) ? 'active' : '' %}*/
/* 							<li class="{{ menu_active_class }}">*/
/* 								<a href="{{ base_url() ~ menu.route }}">*/
/* 									<i class="{{ menu.icon_class }}"></i>*/
/* 									<span>{{ menu.label }}</span>*/
/* 								</a>*/
/* 							</li>*/
/* 						{% else %}*/
/* 							{% set menu_active_class = (menu_key == menu_parent_key) ? 'active' : '' %}*/
/* 							<li class="treeview {{ menu_active_class }}">*/
/* 								<a href="javascript:void(0)">*/
/* 									<i class="{{ menu.icon_class }}"></i>*/
/* 									<span>{{ menu.label }}</span>*/
/* 									<i class="fa fa-angle-left pull-right"></i>*/
/* 								</a>*/
/* 								<ul class="treeview-menu">*/
/* 									{% for submenu_key, submenu in menu.submenus %}*/
/* 										{% set submenu_active_class = (submenu_key == menu_current_key) ? 'active' : '' %}*/
/* 										<li class="{{ submenu_active_class }}">*/
/* 											<a href="{{ base_url() ~ submenu.route }}">*/
/* 												<i class="{{ submenu.icon_class }}"></i>*/
/* 												{{ submenu.label }}*/
/* 											</a>*/
/* 										</li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* 							</li>*/
/* 						{% endif %}*/
/* 					{% endfor %}*/
/* 					</ul>*/
/*                 </section>*/
/*                 <!-- /.sidebar -->*/
/*             </aside>*/
/* */
/*             <!-- Right side column. Contains the navbar and content of the page -->*/
/*             <aside class="right-side">*/
/*                 {% block backend_content_header %}{% endblock %}*/
/* 				*/
/*                 <!-- Main content -->*/
/*                 <section class="content">*/
/* 					{% block backend_content %}{% endblock %}*/
/* 				</section>*/
/*             </aside><!-- /.right-side -->*/
/*         </div><!-- ./wrapper -->*/
/* */
/*         {% block backend_js %}*/
/* 		<!-- jQuery 2.0.2 -->*/
/* 		<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->*/
/* 		*/
/* 		<!-- jQuery 1.11.1 -->*/
/*         {{ asset_js('jquery-1.11.1.min.js') | raw }}*/
/* 		*/
/* 		<!-- Bootstrap -->*/
/* 		{{ asset_js('bootstrap.min.js') | raw }}*/
/* 		*/
/*         <!-- AdminLTE App -->*/
/*         {{ asset_js('AdminLTE/app.js') | raw }}*/
/* 		{% endblock %}*/
/*     </body>*/
/* </html>*/
