<?php

/* sign_in.twig */
class __TwigTemplate_9ba85f6e5fcda1183f49855a2d33c82640ea5d7d0ea769c58b20de14c9c32ab9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("backend/base.twig", "sign_in.twig", 1);
        $this->blocks = array(
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"form-box\" id=\"login-box\">
\t";
        // line 5
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 6
            echo "\t<div class=\"alert alert-danger alert-dismissable\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t\t";
            // line 8
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
\t</div>
\t";
        }
        // line 11
        echo "\t<div class=\"header\">Sign In</div>
\t<form action=\"\" method=\"post\">
\t\t<div class=\"body bg-gray\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"email\" class=\"form-control\" placeholder=\"Email\"/>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\"/>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"footer\">                                                               
\t\t\t<button type=\"submit\" class=\"btn bg-olive btn-block\">Sign me in</button>
\t\t</div>
\t</form>
</div>
";
    }

    public function getTemplateName()
    {
        return "sign_in.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 11,  40 => 8,  36 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "backend/base.twig" %}*/
/* */
/* {% block backend_content %}*/
/* <div class="form-box" id="login-box">*/
/* 	{% if (error_message | length > 0) %}*/
/* 	<div class="alert alert-danger alert-dismissable">*/
/* 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/* 		{{ error_message | raw }}*/
/* 	</div>*/
/* 	{% endif %}*/
/* 	<div class="header">Sign In</div>*/
/* 	<form action="" method="post">*/
/* 		<div class="body bg-gray">*/
/* 			<div class="form-group">*/
/* 				<input type="text" name="email" class="form-control" placeholder="Email"/>*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<input type="password" name="password" class="form-control" placeholder="Password"/>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="footer">                                                               */
/* 			<button type="submit" class="btn bg-olive btn-block">Sign me in</button>*/
/* 		</div>*/
/* 	</form>*/
/* </div>*/
/* {% endblock %}*/
