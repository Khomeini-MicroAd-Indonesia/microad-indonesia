<?php

/* photo.twig */
class __TwigTemplate_5f5582845dbf6fe0b485f864ac54f4a0ea459b2915723c2a0237731eadff1c62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>

    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 22
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 25
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 27
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 29
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 35
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div class=\"detail-category\">
    
        <div class=\"row\">
        ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_data"]) ? $context["photo_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 44
            echo "            <div class=\"dc-title\">
                <div class=\"small-12 columns\">
                    <h1>";
            // line 46
            echo $this->getAttribute($context["photo"], "category", array());
            echo "</h1>
                </div>
            </div>
            <div class=\"dc-desc\">
                <div class=\"small-12 columns\">
                    ";
            // line 51
            echo $this->getAttribute($context["photo"], "desc", array());
            echo "
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"back-detail\" style=\"margin-bottom:10px; text-align: center;\">
                <a href=\"";
            // line 56
            echo Uri::base();
            echo "culture\">Back to Culture</a>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "            <div class=\"dc-image\">
                ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo_detail"]) ? $context["photo_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 61
            echo "                <div class=\"small-12 medium-6 columns no-padding\">
                    <img src=\"";
            // line 62
            echo Uri::base();
            echo "media/photo/";
            echo $this->getAttribute($context["photo"], "image", array());
            echo "\" style=\"width: 100%;\" alt=\"";
            echo $this->getAttribute($context["photo"], "title", array());
            echo "\">
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"back-detail dp-client back-top\" style=\"padding-left: 0;\">
                <a href=\"#top\">Back to Top</a>
            </div>
        </div>
        
</div>

<nav id=\"main-nav\">
    <div id=\"logo-mai\">
        <div class=\"lm-image\">
            <a href=\"";
        // line 77
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/logo-mai.png\"/></a>
        </div>
        <div class=\"lm-title\">
            MicroAd Indonesia 2016
        </div>
    </div>
    <div class=\"nav-menu\">
        <div class=\"nm-culture\">
            <a href=\"";
        // line 85
        echo Uri::base();
        echo "culture\">Culture</a>
        </div>
        <div class=\"nm-item nm-contact\">
            <a id=\"contactButton\" href=\"#contact\">Contact</a>
        </div>
        <div class=\"nm-item separator\">
            <a href=\"#career\">Career</a>
        </div>
        <div class=\"nm-item\">
            <a href=\"https://www.microad.co.jp/en/\" target=\"_blank\">MicroAd Japan</a>
        </div>
    </div>
</nav>

";
        // line 99
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 106
        echo "
<script>
    \$('.back-top a').click(function () {
        \$('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
</script>

</body>
</html>

";
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 18
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 19
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 20
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 22
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 23
        echo "        ";
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 25
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 26
        echo "    ";
    }

    // line 27
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 28
        echo "    ";
    }

    // line 99
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 100
        echo "    ";
        echo Asset::js("vendor/jquery.js");
        echo "
    ";
        // line 101
        echo Asset::js("vendor/what-input.js");
        echo "
    ";
        // line 102
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 103
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 104
        echo Asset::js("app.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "photo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 104,  264 => 103,  260 => 102,  256 => 101,  251 => 100,  248 => 99,  244 => 28,  241 => 27,  237 => 26,  234 => 25,  227 => 23,  224 => 22,  218 => 20,  214 => 19,  210 => 18,  206 => 17,  201 => 16,  198 => 15,  181 => 106,  179 => 99,  162 => 85,  149 => 77,  135 => 65,  122 => 62,  119 => 61,  115 => 60,  112 => 59,  103 => 56,  95 => 51,  87 => 46,  83 => 44,  79 => 43,  68 => 35,  60 => 29,  57 => 27,  54 => 25,  51 => 22,  49 => 15,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="ie8">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>{{ meta_title }}</title>*/
/*     <meta name="description" content="{{ meta_desc }}">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <link rel="shortcut icon" href="{{ base_url() }}assets/css/images/logo_aio.png" />*/
/*     <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>*/
/*     <link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>*/
/* */
/*     {% block frontend_css %}*/
/*         {{ asset_css('custom/normalize.css') | raw }}*/
/*         {{ asset_css('custom/foundation.css') | raw }}*/
/*         {{ asset_css('custom/motion-ui.css') | raw }}*/
/*         {{ asset_css('custom/foundation-icons/foundation-icons.css') | raw }}*/
/*         {{ asset_css('custom/style.css') | raw }}*/
/*     {% endblock %}*/
/*     {% block frontend_head_js %}*/
/*         {{ asset_js('modernizr.js') }}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_twitter %}*/
/*     {% endblock %}*/
/*     {% block frontend_meta_facebook %}*/
/*     {% endblock %}*/
/*     <script>*/
/*         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/*         ga('create', '{{ config('config_basic.google_analytic_code') }}', 'auto');*/
/*         ga('send', 'pageview');*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <div class="detail-category">*/
/*     */
/*         <div class="row">*/
/*         {% for photo in photo_data %}*/
/*             <div class="dc-title">*/
/*                 <div class="small-12 columns">*/
/*                     <h1>{{ photo.category }}</h1>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="dc-desc">*/
/*                 <div class="small-12 columns">*/
/*                     {{ photo.desc }}*/
/*                 </div>*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <div class="back-detail" style="margin-bottom:10px; text-align: center;">*/
/*                 <a href="{{ base_url() }}culture">Back to Culture</a>*/
/*             </div>*/
/*         {% endfor %}*/
/*             <div class="dc-image">*/
/*                 {% for photo in photo_detail %}*/
/*                 <div class="small-12 medium-6 columns no-padding">*/
/*                     <img src="{{ base_url() }}media/photo/{{ photo.image }}" style="width: 100%;" alt="{{ photo.title }}">*/
/*                 </div>*/
/*                 {% endfor %}*/
/*                 <div style="clear: both;"></div>*/
/*             </div>*/
/*             <div class="back-detail dp-client back-top" style="padding-left: 0;">*/
/*                 <a href="#top">Back to Top</a>*/
/*             </div>*/
/*         </div>*/
/*         */
/* </div>*/
/* */
/* <nav id="main-nav">*/
/*     <div id="logo-mai">*/
/*         <div class="lm-image">*/
/*             <a href="{{ base_url() }}"><img src="{{ base_url() }}assets/css/images/logo-mai.png"/></a>*/
/*         </div>*/
/*         <div class="lm-title">*/
/*             MicroAd Indonesia 2016*/
/*         </div>*/
/*     </div>*/
/*     <div class="nav-menu">*/
/*         <div class="nm-culture">*/
/*             <a href="{{ base_url() }}culture">Culture</a>*/
/*         </div>*/
/*         <div class="nm-item nm-contact">*/
/*             <a id="contactButton" href="#contact">Contact</a>*/
/*         </div>*/
/*         <div class="nm-item separator">*/
/*             <a href="#career">Career</a>*/
/*         </div>*/
/*         <div class="nm-item">*/
/*             <a href="https://www.microad.co.jp/en/" target="_blank">MicroAd Japan</a>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
/* */
/* {% block frontend_js %}*/
/*     {{ asset_js('vendor/jquery.js') | raw }}*/
/*     {{ asset_js('vendor/what-input.js') | raw }}*/
/*     {{ asset_js('motion-ui.js') | raw }}*/
/*     {{ asset_js('foundation.js') | raw }}*/
/*     {{ asset_js('app.js') | raw }}*/
/* {% endblock %}*/
/* */
/* <script>*/
/*     $('.back-top a').click(function () {*/
/*         $('body,html').animate({*/
/*             scrollTop: 0*/
/*         }, 800);*/
/*         return false;*/
/*     });*/
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
