<?php
namespace Pages;

class Controller_Backend extends \Controller_Backend
{
	private $_module_url = 'backend/pages';
	private $_menu_key = 'pages';
	
	public function before() {
		parent::before();
		$this->authenticate();
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'read')) {
			// if not have an access then redirect to error page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		$this->_data_template['meta_title'] = 'Pages';
		$this->_data_template['menu_current_key'] = 'pages';
	}
	
	public function action_index() {
		$this->_data_template['pages_list'] = Model_Pages::find('all');
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		$this->_data_template['error_message'] = \Session::get_flash('error_message');
		return \Response::forge(\View::forge('pages::backend_list.twig', $this->_data_template, FALSE));
	}
	
	public function action_form($page_id) {
		// find page model by page id
		$page_model = Model_Pages::find($page_id);
		// if empty then define empty page model
		if (empty($page_model)) {
			// Page ID "0" is only for add new page, if greater than 0 then its mean edit page
			if ($page_id > 0) {
				\Session::set_flash('error_message', 'The Page with ID "'.$page_id.'" is not found here');
				\Response::redirect(\Uri::base().$this->_module_url);
			}
			$page_model = Model_Pages::forge();
		}
		$this->_save_setting_data($page_model);
		$this->_data_template['content_header'] = 'Pages';
		$this->_data_template['content_subheader'] = 'Form';
		$this->_data_template['breadcrumbs'] = array(
			array(
				'label' => 'Pages',
				'link' => \Uri::base().$this->_module_url
			),
			array(
				'label' => 'Form'
			)
		);
		$this->_data_template['form_data'] = $page_model->get_form_data_basic();
		$this->_data_template['cancel_button_link'] = \Uri::base().$this->_module_url;
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		return \Response::forge(\View::forge('backend/form/basic.twig', $this->_data_template, FALSE));
	}
	
	private function _save_setting_data($page_model) {
		$all_post_input = \Input::post();
		if (count($all_post_input)) {
			// Check menu permission
			$access_name = ($page_model->id > 0) ? 'update' : 'create';
			if (!$this->check_menu_permission($this->_menu_key, $access_name)) {
				// if not have an access then redirect to error page
				\Response::redirect(\Uri::base().'backend/no-permission');
			}
			$page_model->title = $all_post_input['page_title'];
			$page_model->content = $all_post_input['page_content'];
			$page_model->meta_title = $all_post_input['page_meta_title'];
			$page_model->meta_desc = $all_post_input['page_meta_desc'];
			$page_model->url_path = $all_post_input['page_url_path'];
			$page_model->status = $all_post_input['status'];
			// Set created_by/updated_by
			if ($page_model->id > 0) {
				$page_model->updated_by = $this->admin_auth->getCurrentAdmin()->id;
			} else {
				$page_model->created_by = $this->admin_auth->getCurrentAdmin()->id;
			}
			// Save with validation, if error then throw the error
			try {
				$page_model->save();
				\Session::set_flash('success_message', 'Successfully Saved');
				\Response::redirect(\Uri::current());
			} catch (\Orm\ValidationFailed $e) {
				$this->_data_template['error_message'] = $e->getMessage();
			}
		}
	}
	
	public function action_delete($page_id) {
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'delete')) {
			// if not have an access then redirect to error page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		// find page model by page id
		$page_model = Model_Pages::find($page_id);
		// if empty then redirect back with error message
		if (empty($page_model)) {
			\Session::set_flash('error_message', 'The Page with ID "'.$page_id.'" is not found here');
			\Response::redirect(\Uri::base().$this->_module_url);
			$page_model = Model_Pages::forge();
		}
		// Delete the admin
		try {
			$page_model->delete();
			\Session::set_flash('success_message', 'Delete Page with title "'.$page_model->title.'" is successfully');
		} catch (Orm\ValidationFailed $e) {
			\Session::set_flash('error_message', $e->getMessage());
		}
		\Response::redirect(\Uri::base().$this->_module_url);
	}
}

