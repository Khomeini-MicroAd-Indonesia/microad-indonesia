<?php

namespace Pages;

class Controller_Frontend_Home extends \Controller_Frontend {

    private $_module_url = '';
    private $_menu_key = 'home';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {

        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_home.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template, FALSE));
        }

    }

    public function action_mhome() {

        return \Response::forge(\View::forge('pages::frontend/m_home.twig', $this->_data_template, FALSE));
    }

    public function action_business() {
        
        $this->_contact_submission();
        $this->_data_template['preface_content']    = \Preface\Prefaceutil::get_preface_content();
        $this->_data_template['portfolio_data']     = \Portfolio\Portfolioutil::get_portfolio_data();
        $this->_data_template['client_logo']        = \Client\Clientutil::get_client_logo();
        $this->_data_template['capability_data']    = \Capability\Capabilityutil::get_capability_data();
        $this->_data_template['ourpeople_content']  = \Ourpeople\Ourpeopleutil::get_ourpeople_content();
        $this->_data_template['businessunit_data']  = \Businessunit\Businessunitutil::get_businessunit_data();

        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_business.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/business.twig', $this->_data_template, FALSE));
        }

    }


    public function action_mbusiness() {

        $this->_contact_submission();
        $this->_data_template['preface_content']    = \Preface\Prefaceutil::get_preface_content();
        $this->_data_template['portfolio_data']     = \Portfolio\Portfolioutil::get_portfolio_data();
        $this->_data_template['client_logo']        = \Client\Clientutil::get_client_logo();
        $this->_data_template['capability_data']    = \Capability\Capabilityutil::get_capability_data();
        $this->_data_template['ourpeople_content']  = \Ourpeople\Ourpeopleutil::get_ourpeople_content();
        $this->_data_template['businessunit_data']  = \Businessunit\Businessunitutil::get_businessunit_data();

            return \Response::forge(\View::forge('pages::frontend/m_business.twig', $this->_data_template, FALSE));
    }

    public function action_culture() {
        
        $this->_world_time();
//        $this->_world_weather_jkt();
        $this->_world_weather_sin();
        $this->_world_weather_tyo();
//        $this->_world_weather_bkk();
//        $this->_world_weather_hcm();
        $this->_data_template['about_maizen']   = \Aboutmaizen\Aboutmaizenutil::get_about_maizen();
        $this->_data_template['news_data']      = \News\Newsutil::get_news_data();
        $this->_data_template['photo_data1']    = \Photo\Photoutil::get_photo_category1();
        $this->_data_template['photo_data2']    = \Photo\Photoutil::get_photo_category2();
        $this->_data_template['photo_data3']    = \Photo\Photoutil::get_photo_category3();
        $this->_data_template['photo_data']    = \Photo\Photoutil::get_photo_category();
        $this->_data_template['video_data']     = \Video\Videoutil::get_video_data();

        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_culture.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/culture.twig', $this->_data_template, FALSE));
        }

        
    }

    public function action_mculture() {

        return \Response::forge(\View::forge('pages::frontend/m_culture.twig', $this->_data_template, FALSE));
    }

    
    private function _send_email($post_data) {
        $data = array(
            'email_from' => 'website@microad.co.id',
            'email_to' => \Config::get('config_basic.contact_us_email_to'),
            'email_subject' => '[Website microad.co.id] Contact Us message from ' . $post_data['name'],
            'mail_subject' => 'Enquiry Confirmation from PT. MicroAd Indonesia',
            'email_reply_to' => array(
                'email' => $post_data['email'],
                'name' => $post_data['name']
            ), // Optional
            'email_data' => array(
                'base_url' => \Uri::base(),
                'name' => $post_data['name'],
                'email' => $post_data['email'],
                'message' => $post_data['message']
            ),
            'email_view' => 'pages::email/contact_us.twig',
            'email_respond_view' => 'pages::email/email_responder.twig',
        );
            
        \Util_Email::queue_send_email($data);
        $email = \Email::forge();

        // Set the from address
        $email->from($data['email_from'], 'MicroAd Indonesia');
        // Set the to address
        $email->to($data['email_to'],'Web Admin of MicroAd Indonesia');
        // Set a subject
        $email->subject($data['email_subject']);
        // And set the body.
        $email->html_body(\View::forge($data['email_view'], $data['email_data']));
        
        $email->send();
        
        $mail = \Email::forge();
        $mail->from($data['email_from'], 'PT. MicroAd Indonesia');
        $mail->to($data['email_data']['email'],$data['email_data']['name']);
        $mail->subject($data['mail_subject']);
        $mail->html_body(\View::forge($data['email_respond_view'], $data['email_data']));

        $mail->send();
    }
    
    private function _contact_submission() {
        $_post_data = \Input::post();
        $_confirmation_msg = '';
        if (count($_post_data) > 0) {
            $_err_msg = [];
            
            $val = \Validation::forge('contact_validation');
            $val->add('name', 'Your Name')->add_rule('required');
            $val->add('email', 'Your Email')->add_rule('required|valid_email');
            $val->set_message('required', 'Please fill in :label');
            $val->set_message('valid_email', 'Please fill in :label with valid email');
            if (!$val->run()) {
                foreach ($val->error() as $field => $error) {
                    $_err_msg[$field] = $error->get_message();
                }
            } else {
                $_confirmation_msg = "Thank you for contacting us, we'll get back to you as soon as possible";
                $this->_send_email($_post_data);
                \Session::set_flash('contact_success_message', 'Thank you for your comment.');
                \Response::redirect(\Uri::current());
            }
            $this->_data_template['err_msg'] = $_err_msg;
            $this->_data_template['post_data'] = $_post_data;
        }
    }
    
    public function _world_time() {
        
        $date = date("M' d-H:i:s");
        //var_dump($date); exit;
        $ind = explode('-', $date);
        //var_dump($jkt[1]); exit;
        date_default_timezone_set("Asia/Singapore");
        $sin = date("H:i:s");
        
        date_default_timezone_set("Asia/Bangkok");
        $tha = date("H:i:s");
        
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $vie = date("H:i:s");
        
        date_default_timezone_set("Asia/Manila");
        $phi = date("H:i:s");
        
        date_default_timezone_set("Asia/Tokyo");
        $jpn = date("H:i:s");
        
        $this->_data_template['indonesia'] = $ind;
        $this->_data_template['singapore'] = $sin;
        $this->_data_template['thailand'] = $tha;
        $this->_data_template['vietnam'] = $vie;
        $this->_data_template['philippines'] = $phi;
        
        return \Response::forge(\View::forge('pages::frontend/culture.twig', $this->_data_template, FALSE));
    }
    
    public function _world_weather_jkt() {
        
//        $city="Jakarta";
//        $api_key = 'a66e245389de1463e0835e7d2b0ffc7f';
//        $country="ID"; //Two digit country code
//        $url="http://api.openweathermap.org/data/2.5/weather?q=".$city.",".$country."&units=metric&cnt=7&lang=en&appid=".$api_key;
//        $json=file_get_contents($url);
//        $data=json_decode($json,true);
        
        //Get current Temperature in Celsius
//        $temperature =  $data['main']['temp'];
        
        //Get weather condition
        //$condition = $data['weather'][0]['main'];
        
        //Get cloud percentage
//        $cloud = $data['clouds']['all'];
        
        //Get wind speed
        //echo $data['wind']['speed']."<br>";
        
        //Get weather ID
//        $weater_id = $data['weather'][0]['id'];
//        $id = substr($weater_id, 0, 1);
//        $cond_jkt = '';
        //var_dump($id); exit;
//        switch ($id){
//            case '8':
//
//                $cond_jkt = 'sun';
//                break;
//
//            case '2':
//                $cond_jkt = 'lightning';
//                break;
//
//            case '5':
//                $cond_jkt = 'rain';
//                break;
//
//            case '6':
//                $cond_jkt = 'snow';
//                break;
//
//            default:
//        }
        
//        $this->_data_template['temp_jkt'] = $temperature;
//        $this->_data_template['cond_jkt'] = $cond_jkt;
//        $this->_data_template['cloud_jkt'] = $cloud;
        
        return \Response::forge(\View::forge('pages::frontend/culture.twig', $this->_data_template, FALSE));
        
    }
    
    public function _world_weather_sin() {
        
        $city="Singapore";
        $api_key = 'a66e245389de1463e0835e7d2b0ffc7f';
        $country="SG"; //Two digit country code
        $url="http://api.openweathermap.org/data/2.5/weather?q=".$city.",".$country."&units=metric&cnt=7&lang=en&appid=".$api_key;
        $json=file_get_contents($url);
        $data=json_decode($json,true);
        
        //Get current Temperature in Celsius
        $temperature =  $data['main']['temp'];
        
        //Get weather condition
        //$condition = $data['weather'][0]['main'];
        
        //Get cloud percentage
        $cloud = $data['clouds']['all'];
        
        //Get wind speed
        //echo $data['wind']['speed']."<br>";
        
        //Get weather ID
        $weater_id = $data['weather'][0]['id'];
        $id = substr($weater_id, 0, 1);
        $cond_sin = '';
        
        switch ($id){
            case '8':
                
                $cond_sin = 'sun';
                break;
            
            case '2':
                $cond_sin = 'lightning';
                break;
            
            case '5':
                $cond_sin = 'rain';
                break;
            
            case '6':
                $cond_sin = 'snow';
                break;
            
            default:
        }
        
        $this->_data_template['temp_sin'] = $temperature;
        $this->_data_template['cond_sin'] = $cond_sin;
        $this->_data_template['cloud_sin'] = $cloud;
        
        return \Response::forge(\View::forge('pages::frontend/culture.twig', $this->_data_template, FALSE));
        
    }
    
    public function _world_weather_tyo() {
        
        $city="Tokyo";
        $api_key = 'a66e245389de1463e0835e7d2b0ffc7f';
        $country="JP"; //Two digit country code
        $url="http://api.openweathermap.org/data/2.5/weather?q=".$city.",".$country."&units=metric&cnt=7&lang=en&appid=".$api_key;
        $json=file_get_contents($url);
        $data=json_decode($json,true);
        
        //Get current Temperature in Celsius
        $temperature =  $data['main']['temp'];
        
        //Get weather condition
        //$condition = $data['weather'][0]['main'];
        
        //Get cloud percentage
        $cloud = $data['clouds']['all'];
        
        //Get wind speed
        //echo $data['wind']['speed']."<br>";
        
        //Get weather ID
        $weater_id = $data['weather'][0]['id'];
        $id = substr($weater_id, 0, 1);
        $cond_tyo = '';
        
        switch ($id){
            case '8':
                
                $cond_tyo = 'sun';
                break;
            
            case '2':
                $cond_tyo = 'lightning';
                break;
            
            case '5':
                $cond_tyo = 'rain';
                break;
            
            case '6':
                $cond_tyo = 'snow';
                break;
            
            default:
        }
        
        $this->_data_template['temp_tyo'] = $temperature;
        $this->_data_template['cond_tyo'] = $cond_tyo;
        $this->_data_template['cloud_tyo'] = $cloud;
        
        return \Response::forge(\View::forge('pages::frontend/culture.twig', $this->_data_template, FALSE));
        
    }

    public function action_project() {
        
        $slug = $this->param('slug');
        $this->_data_template['portfolio_detail'] = \Portfolio\Portfolioutil::get_portfolio_detail($slug);

        return \Response::forge(\View::forge('pages::frontend/project.twig', $this->_data_template, FALSE));
    }

    public function action_photo() {
        
        $slug = $this->param('slug');
        $this->_data_template['photo_data'] = \Photo\Photoutil::get_photo_data($slug);
        $this->_data_template['photo_detail'] = \Photo\Photoutil::get_photo_detail($slug);
        
        return \Response::forge(\View::forge('pages::frontend/photo.twig', $this->_data_template, FALSE));
    }
    
    public function action_news() {
        
        $slug = $this->param('slug');
        $this->_data_template['news_detail'] = \News\Newsutil::get_news_detail($slug);
        $this->_data_template['news_image'] = \News\Newsutil::get_news_image($slug);
        return \Response::forge(\View::forge('pages::frontend/news.twig', $this->_data_template, FALSE));
    }
    
}
