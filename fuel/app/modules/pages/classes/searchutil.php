<?php

namespace Pages;


class Searchutil {

    /*
     * get brand
     */

    public static function get_brand($keyword) {

        $details = \Product\Model_ProductVariants::query()
            ->related('images')
            ->where('title', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title'     => $detail->title,
                'desc'      => $detail->desc,
                'image'     => $detail->images->filename,
                'status'    => 'brand'
            );
        }
        return $data;
    }
    
    /*
     * get career
     */

    public static function get_career($keyword) {
        
        $careers = \Career\Model_Careers::query()
            ->where('name', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($careers as $career) {

            $data[] = array(
                'name'      => $career->name,
                'level'     => $career->level,
                'location'  => $career->location,
                'req'       => $career->req,
                'date'      => date('l, d F Y', strtotime($career->date)),
                'status'    => 'career'
            );
        }
        return $data;
    }
    
    /*
     * get press room
     */

    public static function get_press($keyword) {

        $presses = \Press\Model_Press::query()
            ->related('images')
            ->where('title', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($presses as $press) {

            $data[] = array(
                'title'         => $press->title,
                'highlight'     => $press->highlight,
                'content'       => $press->content,
                'image'         => $press->images->filename,
                'status'        => 'press'
            );
        }
        return $data;
    }
    
    
    /*
     * get csr activity
     */

    public static function get_activity($keyword) {

        $activities = \Csrmanagement\Model_Activities::query()
            ->related('images')
            ->where('title', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($activities as $activity) {

            $data[] = array(
                'title'         => $activity->title,
                'date'          => date('l, d F Y', strtotime($activity->post_date)),
                'highlight'     => $activity->highlight,
                'content'       => $activity->content,
                'image'         => $activity->images->filename,
                'status'        => 'activity'
            );
        }
        return $data;
    }
    
}
