<?php
namespace Video;

class Videoutil{

    /**
     * Get Video Data
     */
    
    public static function get_video_data(){
        $videos = \Video\Model_Videos::query()
            ->related('images')
            ->related('images_prev')
            ->related('images_next')
            ->where('status', 1)
            ->order_by('seq', 'DESC')
            ->get();
                
        $data = array();
        
        foreach ($videos as $video){
            
            $word_total = explode(" ",strip_tags($video->desc));
            $highlight = implode(" ",array_splice($word_total,0,15));
            
            $data[] = array(
                'id'                => $video->id,   
                'url'               => $video->url,
                'image'             => $video->images->filename,
                'image_prev'        => $video->images_prev->filename,
                'image_next'        => $video->images_next->filename,
                'title'             => $video->title,
                'highlight'         => $highlight,
                'desc'              => $video->desc,
                'seq'               => $video->seq,
                'created_at'        => $video->created_at
            );
        }
        return $data;
    }
    
    /**
     * Get Video Navigation Images
     */
    
    public static function get_video_thumb_prev($count){
        
        //$curr = 1;
        
        for($i = 1; $i <= $count; $i ++){
            
            if($i == 1){
                
                $prevs = \Video\Model_VideoImages::query()
                    //->related('images')
                    ->where('status', 1)
                    ->where('seq', $count)
                    ->get();

                $data = array();
                //var_dump($prevs); exit;
                foreach ($prevs as $prev){

                    $data[] = array(
                        'image' => $prev->filename
                    );
                }
            
            }else{
                
                $prevs = \Video\Model_VideoImages::query()
                    //->related('images')
                    ->where('status', 1)
                    ->where('seq', $i - 1)
                    ->get();

                $data = array();

                foreach ($prevs as $prev){

                    $data[] = array(
                        'image' => $prev->filename
                    );
                }
                
            }
            return $data;
        }
        
    }
    
    public static function get_video_thumb_next($count){
        
        for($i = 1; $i <= $count; $i ++){
            
            if($i != $count){
                
                $nexts = \Video\Model_VideoImages::query()
                    //->related('images')
                    ->where('status', 1)
                    ->where('seq', $i + 1)
                    ->get();

                $data = array();

                foreach ($nexts as $next){

                    $data[] = array(
                        'image' => $next->filename
                    );
                }
            
            }else{
                
                $nexts = \Video\Model_VideoImages::query()
                //->related('images')
                ->where('status', 1)
                ->where('seq', 1)
                ->get();

                $data = array();

                foreach ($nexts as $next){

                    $data[] = array(
                        'image' => $next->filename
                    );
                }
                
            }
        return $data;    
        }
        
    }
    
}