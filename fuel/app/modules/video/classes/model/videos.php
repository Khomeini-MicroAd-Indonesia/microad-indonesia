<?php
namespace Video;

class Model_Videos extends \Orm\Model {
    private $status_name = array('InActive', 'Active');

    protected static $_table_name = 'videos';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'videos'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'videos'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'videos'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'image_prev' => array(
            'label' => 'Image Prev',
            'validation' => array(
                'required',
            )
        ),
        'image_next' => array(
            'label' => 'Image Next',
            'validation' => array(
                'required',
            )
        ),
        'url' => array(
            'label' => 'URL',
            'validation' => array(
                'required',
                'max_length' => array(200),
            )
        ),
        'title' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'desc' => array(
            'label' => 'Description',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Seq',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\Video\Model_VideoImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_prev' => array(
            'key_from' => 'image_prev',
            'model_to' => '\Video\Model_VideoImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_next' => array(
            'key_from' => 'image_next',
            'model_to' => '\Video\Model_VideoImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
        
    private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
    public function get_image_name(){
        if(empty(self::$_images)){
            self::$_images = Model_VideoImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }
    
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }
	
    public function get_form_data_basic($image) {
        return array(
            'attributes' => array(
                'name' => 'frm_video',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Prev',
                        'id' => 'image_prev',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_prev',
                        'value' => $this->image_prev,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image Prev',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Next',
                        'id' => 'image_next',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_next',
                        'value' => $this->image_next,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image Next',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'URL',
                        'id' => 'url',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'url',
                        'value' => $this->url,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'URL',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description',
                        'id' => 'desc',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'desc',
                        'value' => $this->desc,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
    
}
