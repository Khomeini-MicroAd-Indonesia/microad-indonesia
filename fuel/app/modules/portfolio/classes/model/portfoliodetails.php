<?php
namespace Portfolio;

class Model_PortfolioDetails extends \Orm\Model {
    
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/portfolio/';

    protected static $_table_name = 'portfolio_detail_images';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'portfolio_detail_images'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'portfolio_detail_images'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'portfolio_detail_images'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'portfolio_id' => array(
            'label' => 'Portfolio',
            'validation' => array(
                'required',
            )
        ),
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'title' => array(
            'label' => 'Detail Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'slug' => array(
            'label' => 'Detail Slug',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'desc' => array(
            'label' => 'Detail Desc',
            'validation' => array()
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
        
    protected static $_belongs_to = array(
        'portfolios' => array(
            'key_from' => 'portfolio_id',
            'model_to' => '\portfolio\Model_Portfolios',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\portfolio\Model_PortfolioImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )

    );
	
    private static $_portfolios;
    private static $_images;
        
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_portfolio_name(){
        if(empty(self::$_portfolios)){
            self::$_portfolios = Model_Portfolios::get_as_array();
        }
        $flag = $this->portfolio_id;
        return isset(self::$_portfolios[$flag]) ? self::$_portfolios[$flag] : '-';
    }

    public function get_image_name(){
        if(empty(self::$_images)){
            self::$_images = Model_PortfolioImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->title;
            }
        }
        return $data;
    }

    public function get_form_data_basic($portfolio, $image) {
        return array(
            'attributes' => array(
                'name' => 'frm_portfolio_detail',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Portfolio Name',
                        'id' => 'portfolio_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'portfolio_id',
                        'value' => $this->portfolio_id,
                        'options' => $portfolio,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Portfolio',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Name',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Image Name',
                            'data-live-search' => 'true',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Detail Desc',
                        'id' => 'desc',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'desc',
                        'value' => $this->desc,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
}
