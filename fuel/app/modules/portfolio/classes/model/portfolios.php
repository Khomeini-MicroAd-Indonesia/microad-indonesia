<?php
namespace Portfolio;

class Model_Portfolios extends \Orm\Model {
    private $status_name = array('InActive', 'Active');

    protected static $_table_name = 'portfolios';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'portfolios'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'portfolios'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'portfolios'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'slug' => array(
            'label' => 'Slug',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'url' => array(
            'label' => 'URL',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'image_m_id' => array(
            'label' => 'Image(M)',
            'validation' => array(
                'required',
            )
        ),
        'image_s_id' => array(
            'label' => 'Image(S)',
            'validation' => array(
                'required',
            )
        ),
        'client' => array(
            'label' => 'Client',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'start_date' => array(
            'label' => 'Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'end_date' => array(
            'label' => 'Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'desc' => array(
            'label' => 'Description',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\Portfolio\Model_PortfolioImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_m' => array(
            'key_from' => 'image_m_id',
            'model_to' => '\Portfolio\Model_PortfolioImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_s' => array(
            'key_from' => 'image_s_id',
            'model_to' => '\Portfolio\Model_PortfolioImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
        
    private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
    public function get_image_name(){
        if(empty(self::$_images)){
            self::$_images = Model_PortfolioImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }
    
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->client;
            }
        }
        return $data;
    }
	
    public function get_form_data_basic($image) {
        return array(
            'attributes' => array(
                'name' => 'frm_portfolio',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'URL',
                        'id' => 'url',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'url',
                        'value' => $this->url,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'URL',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image(L)',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image(M)',
                        'id' => 'image_m_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_m_id',
                        'value' => $this->image_m_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image(S)',
                        'id' => 'image_s_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_s_id',
                        'value' => $this->image_s_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Client',
                        'id' => 'client',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'client',
                        'value' => $this->client,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Client',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Start Date',
                        'id' => 'start_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'start_date',
                        'value' => $this->start_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'End Date',
                        'id' => 'end_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'end_date',
                        'value' => $this->end_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description',
                        'id' => 'desc',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'desc',
                        'value' => $this->desc,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
    
}
