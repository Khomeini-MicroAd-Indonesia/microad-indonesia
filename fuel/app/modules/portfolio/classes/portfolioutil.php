<?php
namespace Portfolio;

class Portfolioutil{

    /**
     * get Portfolio data
     */
    
    public static function get_portfolio_data(){
        $portfolios = \Portfolio\Model_Portfolios::query()
            ->related('images')
            ->related('images_m')
            ->related('images_s')
            ->where('status', 1)
            ->get();
                
        $data = array();
        
        foreach ($portfolios as $portfolio){
            
            $data[] = array(
                'slug'          => $portfolio->slug,
                'url'           => $portfolio->slug,
                'image'         => $portfolio->images->filename,
                'image_m'       => $portfolio->images_m->filename,
                'image_s'       => $portfolio->images_s->filename,
                'client'        => $portfolio->client,
                'start_date'    => $portfolio->start_date,
                'end_date'      => $portfolio->end_date,
                'desc'          => $portfolio->desc
            );
        }
        return $data;
    }
    
    /**
     * get Portfolio detail
     */
    
    public static function get_portfolio_detail($slug){
        $portfolios = \Portfolio\Model_PortfolioDetails::query()
            ->related('portfolios')    
            ->related('images')
//            ->related('images_m')
//            ->related('images_s')
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
                
        $data = array();
        
        foreach ($portfolios as $portfolio){
            
            $name = explode(".", $portfolio->images->name); 
            //$n = $portfolio->images->filename;
            //var_dump($name[0]); exit;
            $data[] = array(
                'title'         => $portfolio->title,
                'slug'          => $portfolio->slug,
                'url'           => $portfolio->portfolios->url,
                'image'         => $portfolio->images->filename,//$name[0],
//                'image_m'       => $portfolio->images_m->filename,
//                'image_s'       => $portfolio->images_s->filename,
                'client'        => $portfolio->portfolios->client,
                'start_date'    => date('d F Y', strtotime($portfolio->portfolios->start_date)),
                'end_date'      => date('d F Y', strtotime($portfolio->portfolios->end_date)),
                'desc'          => $portfolio->desc
            );
        }
        return $data;
    }
    
}