<?php
namespace Client;

class Clientutil{

    /**
     * get Client logo
     */
    
    public static function get_client_logo(){
        //$logos = \Client\Model_ClientImages::query()
        $logos = \Client\Model_Clients::query()
            ->related('images')
            ->where('status', 1)
            ->get();
                
        $data = array();
        
        foreach ($logos as $logo){
            
            $data[] = array(
                'image' => $logo->images->filename,
                'url'   => $logo->link
            );
        }
        return $data;
    }
    
}

