<?php
namespace Photo;

class Photoutil{

    /**
     * get Photo data for mobile
     */
    
    public static function get_photo_category(){
        $photos = \Photo\Model_Photos::query()
            ->related('images')
            ->related('images_m')
            ->related('images_s')
            ->where('status', 1)
//            ->where('slug', 'mai-share') 
//            ->or_where('slug', 'mai-sport') 
//            ->or_where('slug', 'mai-study') 
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'slug'          => $photo->slug,
                'image'         => $photo->images->filename,
                'image_m'       => $photo->images_m->filename,
                'image_s'       => $photo->images_s->filename,
                'category'      => $photo->category,
                'desc'          => $photo->desc
            );
        }
        return $data;
    }
    
    
    /**
     * get Photo data
     */
    
    public static function get_photo_category1(){
        $photos = \Photo\Model_Photos::query()
            ->related('images')
            ->related('images_m')
            ->related('images_s')
            ->where('status', 1)
            ->where('slug', 'mai-share') 
            ->where('slug', 'mai-sport') 
            ->where('slug', 'mai-study')
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'slug'          => $photo->slug,
                'image'         => $photo->images->filename,
                'image_m'       => $photo->images_m->filename,
                'image_s'       => $photo->images_s->filename,
                'category'      => $photo->category,
                'desc'          => $photo->desc
            );
        }
        return $data;
    }
    
    public static function get_photo_category2(){
        $photos = \Photo\Model_Photos::query()
            ->related('images')
            ->related('images_m')
            ->related('images_s')
            ->where('status', 1)
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'slug'          => $photo->slug,
                'image'         => $photo->images->filename,
                'image_m'       => $photo->images_m->filename,
                'image_s'       => $photo->images_s->filename,
                'category'      => $photo->category,
                'desc'          => $photo->desc
            );
        }
        return $data;
    }
    
    public static function get_photo_category3(){
        $photos = \Photo\Model_Photos::query()
            ->related('images')
            ->related('images_m')
            ->related('images_s')
            ->where('status', 1)
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'slug'          => $photo->slug,
                'image'         => $photo->images->filename,
                'image_m'       => $photo->images_m->filename,
                'image_s'       => $photo->images_s->filename,
                'category'      => $photo->category,
                'desc'          => $photo->desc
            );
        }
        return $data;
    }
    
    /**
     * get Photo data
     */
    
    public static function get_photo_data($slug){
        
        $photos = \Photo\Model_Photos::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'category'      => $photo->category,
                'desc'          => $photo->desc
            );
        }
        return $data;
    }
    
    /**
     * get Photo detail
     */
    
    public static function get_photo_detail($slug){
        
        $photos = \Photo\Model_PhotoDetails::query()
            ->related('photos')    
            ->related('images')
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
                
        $data = array();
        
        foreach ($photos as $photo){
            
            $data[] = array(
                'title'         => $photo->title,
                'slug'          => $photo->slug,
                'image'         => $photo->images->filename
            );
        }
        return $data;
    }
    
}