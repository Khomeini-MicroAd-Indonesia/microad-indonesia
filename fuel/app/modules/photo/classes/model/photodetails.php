<?php
namespace Photo;

class Model_PhotoDetails extends \Orm\Model {
    
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/photo/';

    protected static $_table_name = 'photo_detail_images';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'photo_detail_images'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'photo_detail_images'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'photo_detail_images'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'photo_id' => array(
            'label' => 'Photo',
            'validation' => array(
                'required',
            )
        ),
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'title' => array(
            'label' => 'Detail Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'slug' => array(
            'label' => 'Detail Slug',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
        
    protected static $_belongs_to = array(
        'photos' => array(
            'key_from' => 'photo_id',
            'model_to' => '\photo\Model_Photos',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\photo\Model_PhotoImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )

    );
	
    private static $_photos;
    private static $_images;
        
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_photo_name(){
        if(empty(self::$_photos)){
            self::$_photos = Model_Photos::get_as_array();
        }
        $flag = $this->photo_id;
        return isset(self::$_photos[$flag]) ? self::$_photos[$flag] : '-';
    }

    public function get_image_name(){
        if(empty(self::$_images)){
            self::$_images = Model_PhotoImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->title;
            }
        }
        return $data;
    }

    public function get_form_data_basic($photo, $image) {
        return array(
            'attributes' => array(
                'name' => 'frm_photo_detail',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Album Name',
                        'id' => 'album_name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'album_name',
                        'value' => $photo,//$this->name,
                        'options' => $photo,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Name',
                        'id' => 'image_name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_name',
                        'value' => $image,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Image Name',
                            'data-live-search' => 'true',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'detail_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'detail_title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'detail_seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'detail_seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
}
