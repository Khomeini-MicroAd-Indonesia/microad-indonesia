<?php

namespace News;

class Newsutil {

    /**
     * get News data
     */
    public static function get_news_data() {

        $newss = \News\Model_News::query()
                //->related('images')
//                ->related('images_m')
//                ->related('images_s')
                ->where('status', 1)
                ->order_by('seq', 'DESC')
                ->limit(5)
                ->get();

        $data = array();

        foreach ($newss as $news) {
            
            $count = 0;
            $year = date('Y', strtotime($news->date));
            $month = date('M', strtotime($news->date));
            $day = date('d', strtotime($news->date));
            //var_dump($day);exit;
            $data[] = array(
                'slug'      => $news->slug,
                'id'        => $news->id,
//                'name'      => $news->images->name,
//                'image'     => $news->images->filename,
                'day'       => $day,
                'month'     => $month,
                'year'      => $year,
                'highlight' => $news->highlight,
                'content'   => $news->content,
                'seq'       => $news->seq,
                'count'     => $count = $count + 1
            );
        }
        return $data;
    }
    
    /**
     * get News detail
     */
    
    public static function get_news_detail($slug){
        
        $newss = \News\Model_News::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
                
        $data = array();
        
        foreach ($newss as $news){
            
            $data[] = array(
                'slug'          => $news->slug,
                'date'          => date('d F Y'),
                'highlight'     => $news->highlight,
                'content'       => $news->content,
            );
        }
        return $data;
    }
    
    /**
     * get News image
     */
    
    public static function get_news_image($slug){
        
        $newss = \News\Model_NewsImages::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
                
        $data = array();
        
        foreach ($newss as $news){
            
            $data[] = array(
                'slug'          => $news->slug,
                'image_name'    => $news->name,
                'image'         => $news->filename
            );
        }
        return $data;
    }

}
