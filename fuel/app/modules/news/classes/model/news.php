<?php
namespace News;

class Model_News extends \Orm\Model {
    private $status_name = array('InActive', 'Active');

    protected static $_table_name = 'news';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'news'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'news'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'news'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'slug' => array(
            'label' => 'Slug',
            'validation' => array(
                'max_length' => array(50)
            )
        ),
//        'image_id' => array(
//            'label' => 'Image',
//            'validation' => array(
//                'required',
//            )
//        ),
        'date' => array(
            'label' => 'Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'highlight' => array(
            'label' => 'Highlight',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'content' => array(
            'label' => 'Content',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

//    protected static $_belongs_to = array(
//        'images' => array(
//            'key_from' => 'image_id',
//            'model_to' => '\news\Model_NewsImages',
//            'key_to' => 'id',
//            'cascade_save' => true,
//            'cascade_delete' => false,
//        )
//    );
    
    //private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
//    public function get_image_name() {
//        if (empty(self::$_images)) {
//            self::$_images = Model_NewsImages::get_as_array();
//        }
//        $flag = $this->image_id;
//        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
//    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_news',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
//                array(
//                    'label' => array(
//                        'label' => 'Image',
//                        'id' => 'image_id',
//                        'attributes' => array(
//                            'class' => 'col-sm-2 control-label'
//                        )
//                    ),
//                    'select' => array(
//                        'name' => 'image_id',
//                        'value' => $this->image_id,
//                        'options' => $image,
//                        'attributes' => array(
//                            'class' => 'form-control bootstrap-select',
//                            'placeholder' => 'Image',
//                            'data-live-search' => 'true',
//                            'data-size' => '3',
//                        ),
//                        'container_class' => 'col-sm-10'
//                    )
//                ),
                array(
                    'label' => array(
                        'label' => 'Date',
                        'id' => 'date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'date',
                        'value' => $this->date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Highlight',
                        'id' => 'highlight',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'highlight',
                        'value' => $this->highlight,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight',
                            'required' => '',
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content',
                        'id' => 'content',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content',
                        'value' => $this->content,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
}
