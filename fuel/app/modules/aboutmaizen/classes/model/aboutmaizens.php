<?php
namespace Aboutmaizen;

class Model_AboutMaizens extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'about_maizens';

	protected static $_observers = array(
            'Orm\Observer_CreatedAt' => array(
                'about_maizens'=>array('before_insert'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_UpdatedAt' => array(
                'about_maizens'=>array('before_update'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_Validation' => array(
                'about_maizens'=>array('before_save')
            )
	);
        
	protected static $_properties = array(
            'id',
            'header' => array(
                'label' => 'Header',
                'validation' => array(
                    'required',
                )
            ),
            'content' => array(
                'label' => 'Content',
                'validation' => array(
                    'required',
                )
            ),
            'status' => array(
                'label' => 'Status',
                'validation' => array(
                    'required',
                )
            ),
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
	);
	
        public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic() {
            return array(
                'attributes' => array(
                    'name'       => 'frm_about_maizens',
                    'class'      => 'form-horizontal',
                    'role'       => 'form',
                    'action'     => '',
                    'method'     => 'post',
                ),
                'hidden' => array(),
                'fieldset' => array(
                    array(
                        'label' => array(
                            'label' => 'Header',
                            'id' => 'header',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'header',
                            'value' => $this->header,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Content',
                            'id' => 'content',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'content',
                            'value' => $this->content,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Status',
                            'id' => 'status',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select' => array(
                            'name' => 'status',
                            'value' => $this->status,
                            'options' => $this->status_name,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Status',
                                'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                        )
                    ),
                )
            );
	}
}
