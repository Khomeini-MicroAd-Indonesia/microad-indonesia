<?php

namespace Aboutmaizen;

class Aboutmaizenutil{
    
    
    /**
     * get about maizen 
     */

    public static function get_about_maizen(){

        $contents = \Aboutmaizen\Model_AboutMaizens::query()
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($contents as $content){

            $data[] = array(
                'header'    => $content->header,
                'content'   => $content->content
            );
        }

        return $data;
    }
    
}