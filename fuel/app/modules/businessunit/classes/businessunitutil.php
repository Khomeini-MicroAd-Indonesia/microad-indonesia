<?php
namespace Businessunit;

class Businessunitutil{

    /**
     * get Business Unit data
     */
    
    public static function get_businessunit_data(){
        $businessunits = \Businessunit\Model_BusinessUnits::query()
                ->related('images')
                ->where('status', 1)
                ->limit(1)
                ->get();
                
        $data = array();
        
        foreach ($businessunits as $businessunit){
            
            $data[] = array(
                'image'      => $businessunit->images->filename,
                'name'       => $businessunit->name,
                'desc'       => $businessunit->description
            );
        }
        return $data;
    }
    
}

