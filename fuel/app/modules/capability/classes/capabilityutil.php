<?php
namespace Capability;

class Capabilityutil{

    /**
     * get Capability data
     */
    
    public static function get_capability_data(){
        $capabilities = \Capability\Model_Capabilities::query()
                ->related('images')
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($capabilities as $capability){
            
            $data[] = array(
                'image' => $capability->images->filename,
                'name'  => $capability->name,
                'desc'  => $capability->description
            );
        }
        return $data;
    }
    
}

