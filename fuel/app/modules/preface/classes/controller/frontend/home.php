<?php
namespace About;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'about';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->_data_template['about_detail']   = \About\Aboututil::get_about_detail();
        $this->_data_template['active_about'] = "active";
        return \Response::forge(\View::forge('about::frontend/about.twig', $this->_data_template, FALSE));
    }
    public function action_awards_and_certificates() {
        
        $this->_data_template['award_detail']           = \About\Aboututil::get_award_detail();
        $this->_data_template['certificate_detail']     = \About\Aboututil::get_certificate_detail();
        $this->_data_template['active_award'] = "active";
        return \Response::forge(\View::forge('about::frontend/awards_and_certificates.twig', $this->_data_template, FALSE));
    }
    
    public function action_history() {
        
        $this->_data_template['history_detail'] = \History\Historyutil::get_history_data();
        $this->_data_template['active_history'] = "active";
        return \Response::forge(\View::forge('about::frontend/history.twig', $this->_data_template,FALSE));
    }

}

