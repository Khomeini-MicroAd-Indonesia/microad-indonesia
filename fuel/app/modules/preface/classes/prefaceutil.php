<?php

namespace Preface;

class Prefaceutil{
    
    
    /**
     * get preface 
     */

    public static function get_preface_content(){

        $prefaces = \Preface\Model_Prefaces::query()
                ->where('status', 1)
                ->limit(1)
                ->get();

        $data = array();

        foreach ($prefaces as $preface){

            $data[] = array(
                'content'   => $preface->content
            );
        }

        return $data;
    }
    
}