<?php

namespace Ourpeople;

class Ourpeopleutil{
    
    
    /**
     * get our people content
     */

    public static function get_ourpeople_content(){

        $ourpeoples = \Ourpeople\Model_OurPeoples::query()
            ->where('status', 1)
            ->get();

        $data = array();

        foreach ($ourpeoples as $ourpeople){

            $data[] = array(
                'content1'  => $ourpeople->content1,
                'content2'  => $ourpeople->content2
            );
        }

        return $data;
    }
    
}