/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
  
    //var count = 100;
    //var maxScrollLeft = $(document).width();
    //console.log(maxScrollLeft);
   
    $(window).bind('scroll', function(e){
        parallax(); 
    });
    
});

var xPrev = 0;
var count = 100;

$(document).mousemove(function(e){
    xPrev < e.pageX ? parallaxv(e.pageX) : parallaxv(e.pageX);//$('#base').html("right") : $('#base').html("left");
    xPrev = e.pageX;
   //$('#status').html(e.pageX +', '+ e.pageY);
   console.log(e.pageX +', '+ e.pageY);

});

function parallax() {

    //scroll horizontally
    var scrollPosition = $(window).scrollLeft();
    $('#base').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#bg-city').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#tokyo-eye').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#tokyo-tower').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#tokyo-crane').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#tokyo-shinkansen').css('left', (0 - (scrollPosition * .1)) + 'px');
    $('#tokyo').css('left', (0 - (scrollPosition * .2)) + 'px');
    $('#tokyo-street').css('left', (0 - (scrollPosition * .2)) + 'px');
    $('#gen-city').css('left', (0 - (scrollPosition * .3)) + 'px');
    $('#monas').css('left', (0 - (scrollPosition * .3)) + 'px');
    $('#jakarta').css('left', (0 - (scrollPosition * .4)) + 'px');
    $('#jakarta-street').css('left', (0 - (scrollPosition * .5)) + 'px');
}

function parallaxv(count) {
    
    $('#base').css('left', (0 - (count * .1)) + 'px');
    $('#bg-city').css('left', (0 - (count * .1)) + 'px');
    $('#tokyo-eye').css('left', (0 - (count * .1)) + 'px');
    $('#tokyo-tower').css('left', (0 - (count * .1)) + 'px');
    $('#tokyo-crane').css('left', (0 - (count * .1)) + 'px');
    $('#tokyo-shinkansen').css('left', (0 - (count * .1)) + 'px');
    $('#tokyo').css('left', (0 - (count * .2)) + 'px');
    $('#tokyo-street').css('left', (0 - (count * .2)) + 'px');
    $('#gen-city').css('left', (0 - (count * .3)) + 'px');
    $('#monas').css('left', (0 - (count * .3)) + 'px');
    $('#jakarta').css('left', (0 - (count * .4)) + 'px');
    $('#jakarta-street').css('left', (0 - (count * .5)) + 'px');
}